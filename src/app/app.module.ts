import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { ToastService } from './shared/services/toast.service';
import { LoadingService } from './shared/services/loading.service';

import { AgmCoreModule } from '@agm/core';
import { SharedModule } from './shared.module';

import { HTTP } from '@ionic-native/http/ngx';
//import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { FCM } from "cordova-plugin-fcm-with-dependecy-updated/ionic/ngx";
import { Device } from '@ionic-native/device/ngx';
import { Network } from '@ionic-native/network/ngx';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';
//import { PayPal } from '@ionic-native/paypal/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
//import { Diagnostic } from '@ionic-native/diagnostic/ngx';
//import { EmailComposer } from '@ionic-native/email-composer/ngx';
//import { CallNumber } from '@ionic-native/call-number/ngx';
//import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';
import { NgxPayPalModule } from 'ngx-paypal';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyC1ewEPNVNYVrFifs4Of1V93uivtBXouhA', 
      libraries: ['places'],
      region: "IT",
      language: "IT"
    }),
    SharedModule,
    FormsModule,
    NgxPayPalModule,
    ReactiveFormsModule
  ],
  providers: [
    StatusBar,
    SplashScreen, ToastService, LoadingService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    NativeStorage,
    HTTP,
    Device,
    Network,
    DatePipe,
    //PayPal,
    InAppBrowser,
    Geolocation,
    FCM,
    NativeGeocoder,
    //Diagnostic,
    //EmailComposer,
    //CallNumber,
    //LaunchNavigator
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {}
