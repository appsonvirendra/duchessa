import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ToastService } from '../../shared/services/toast.service';
import { ConfigApp, UtilityType, TypeService } from '../../shared/services/config-app.service';
import { RecoveryPage } from '../recovery/recovery.page';
import { RegisterPage } from '../register/register.page';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { DB, RESPONSE } from '../../shared/services/env.service';
import { ApiService } from '../../shared/services/api.service';
import { AuthService } from '../../shared/services/auth.service';
import { NavController } from '@ionic/angular';
import { LocationselectPage } from '../locationselect/locationselect.page';
import { RestaurantService } from '../../shared/services/restaurant.service';
import { Router } from '@angular/router';
import { LoadingService } from '../../shared/services/loading.service';
import { AlertService } from '../../shared/services/alert.service';
import { CartService } from 'src/app/shared/services/cart.service';
import { EitPlace } from 'src/app/shared/classes/eitplace';
import { UtilityService } from '../../shared/services/utility.service';
import { NcivicoPage } from '../ncivico/ncivico.page';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderResult } from '@ionic-native/native-geocoder/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  item: any = {username: "", password: ""};

  query: string = "";
  locationData: any;  

  constructor(private navController: NavController, private auth: AuthService, private api: ApiService, private nativeStorage: NativeStorage, private modalCtrl: ModalController, private authService: AuthService, private toastService: ToastService, public config: ConfigApp, private restService: RestaurantService, private router: Router, private loadingService: LoadingService, private alertService: AlertService, private cartService: CartService, private utilityService: UtilityService, private geolocation: Geolocation, private nativeGeo: NativeGeocoder) { }

  ngOnInit() {
    this.locationData = {};
    this.locationData.place = new EitPlace();
    this.locationData.place.zipcode = "";
    this.locationData.place.num = "";
    this.locationData.place.nomelocale = "";
    this.locationData.place.latitude = 0;
    this.locationData.place.longitude = 0;
    this.locationData.place.street_address = "";
    this.locationData.place.locality = "";
    this.locationData.place.sublocality = "";
    this.locationData.place.regione = "";
    this.locationData.place.nazione = "";
    this.locationData.place.admin_area_3 = "";

    if (localStorage.getItem("location")) {
      this.locationData.place = JSON.parse(localStorage.getItem("location"));
      this.query = this.locationData.place.description;
    }    
  }

  login(){
    console.log("login")
    if(this.isValid()){
      this.api.login(this.item.username, this.item.password).then(res => {
        if (res[this.config.JP_STATUS] == this.config.JPS_OK &&
          res[this.config.JP_RESULT]) {
            this.nativeStorage.setItem(DB.USERNAME, this.item.username).then(() => {}, error => console.error('Error login store', error));
            this.nativeStorage.setItem(DB.PW, this.item.password).then(() => {}, error => console.error('Error login store', error));
           

          let cUser = res[RESPONSE.JP_RESULT];
          this.auth.next(cUser);
         
          this.goBack();
        }
        else {
          this.toastService.presentToast("Email e/o password errate!")
        }

      }, (err) => {

        this.toastService.presentToast("Inserisci tutti i campi!")
        console.log(err);

      });

    }
      
  }

  isValid(){
    return this.item.username.length > 0 && this.item.password.length;
  }

  async recover(){
    let modal = await this.modalCtrl.create({component: RecoveryPage});
    return await modal.present();
  }

  async register(){
    let modal = await this.modalCtrl.create({component: RegisterPage});
    return await modal.present();
  }

  dismiss(data: any) {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss(data);
  }

  takeawayClick() {
    this.api.postj("apps/getinfoapp", "", "").then(res => {
      console.log(res);
      if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {        
        if(res[this.config.JP_RESULT].count_asporto){
          localStorage.setItem("service", "0");
          let btns = [{text: 'No', role: 'cancel', handler: () => {console.log('Dismiss clicked');}}, 
                      {text: 'Sì', handler: () => {
                        localStorage.setItem("service", "1");
                        if(res[this.config.JP_RESULT].count_restaurants > 1){
                          this.restService.loadTakeAway(this.locationData.place);
                          this.router.navigate(['/restlist'], { /*queryParams: { record: JSON.stringify(order) } */});
                        }
                        else{
                          this.checkAddress();
                        }
                      }}]
          this.alertService.showAlert('Ritira al locale', "Confermi di voler ritirare il tuo ordine direttamente al locale?", btns);
        }
        else this.alertService.showAlert('Ritira al locale', "Servizio non attivo!");
      }
    })
  }  

  async searchPlace()
  {
    let modal = await this.modalCtrl.create({component: LocationselectPage});
    (await modal).onDidDismiss().then((result) => {
      if ((result.data.result = this.config.RESULT_OK)) {
        if (null != result.data.location) {
  
          this.query = result.data.location.description;
          this.locationData.place = result.data.location;
          this.locationData.place.numForced = "";
          //this.config.selectPlace(this.geocoder, this.locationData.place, result.location);
        }
      }
    });
  
    return await modal.present();
  }  

  async checkAddress() {
    let service =  JSON.parse(localStorage.getItem("service"));
    switch(service){
      case 0:
        //await this.loadingService.show();
        this.api.postj("restaurant/checkaddress", this.locationData, "").then(res => {
          console.log(res);
    
          this.loadingService.hide();
          if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {     
            let rest = res[this.config.JP_RESULT]
            localStorage.setItem('restaurant', JSON.stringify(rest));
            let ispreorder = (!rest.message_event && rest.nowclosed && !rest.day_closed && rest.preorder);
            this.cartService.createCart(rest.id, this.auth.idOrder, this.locationData.place, TypeService.HOME, ispreorder)
            this.router.navigate(['/menu']);
          }
          else if (res[this.config.JP_STATUS] == this.config.JPS_ERROR && res[this.config.JP_MSG]) {
            this.alertService.showAlert("Attenzione", res[this.config.JP_MSG]);
          }
          else {
            console.log(res);
            this.alertService.showAlert('Attenzione', "Il locale è momentaneamente chiuso!");
          }
    
        }, (err) => {
          console.log(err);
          this.loadingService.hide();
        });
        break;
      case 1:
        //devo prendere le info del ristorante
        this.loadingService.show();
        this.api.postj("restaurant/recordtakeaway", "", "").then(res => {
          console.log(res);
    
          if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {    
            let rest = res[this.config.JP_RESULT]
            localStorage.setItem('restaurant', JSON.stringify(rest));
            let ispreorder = (!rest.message_event && rest.nowclosed && !rest.day_closed && rest.preorder);
            this.cartService.createCart(rest.id, this.auth.idOrder, this.locationData, TypeService.TAKEAWAY, ispreorder)
            this.router.navigate(['/menu']);
          }
          else {
            console.log(res);
            this.alertService.showAlert("Attenzione", res[this.config.JP_MSG]);
            //this.showAlert('Attenzione', "Impossibile inviare la richiesta!");
          }
    
          this.loadingService.hide();
        }, (err) => {
          console.log(err);
          this.loadingService.hide();
        });
        break;
    }
  }  

  menuClick() {
    /*this.diagnostic.getLocationMode().then(state =>{
      if(state === this.diagnostic.locationMode.LOCATION_OFF) {
        this.diagnostic.switchToLocationSettings();
      }
      else{  */
        this.geolocation.getCurrentPosition().then(position => {            
          console.log(JSON.stringify(position));  
          this.nativeGeo.reverseGeocode( position.coords.latitude, position.coords.longitude).then((result: NativeGeocoderResult[]) =>{
            console.log(JSON.stringify(result[0]));  
            this.router.navigate(['/menu-locale'], { queryParams: { coords: position.coords } });
          }).catch((error: any) => console.log(error));
        }, err => {
          console.log(err);
        })
        .catch((error: any) => {
          console.log(error)
        });
      /*}
    }); */  
  }  

  deliveryClick() {

    if (!(this.locationData.place.num && this.locationData.place.num.length) && (this.locationData.place.numForced && this.locationData.place.numForced.length)) {
      this.locationData.place.num = this.locationData.place.numForced;
    }

    if(!this.config.isOnline()) {
      this.utilityService.showUtilityPage(UtilityType.OFFLINE);
    }
    else if (this.locationData.place.num && this.locationData.place.zipcode && this.locationData.place.num.length && this.locationData.place.zipcode.length) {
      this.api.postj("apps/getinfoapp", "", "").then(res => {
        console.log(res);
        if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {
          if (this.locationData.place.locality === "") this.locationData.place.locality = this.locationData.place.admin_area_3;

          localStorage.setItem("location", JSON.stringify(this.locationData.place));
          localStorage.setItem("service", "0");

          this.restService.loadDelivery(this.locationData.place);  
          if(res[this.config.JP_RESULT].count_restaurants > 1){
            this.router.navigate(['/restlist'], { /*queryParams: { record: JSON.stringify(order) } */});
          }
          else{
            this.checkAddress();
          }
        }
      })
    }
    else if (this.locationData.place.street_address && this.locationData.place.street_address.length) {
      this.selectCivico();
    }
    else {
      this.utilityService.showUtilityPage(UtilityType.ADDRESS);
    }
  }  

  async selectCivico() {
    let modal = await this.modalCtrl.create({component: NcivicoPage});
    (await modal).onDidDismiss().then((result) => {
      if ((result.data.result = this.config.RESULT_OK)) {
        if(null!=result.data.civico){
          let geocoder = new google.maps.Geocoder;
          this.locationData.place.num = result.data.civico;
          this.locationData.place.numForced = result.data.civico;
          this.locationData.place.description = this.config.getAddressFromPlace(this.locationData.place);
          this.config.selectAddress(geocoder, this.locationData.place, this.locationData.place.description);
          this.query = this.locationData.place.description;
          //this.goDelivery(); loop*/
        }
      }
      
    });
    return await modal.present();
  }  

  goBack()
  {
    this.navController.back();
  }
}
