import { Component, OnInit, Input } from '@angular/core';
import { UtilityType } from '../../shared/services/config-app.service';
import { ModalController } from '@ionic/angular';
import { Location } from '@angular/common';

@Component({
  selector: 'app-utility',
  templateUrl: './utility.page.html',
  styleUrls: ['./utility.page.scss'],
})
export class UtilityPage implements OnInit {

  @Input() type: number;
  @Input() info: any = {};
  utilityType: any = UtilityType;

  constructor(private modalCtrl: ModalController, private location: Location) { }

  ngOnInit() {
  }

  dismiss(data: any) {
    this.modalCtrl.dismiss(data);
  }

  goBack() {
    this.location.back();
  }   

}
