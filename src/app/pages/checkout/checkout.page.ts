import { Component, OnInit } from '@angular/core';
import { ConfigApp, TypeOrder } from '../../shared/services/config-app.service';
import { ApiService } from '../../shared/services/api.service';
import { ModalController } from '@ionic/angular';
import { OrdertimePage } from '../ordertime/ordertime.page';
import { LocationselectPage } from '../locationselect/locationselect.page';
import { AlertService } from '../../shared/services/alert.service';
import { Router } from '@angular/router';
import { CartService } from '../../shared/services/cart.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.page.html',
  styleUrls: ['./checkout.page.scss'],
})
export class CheckoutPage implements OnInit {

  cartData: any = "";
  isDomicilio: boolean = true;
  valid: any = {citofono: true, phone: true}
  isOrderTimeValid: boolean;
  isLocationValid: boolean;
 
  constructor(private cartService: CartService, private api: ApiService, public config: ConfigApp, private modalController: ModalController, private alertService: AlertService, private router: Router) { 
    this.cartService.cartObs.subscribe(x => {

      this.cartData = x;
      if(x && this.cartData.order)
      {
        this.isDomicilio = this.cartData.order.type_order == TypeOrder.EIT_DOMICILIO || this.cartData.order.type_order == TypeOrder.REST_DOMICILIO
      }

    });
  }

  ionViewWillEnter(){  
   
  }

  ngOnInit() {
    console.log("checkout")
  }

  goToHome(){
    this.router.navigateByUrl('/');
  }

  confirm(){
    let dataValid = this.validate();

    /*if (this.cartData.order.type_order) {
      dataValid = this.valid.phone;
    }*/
    /*this.order_form_submitted = true;*/
    this.isLocationValid = dataValid;
    this.isOrderTimeValid = true;

    if (this.cartData.order.tipooraconsegna == 0){
      //controllo order_date      
      console.log("Booking_date:"+this.cartData.order.bookingdate);
      if (this.cartData.order.order_date == "-2"){
        this.isOrderTimeValid = false;
        this.alertService.showAlert("", "Attenzione! Selezionare un orario valido!");
      }
      else{
        var hmArray = this.cartData.order.order_date.split(":");
        let ore = "", minuti  = "";
        
        if (hmArray.length == 2) {
          ore = hmArray[0];
          minuti = hmArray[1];
        }

        let dateToBeCheckOut = new Date(this.cartData.order.bookingdate);
        dateToBeCheckOut = new Date( dateToBeCheckOut.getFullYear(), dateToBeCheckOut.getMonth(), dateToBeCheckOut.getDate(), + ore, +minuti); //col + cast string==>number

        let today = new Date();
        if(dateToBeCheckOut < today){
          this.isOrderTimeValid = false;
          this.alertService.showAlert("", "Attenzione! Selezionare un orario valido!");
        }
      }
    }
    else{
      //controllo la fascia oraria
      if (this.cartData.order.fasciaoraria == "-2"){
        this.isOrderTimeValid = false;
        this.alertService.showAlert("", "Attenzione! Selezionare una fascia oraria valida!");
      }
    }

    if (this.isLocationValid && this.isOrderTimeValid) {
      //this.content.resize();
      this.cartService.postCart(this.cartData);
      this.router.navigate(['/checkpay']);
    } else {
      this.alertService.showAlert("", "Attenzione! Controllare i dati inseriti!");
    }
  }

  validate(): boolean {
    this.valid.citofono = !this.isDomicilio || (this.cartData.order.location.citofono && this.cartData.order.location.citofono.length > 0);
    this.valid.phone = this.cartData.order.phone && this.cartData.order.phone.length > 0;

    return this.valid.citofono && this.valid.phone;
  }

  async onChangeTime(){ 
    let modal = await this.modalController.create({component: OrdertimePage, componentProps: {}});
    (await modal).onDidDismiss().then((result) => {
      if (result && result.data && result.data.result == this.config.JPS_OK) {
        if(result.data.orarioscelto){            
          this.cartService.postOrario(this.cartData, result.data.orarioscelto, result.data.giornoconsegna);
          
        }
      }
    });  
    return await modal.present();
  }

  async onChangeLocation(){
    let modal = await this.modalController.create({component: LocationselectPage, componentProps: {}});
    (await modal).onDidDismiss().then((result) => {
      if (result && result.data && result.data.result == this.config.RESULT_OK) {
        if(result.data.location){ 
          let data = {place: result.data.location, order_id: this.cartData.order_id}
          this.api.postj("restaurant/checkaddress", data, "").then(res => {
            if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {              
              
              localStorage.setItem("location", JSON.stringify(result.data.location));
              this.cartService.postLocation(result.data.location, this.cartData);             
            }
            else if(res[this.config.JP_STATUS] == this.config.JPS_ERROR && res[this.config.JP_MSG]){
              this.alertService.showAlert("", "Attenzione! Questo indirizzo è fuori dalla zona di consegna del locale selezionato!")
            }
            else {
              console.log(res);
            }
          }, err => {
            console.log(err);
          });
        }
      }
    });  
    return await modal.present();
  } 

  goToCart() {
    this.router.navigate(['/cart']);
  }   

}
