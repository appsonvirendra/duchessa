import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { AppInfo } from '../../shared/services/env.service';
import { ConfigApp, TypeOrder, UtilityType } from '../../shared/services/config-app.service';
import { LoadingService } from '../../shared/services/loading.service';
import { UtilityService } from '../../shared/services/utility.service';
import { ModalController } from '@ionic/angular';
import { CouponPage } from '../coupon/coupon.page';
import { OrdertimePage } from '../ordertime/ordertime.page';
import { Router } from '@angular/router';
import { CartService } from '../../shared/services/cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {

  cartData: any = "";
  typeOrder: any = TypeOrder;
  appInfo: any = AppInfo;
  items: Array<any> = []
  isDomicilio: boolean = true;

  constructor(private cartService: CartService, private api: ApiService, public config: ConfigApp, private modalController: ModalController, private loadingService: LoadingService,
    private router: Router, private utilityService: UtilityService) {
    this.cartService.cartObs.subscribe(x => {

      this.cartData = x;
      if (x && this.cartData.order) {
        this.isDomicilio = this.cartData.order.type_order == TypeOrder.EIT_DOMICILIO || this.cartData.order.type_order == TypeOrder.REST_DOMICILIO;
        this.toArray();
      }

    });
  }

  ngOnInit() {
    console.log("cart")
  }

  goToHome() {
    this.router.navigateByUrl('/');
  }

  sovrapprezzoClick() {

  }

  addToCartP(item: any) {
    if (item != null) {
      this.cartService.addToCartSingle(item.id, 1, "", 1);
    }
  }

  removeToCartP(item: any) {
    if (item != null) {
      this.cartService.removeToCartP(item.id, 1, 1);
    }
  }

  async onPromoCode() {
    let modal = await this.modalController.create({ component: CouponPage, componentProps: {} });
    (await modal).onDidDismiss().then((result) => {
      if (result && result.data && result.data.result == this.config.RESULT_OK) {
        if (result.data.codice && result.data.codice.length > 0) {
          this.api.postj("cart/updatecart", { order_id: this.cartData.order_id }, "").then(res => {
            if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {
              this.cartData = res[this.config.JP_RESULT];
              this.cartService.next(this.cartData);
            }
            else {
              console.log(res);
            }
          }, err => {
            console.log(err);
          });
        }
      }
    });
    return await modal.present();
  }

  async onOrderTime() {
    let modal = await this.modalController.create({ component: OrdertimePage, componentProps: {} });
    (await modal).onDidDismiss().then((result) => {
      if (result && result.data && result.data.result == this.config.JPS_OK) {
        if (result.data.orarioscelto) {
          this.cartService.postOrario(this.cartData, result.data.orarioscelto, result.data.giornoconsegna);
        }
      }
    });
    return await modal.present();
  }

  goToCheckout() {
    if (this.cartData.order.sovrapprezzononapplicabile) {
      this.utilityService.showUtilityPage(UtilityType.MINORDER, { ordineminimo: this.cartData.order.ordineminimo });
    }
    else {
      this.router.navigate(['/checkout']);
    }
  }

  toArray() {
    if (this.cartData.order && this.cartData.order.items) {
      this.items = Object.keys(this.cartData.order.items).map(key => ({ type: key, value: this.cartData.order.items[key] }));
    }
  }

  goToMenu() {
    this.router.navigate(['/menu']);
  }   
}
