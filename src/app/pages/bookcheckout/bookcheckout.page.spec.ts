import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BookcheckoutPage } from './bookcheckout.page';

describe('BookcheckoutPage', () => {
  let component: BookcheckoutPage;
  let fixture: ComponentFixture<BookcheckoutPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookcheckoutPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BookcheckoutPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
