import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BookcheckoutPageRoutingModule } from './bookcheckout-routing.module';

import { BookcheckoutPage } from './bookcheckout.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BookcheckoutPageRoutingModule
  ],
  declarations: [BookcheckoutPage]
})
export class BookcheckoutPageModule {}
