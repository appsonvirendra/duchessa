import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService } from '../../shared/services/alert.service';
import { DB } from '../../shared/services/env.service';
import { ApiService } from '../../shared/services/api.service';
import { ConfigApp } from '../../shared/services/config-app.service';
import { ModalController } from '@ionic/angular';
import { BookcheckpayPage } from '../bookcheckpay/bookcheckpay.page';
import { LoadingService } from '../../shared/services/loading.service';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Location } from '@angular/common';

@Component({
  selector: 'app-bookcheckout',
  templateUrl: './bookcheckout.page.html',
  styleUrls: ['./bookcheckout.page.scss'],
})
export class BookcheckoutPage implements OnInit {

  bookData: any = {};
  cartBookData: any = {};
  valid: any = {name: true, lastname: true, email: true, phone: true, note: true}

  constructor(private api: ApiService, public config: ConfigApp, private router: Router, private alertService: AlertService, private modalController: ModalController,
              private loadingService: LoadingService, private nativeStorage: NativeStorage, public zone: NgZone, private location: Location) { 
    let booktemp = localStorage.getItem(DB.BOOK);
    if(booktemp) this.bookData = JSON.parse(booktemp);
    let carttemp = localStorage.getItem(DB.CARTBOOK);
    if(carttemp) this.cartBookData = JSON.parse(carttemp);
  }

  ngOnInit() {
  }

  ionViewWillEnter(){
    let carttemp = localStorage.getItem(DB.CARTBOOK);
    if(carttemp) {
      this.cartBookData = JSON.parse(carttemp);
      if (!this.cartBookData || this.cartBookData.order.status == 1) this.goToHome();
    }
    else this.goToHome();
  }

  goToHome(){
    console.log("gohome");
    localStorage.removeItem(DB.BOOK);
    localStorage.removeItem(DB.CARTBOOK);
    this.router.navigateByUrl('/');
  }

  withMenu(){
    return this.cartBookData.count > 0;
  }

  onComplete(){ 
    if(this.withMenu()){
      if(this.validate()) this.goToPayment();
      else this.alertService.showAlert("", "Attenzione! Controllare i dati inseriti!");
    }
    else this.finalize();
  }

  finalize(){
    if(this.validate()){
      this.nativeStorage.getItem(DB.USER).then(data =>{
        this.zone.run(() => {
          this.loadingService.show();
          var params = {idbook: this.bookData.book_id, iduser: data.user_id, name: this.bookData.book.name, lastname: this.bookData.book.lastname, 
                        email: this.bookData.book.email, phone: this.bookData.book.phone, note: this.bookData.book.note};
          this.api.postj("booking/finalizebooking", params, "").then(res => {        
            console.log(res);              
            this.loadingService.hide();
            if (res[this.config.JP_STATUS] == this.config.JPS_OK ) { 
              this.router.navigate(['/bookfinalize']);
            }
            else{
              localStorage.removeItem(DB.BOOK);
              localStorage.removeItem(DB.CARTBOOK);
              this.router.navigateByUrl('/');
            }
          }, err => {
            console.log(err);
            this.loadingService.hide();
            localStorage.removeItem(DB.BOOK);
            localStorage.removeItem(DB.CARTBOOK);
            this.router.navigateByUrl('/');
          });
        })
      })
    }
    else this.alertService.showAlert("", "Attenzione! Controllare i dati inseriti!");
  }

  async goToPayment(){
    let modal = await this.modalController.create({component: BookcheckpayPage, componentProps: {}});
    (await modal).onDidDismiss().then((result) => {    
      console.log("dismissed")      
      if (result && result.data.result == this.config.RESULT_OK) this.finalize();
    });
    return await modal.present();
  }

  validate(): boolean {
    this.valid.name = this.bookData.book.name && this.bookData.book.name.length > 0;
    this.valid.lastname = this.bookData.book.lastname && this.bookData.book.lastname.length > 0;
    this.valid.email = this.bookData.book.email && this.bookData.book.email.length > 0;
    this.valid.phone = this.bookData.book.phone && this.bookData.book.phone.length > 0;
    return this.valid.name && this.valid.phone;
  }

  goBack() {
    this.location.back();
  }      

}
