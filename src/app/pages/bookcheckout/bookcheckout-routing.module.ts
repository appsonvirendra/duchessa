import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BookcheckoutPage } from './bookcheckout.page';

const routes: Routes = [
  {
    path: '',
    component: BookcheckoutPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BookcheckoutPageRoutingModule {}
