import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreditPayPageRoutingModule } from './credit-pay-routing.module';

import { CreditPayPage } from './credit-pay.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreditPayPageRoutingModule
  ],
  declarations: [CreditPayPage]
})
export class CreditPayPageModule {}
