import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CreditPayPage } from './credit-pay.page';

describe('CreditPayPage', () => {
  let component: CreditPayPage;
  let fixture: ComponentFixture<CreditPayPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditPayPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CreditPayPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
