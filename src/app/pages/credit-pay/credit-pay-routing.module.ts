import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreditPayPage } from './credit-pay.page';

const routes: Routes = [
  {
    path: '',
    component: CreditPayPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreditPayPageRoutingModule {}
