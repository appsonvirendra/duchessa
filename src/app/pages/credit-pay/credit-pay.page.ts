import { Component, OnInit, Input } from '@angular/core';
import { ConfigApp } from '../../shared/services/config-app.service';
import { ApiService } from '../../shared/services/api.service';
import { ModalController } from '@ionic/angular';
import { AlertService } from '../../shared/services/alert.service';
import { RechargePage } from '../recharge/recharge.page';
import { ConvertpointsPage } from '../convertpoints/convertpoints.page';

@Component({
  selector: 'app-credit-pay',
  templateUrl: './credit-pay.page.html',
  styleUrls: ['./credit-pay.page.scss'],
})
export class CreditPayPage implements OnInit {

  @Input() order: any = {};
  useraccount: any = {};
  credit: number = 0;
  popupCredit: number = -1;
  max: number;
  cartData: any = "";

  constructor(private api: ApiService, private modalCtrl: ModalController, public config: ConfigApp, private alertService: AlertService) { }

  ngOnInit() {
    this.fillAccount();
  }

  fillAccount(){    
    this.api.postj("user/settings/infofidelity", "", "").then(res => {
      if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {
        this.useraccount = res[this.config.JP_RESULT]; 
        let sub = parseFloat((this.useraccount.credit - this.order.credit).toFixed(2));
        this.credit = this.max = sub < (this.order.euro_total + this.order.euro_tip) ? sub.toFixed(2) : (this.order.euro_total+this.order.euro_tip).toFixed(2);
      }
      else {
        console.log(res);
      }
    }, (err) => {
      console.log(err);
    })
  }

  onComplete(){    
    let flCredit = parseFloat(parseFloat(""+this.credit).toFixed(2));
    let diff = this.max == parseFloat((this.order.euro_total).toFixed(2));
   
    if(this.max - flCredit == 0 && diff){
      this.popupCredit = 0;
      this.presentConfirm();
    }
    else if(flCredit <= this.max && flCredit > 0){
      this.applyCredit();      
    }
    else if(flCredit <= 0){
      this.alertService.showAlert("", "Attenzione! Hai inserito un valore troppo basso!");
    }
    else{
      this.alertService.showAlert("", "Attenzione! Hai inserito un valore troppo alto!");
    }
  }
  
  applyCredit(){
    var data = {oid: this.order.id, type: 0, credit: parseFloat(""+this.credit).toFixed(2)};
    
    this.api.postj("order/applycredit", data, "").then(res => {
      if (res[this.config.JP_STATUS] == this.config.JPS_OK) {
        this.order.euro_total = this.credit > 0 ? parseFloat((this.order.euro_total - this.credit).toFixed(2)) : parseFloat((this.order.euro_total + this.order.credit).toFixed(2));
        this.order.credit = res[this.config.JP_RESULT];
        this.dismiss({'result':this.config.RESULT_OK});
      }
      else {
        console.log(res);
      }
    }, (err) => {
      console.log(err);
    })
  }

  removeCredit(){
    this.credit = 0;
    this.applyCredit();
  }

  async rechargeCredit(){
    let modal = await this.modalCtrl.create({component: RechargePage, componentProps: {useraccount: this.useraccount}});
    (await modal).onDidDismiss().then((result) => {
      
    })
    return await modal.present();
  }

  async convertEatpoints(){
    let modal = await this.modalCtrl.create({component: ConvertpointsPage, componentProps: {useraccount: this.useraccount}});
    (await modal).onDidDismiss().then((result) => {
      let sub = parseFloat((this.useraccount.credit - this.order.credit).toFixed(2));
      this.credit = this.max = sub < this.order.euro_total ? sub.toFixed(2) : this.order.euro_total.toFixed(2);      
    })
    return await modal.present();
  }

  presentConfirm() {
    let btns = [{text: 'Chiudi', role: 'cancel', handler: () => {console.log('OK clicked');}}, 
                {text: 'OK', handler: () => {console.log('OK clicked'); this.applyCredit();}}]
    this.alertService.showAlert("Conferma pagamento", "Vuoi concludere l'ordine utilizzando il credito?", btns);
  }

  dismiss(data: any) {
    this.modalCtrl.dismiss(data);
  }
}
