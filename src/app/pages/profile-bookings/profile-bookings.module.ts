import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfileBookingsPageRoutingModule } from './profile-bookings-routing.module';

import { ProfileBookingsPage } from './profile-bookings.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfileBookingsPageRoutingModule
  ],
  declarations: [ProfileBookingsPage]
})
export class ProfileBookingsPageModule {}
