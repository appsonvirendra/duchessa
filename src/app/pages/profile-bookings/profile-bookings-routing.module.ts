import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfileBookingsPage } from './profile-bookings.page';

const routes: Routes = [
  {
    path: '',
    component: ProfileBookingsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileBookingsPageRoutingModule {}
