import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ApiService } from '../../shared/services/api.service';
import { ConfigApp } from '../../shared/services/config-app.service';
import { LoadingService } from '../../shared/services/loading.service';
import { CancelbookingPage } from '../cancelbooking/cancelbooking.page';
import { Location } from '@angular/common';

@Component({
  selector: 'app-profile-bookings',
  templateUrl: './profile-bookings.page.html',
  styleUrls: ['./profile-bookings.page.scss'],
})
export class ProfileBookingsPage implements OnInit {

  bookings : Array<any> = [];
  queryPage: any = 0;
  queryPageSize: any = 5;

  constructor(private api: ApiService, public config: ConfigApp, private loadingService: LoadingService, private modalController: ModalController, private location: Location) { }

  ngOnInit() {
    this.fillBookings()
  }

  async fillBookings(){
    await this.loadingService.show();
    let params = {PAGE : this.queryPage, PAGESIZE : this.queryPageSize};
    this.api.postj("user/settings/bookings", params, "").then(res => {
      console.log(res)
      if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {
        let tmparr = res[this.config.JP_RESULT];
        if (typeof tmparr !== 'undefined' && tmparr != null && tmparr.length > 0){
          if (this.bookings.length == 0) this.bookings = tmparr;
          else this.bookings = this.bookings.concat(tmparr); 
        }
      }
      else {
        console.log(res);
      }
      this.loadingService.hide();
    }, (err) => {
      console.log(err);
      this.loadingService.hide();
    })
  }

  nextclick(){
    this.queryPage ++;
    this.fillBookings();
  }
  
  toggleGroup(group: any) {
    group.show = !group.show;
  }

  async cancelbook(book: any){
    let modal = await this.modalController.create({component: CancelbookingPage, componentProps: {booking: book}});
    (await modal).onDidDismiss().then((result) => {    
      console.log("dismissed") 
      if(result && result.data.result == this.config.RESULT_OK) {    
        this.queryPage = 0;
        this.bookings = [];
        this.fillBookings()
      }
    });
    return await modal.present();
  }

  goBack() {
    this.location.back();
  }     

}
