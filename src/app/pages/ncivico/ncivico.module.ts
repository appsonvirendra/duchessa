import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NcivicoPageRoutingModule } from './ncivico-routing.module';

import { NcivicoPage } from './ncivico.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NcivicoPageRoutingModule
  ],
  declarations: [NcivicoPage]
})
export class NcivicoPageModule {}
