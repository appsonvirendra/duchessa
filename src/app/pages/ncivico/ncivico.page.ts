import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ConfigApp } from '../../shared/services/config-app.service';

@Component({
  selector: 'app-ncivico',
  templateUrl: './ncivico.page.html',
  styleUrls: ['./ncivico.page.scss'],
})
export class NcivicoPage implements OnInit {

  civico: string = ""
  error: boolean = false;

  constructor(private modalCtrl: ModalController, public config: ConfigApp) { 
  }

  ngOnInit() {
  }


  dismiss(data: any) {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss(data);
  }

  onFormClicked() {
    this.error = this.civico.length == 0
    if (!this.error) {
      this.dismiss({'result': this.config.RESULT_OK, 'civico': this.civico});
    }    
  }

}
