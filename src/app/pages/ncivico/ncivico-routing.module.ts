import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NcivicoPage } from './ncivico.page';

const routes: Routes = [
  {
    path: '',
    component: NcivicoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NcivicoPageRoutingModule {}
