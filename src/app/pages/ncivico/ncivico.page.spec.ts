import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NcivicoPage } from './ncivico.page';

describe('NcivicoPage', () => {
  let component: NcivicoPage;
  let fixture: ComponentFixture<NcivicoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NcivicoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NcivicoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
