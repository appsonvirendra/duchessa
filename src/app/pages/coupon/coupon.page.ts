import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ApiService } from '../../shared/services/api.service';
import { ConfigApp } from '../../shared/services/config-app.service';
import { ToastService } from '../../shared/services/toast.service';
import { AlertService } from '../../shared/services/alert.service';

@Component({
  selector: 'app-coupon',
  templateUrl: './coupon.page.html',
  styleUrls: ['./coupon.page.scss'],
})
export class CouponPage implements OnInit {

  item: any = {codice: ""};
  showCouponMessage: boolean = false;
  ordineminimo: number = 0;
  toReturn: any = {result: this.config.JPS_ERROR, codice: ""}

  constructor(private api: ApiService, public config: ConfigApp, private modalCtrl: ModalController, private alertService: AlertService, private toastService: ToastService) { }

  ngOnInit() {
    let cart = localStorage.getItem("cart");
    if (cart) {
      let cartData = JSON.parse(cart);
      this.toReturn.codice = cartData.order.coupon;
      this.ordineminimo = cartData.order.ordineminimo;
      this.showCouponMessage = this.toReturn.codice != "" && cartData.order.euro_coupon > 0;
      this.item.orderid = cartData.order_id;
      this.item.costcart = cartData.order.costcart;
      this.item.euro_delivery = cartData.order.euro_delivery;
    }
  }

  dismiss() {
    this.modalCtrl.dismiss(this.toReturn);
  }

  onApplyCouponClicked() {  
    if(this.item.codice.length > 0){
      this.api.postj("order/applycoupon", this.item, "").then(res => {
        if (res[this.config.JP_STATUS] == this.config.JPS_OK) {
          this.showCouponMessage = true;         
          this.alertService.showAlert("", res[this.config.JP_MSG]);
          this.toReturn.result = this.config.RESULT_OK;
          this.toReturn.codice = this.item.codice
        } 
        else {
          console.log(res);
          this.alertService.showAlert("Attenzione!", res[this.config.JP_MSG]);
        }
      }, err => {
        console.log(err);
      });
    }
    else this.toastService.presentToast("Codice coupon necessario!")
  }
  
  onRemoveCouponClicked() {  
    this.api.postj("order/removecoupon", {orderid: this.item.orderid}, "").then(res => {
      if (res[this.config.JP_STATUS] == this.config.JPS_OK) {
        this.showCouponMessage = false;
        this.toReturn.result = this.config.RESULT_OK;
        this.toReturn.codice = ""
        let cartData = res[this.config.JP_RESULT];
        localStorage.setItem("cart", JSON.stringify(cartData));
        this.alertService.showAlert("", "Coupon rimosso correttamente.");
      } 
      else console.log(res);
    }, err => {
      console.log(err);
    });
  }

}
