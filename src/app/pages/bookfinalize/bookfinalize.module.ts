import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BookfinalizePageRoutingModule } from './bookfinalize-routing.module';

import { BookfinalizePage } from './bookfinalize.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BookfinalizePageRoutingModule
  ],
  declarations: [BookfinalizePage]
})
export class BookfinalizePageModule {}
