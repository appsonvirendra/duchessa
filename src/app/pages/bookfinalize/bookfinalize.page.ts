import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { DB } from '../../shared/services/env.service';
import { BookingService } from '../../shared/services/booking.service';
import { TypePayment } from '../../shared/services/config-app.service';
import { AppInfo } from '../../shared/services/env.service';

@Component({
  selector: 'app-bookfinalize',
  templateUrl: './bookfinalize.page.html',
  styleUrls: ['./bookfinalize.page.scss'],
})
export class BookfinalizePage implements OnInit {

  bookData: any = {};
  cartBookData: any = {};
  payType: any = TypePayment;
  items: Array<any> = [];
  appInfo: any = AppInfo;

  constructor(private router: Router, private platform: Platform) { 
    let booktemp = localStorage.getItem(DB.BOOK);
    if(booktemp) this.bookData = JSON.parse(booktemp);
    let carttemp = localStorage.getItem(DB.CARTBOOK);
    if(carttemp) {
      this.cartBookData = JSON.parse(carttemp);
      this.toArray();
    }
    this.platform.backButton.subscribe(() => {
      this.goToHome()
    });
  }

  ngOnInit() {
  }

  withMenu(){
    return this.cartBookData.count > 0;
  }

  goToHome(){
    console.log("gohome");
    localStorage.removeItem(DB.BOOK);
    localStorage.removeItem(DB.CARTBOOK);
    this.router.navigateByUrl('/');
  }

  toArray(){
    if(this.cartBookData.order && this.cartBookData.order.items){
      this.items = Object.keys(this.cartBookData.order.items).map(key => ({type: key, value: this.cartBookData.order.items[key]}));
    }
  }
}
