import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BookfinalizePage } from './bookfinalize.page';

describe('BookfinalizePage', () => {
  let component: BookfinalizePage;
  let fixture: ComponentFixture<BookfinalizePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookfinalizePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BookfinalizePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
