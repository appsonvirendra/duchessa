import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BookfinalizePage } from './bookfinalize.page';

const routes: Routes = [
  {
    path: '',
    component: BookfinalizePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BookfinalizePageRoutingModule {}
