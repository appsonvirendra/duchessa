import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { ConfigApp } from '../../shared/services/config-app.service';
import { AlertController, ModalController } from '@ionic/angular';
import { AppInfo } from '../../shared/services/env.service';

@Component({
  selector: 'app-recovery',
  templateUrl: './recovery.page.html',
  styleUrls: ['./recovery.page.scss'],
})
export class RecoveryPage implements OnInit {

  step: number = 0;
  err: string = "";
  item: any = {email: "", code: "", password: "", id: -1}
  private regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  constructor(private modalCtrl: ModalController, private api: ApiService, public config: ConfigApp, private alertCtrl: AlertController) { }

  ngOnInit() {
  }

  dismiss(data: any) {
    this.modalCtrl.dismiss(data);
  }

  async onFormClicked(){
    switch(this.step){
      case 0: 
        if(this.item.email.length > 0 && this.regex.test(this.item.email)){
          this.err = "Email errata!"
          await this.api.postj("user/recovery/request", {email: this.item.email}, "").then(res => {
            console.log(res);
            if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {
              this.step = 1;
              this.item.id = res[this.config.JP_RESULT]['id'];
              this.err = ""
            }
          })
        }
        else if(this.item.email.length == 0) this.err = "Inserisci l'email!"
        else this.err = "Email non valida!"
        break;
      case 1: 
        if(this.item.id > 0 && this.item.code.length == 8){
          this.err = "Codice di recupero password errato!"
          await this.api.postj("user/recovery/checkcode", {id: this.item.id, code: this.item.code}, "").then(res => {
            console.log(res);
            if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {
              this.step = 2;
              this.err = ""
            }
          })
        }
        else this.err = "Inserisci il codice di recupero password!"
        break;
      case 2: 
        if(this.item.id > 0 && this.item.password.length >= 6){
          this.err = "Controlla i dati inseriti!"
          await this.api.postj("user/recovery/reset", this.item, "").then(res => {
            console.log(res);
            if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {
              this.err = ""
              this.showAlert()
            } 
            else if (res[this.config.JP_STATUS] == this.config.JPS_ERROR && res[this.config.JP_MSG]){     
              this.err = "<ul>";
              res[this.config.JP_MSG].forEach(msg => {
                this.err += "<li>" + msg + "</li>";
              });   
              this.err +="</ul>";
            }
          })
        }
        else this.err = "La password deve contenere almeno 6 caratteri!"
        break;
    }
    if(this.err.length > 0) this.showErrorAlert()
  }

  async showErrorAlert(){
    let alert = await this.alertCtrl.create({
      header: AppInfo.APP_NAME,
      subHeader: "Attenzione!",
      message: this.err,
      buttons: [{text: 'OK', role: 'cancel', handler: () => {console.log('OK clicked')}}]
    });
    await alert.present();
  }

  async showAlert(){
    let alert = await this.alertCtrl.create({
      header: AppInfo.APP_NAME,
      subHeader: "Password modificata con successo!",
      message: "Torna al login!",
      buttons: [{text: 'OK', role: 'cancel', handler: () => {this.dismiss({'result': this.config.RESULT_OK})}}]
    });
    await alert.present();
  }
}
