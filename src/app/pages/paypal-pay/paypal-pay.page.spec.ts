import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PaypalPayPage } from './paypal-pay.page';

describe('PaypalPayPage', () => {
  let component: PaypalPayPage;
  let fixture: ComponentFixture<PaypalPayPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaypalPayPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PaypalPayPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
