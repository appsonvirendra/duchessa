import { Component, OnInit, Input } from '@angular/core';
import { ConfigApp, TypePayment } from '../../shared/services/config-app.service';
import { ApiService } from '../../shared/services/api.service';
import { ModalController } from '@ionic/angular';
import { EitUrl } from '../../shared/services/env.service';
import { AlertService } from '../../shared/services/alert.service';
import {
  IPayPalConfig,
  ICreateOrderRequest 
} from 'ngx-paypal';


@Component({
  selector: 'app-paypal-pay',
  templateUrl: './paypal-pay.page.html',
  styleUrls: ['./paypal-pay.page.scss'],
})
export class PaypalPayPage implements OnInit {

  public payPalConfig ? : IPayPalConfig;

  @Input() cart: any = null;
  item: any = {}

  constructor(private api: ApiService, private modalCtrl: ModalController, public config: ConfigApp, private alertService: AlertService) { 
    
   
  }

  ngOnInit() {
    this.initConfig();
  }

  private initConfig(): void {

    var tot = (this.cart.order.euro_total).toFixed(2);

    this.payPalConfig = {
        currency: 'EUR',
        clientId: this.cart.PYKR,
        createOrderOnClient: (data) => < ICreateOrderRequest > {
            "intent": "CAPTURE",
            "purchase_units": [{
                "amount": {
                    "currency_code": 'EUR',
                    "value": String(tot),
                },
               
            }]
        },
        advanced: {
            commit: 'true'
        },
        style: {
            label: 'paypal',
            layout: 'vertical'
        },
        onApprove: (data, actions) => {
            console.log('onApprove - transaction was approved, but not authorized', data, actions);
            actions.order.get().then(details => {
                console.log('onApprove - you can get full order details inside onApprove: ', details);
            });

        },
        onClientAuthorization: (data) => {
            console.log('onClientAuthorization - you should probably inform your server about completed transaction at this point', data);
            //this.showSuccess = true;
            var response = { state:"approved", id:data.id};
            this.dismiss({'result':this.config.RESULT_OK, "response":response});
           
        },
        onCancel: (data, actions) => {
            console.log('OnCancel', data, actions);
            //this.showCancel = true;
            this.dismiss({'result':this.config.RESULT_CANCEL, "data":null});

        },
        onError: err => {
            console.log('OnError', err);
           // this.showError = true;
        },
        onClick: (data, actions) => {
            console.log('onClick', data, actions);
            //this.resetStatus();
        },
    };
}

  
  dismiss(data: any) {
    this.modalCtrl.dismiss(data);
  }
}
