import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgxPayPalModule } from 'ngx-paypal';
import { IonicModule } from '@ionic/angular';

import { PaypalPayPageRoutingModule } from './paypal-pay-routing.module';

import { PaypalPayPage } from './paypal-pay.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PaypalPayPageRoutingModule,
    NgxPayPalModule,
  ],
  declarations: [PaypalPayPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PaypalPayPageModule {}
