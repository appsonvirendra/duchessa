import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CashPayPageRoutingModule } from './cash-pay-routing.module';

import { CashPayPage } from './cash-pay.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CashPayPageRoutingModule
  ],
  declarations: [CashPayPage]
})
export class CashPayPageModule {}
