import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CashPayPage } from './cash-pay.page';

describe('CashPayPage', () => {
  let component: CashPayPage;
  let fixture: ComponentFixture<CashPayPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashPayPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CashPayPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
