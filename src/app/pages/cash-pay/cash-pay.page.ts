import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ConfigApp } from '../../shared/services/config-app.service';

@Component({
  selector: 'app-cash-pay',
  templateUrl: './cash-pay.page.html',
  styleUrls: ['./cash-pay.page.scss'],
})
export class CashPayPage implements OnInit {

  @Input() order: any = {};

  constructor(private modalCtrl: ModalController, public config: ConfigApp) { }

  ngOnInit() {
  }

  onComplete(){
    this.dismiss({'result':this.config.JPS_OK});
  }

  dismiss(data: any) {
    this.modalCtrl.dismiss(data);
  }

}
