import { NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CustomMenuItemPageRoutingModule } from './custom-menu-item-routing.module';

import { CustomMenuItemPage } from './custom-menu-item.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CustomMenuItemPageRoutingModule
  ],
  declarations: [CustomMenuItemPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CustomMenuItemPageModule {}
