import { Component, OnInit, Input } from '@angular/core';
import { ConfigApp, EtTipoCustom } from '../../shared/services/config-app.service';
import { ModalController } from '@ionic/angular';
import { ToastService } from '../../shared/services/toast.service';
import { ApiService } from '../../shared/services/api.service';
import { AppInfo } from '../../shared/services/env.service';
import { CartService } from '../../shared/services/cart.service';

@Component({
  selector: 'app-custom-menu-item',
  templateUrl: './custom-menu-item.page.html',
  styleUrls: ['./custom-menu-item.page.scss'],
})
export class CustomMenuItemPage implements OnInit {

  @Input() item: any = null;
  @Input() parent: any = null;
  @Input() cartData : any;
  @Input() isBook : boolean;
  headerImage: any = "";
  notes: any = {text : "", show: false};
  TipoCustom: any = EtTipoCustom;
  result: any;
  appInfo: any = AppInfo;
  endPrice: number = 0;

  constructor(private api: ApiService, public cartService: CartService, public config: ConfigApp, private modalCtrl: ModalController, private toastService: ToastService) { }

  ngOnInit() {
    console.log("Custom")
    this.initProduct()
  }

  dismiss(data: any) {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss(data);
  }

  toggleGroup(group: any, type: number) {
    if (event) {
      event.stopPropagation();
    }
    if(this.result.joins.length > 0){
      this.result.joins.forEach((join: any) => {
        if(join.id != group.id) join.show = false;
      });
      group.show = !group.show;
      if(type == 0) this.notes.show = false;
    }
    else{
      group.show = !group.show;
      if(type == 0) this.notes.show = false;
      else this.result.show = false;
    }    
  }

  isChecked(detIngredient: any, index: number){
    let jn = this.result.joins[index]; 
    let idDet = -1;
    for(let i = 0; i < jn.details.length && idDet == -1; i++) {
      idDet = jn.details[i].id == detIngredient.id ? i : idDet;
    }
    return idDet;
  }

  checkDetail(detIngredient: any, index: number, newcust: boolean){
    let idDet = -1;
    let jn = this.result.joins[index];
    
    for(let i = 0; i < jn.details.length && idDet == -1; i++) {
      idDet = jn.details[i].id == detIngredient.id ? i : idDet;
    }
    if(idDet >= 0){
      //ho già selezionato il dettaglio => lo tolgo
      jn.details.splice(idDet, 1);  
      this.endPrice -= detIngredient.price    
    }
    else{
      //il dettaglio non è ancora stato selezionato => lo aggiungo
      if(newcust){
        let dt = {id: detIngredient.id, cd_descr: detIngredient.cd_descr, price: detIngredient.price, attivo: detIngredient.attivo, checked: detIngredient.checked, newcust: newcust};
        jn.details.push(dt);
      }
      else{
        jn.details.push(detIngredient);
      }
      this.endPrice += detIngredient.price    
    }
  }

  updateSelection(pos: number, joinR: any, index: number){
    if(this.result.joins[index].details.length) this.endPrice -= this.result.joins[index].details[0].price;
    this.result.joins[index].details = []; 
    if(!joinR.newcust){
      this.result.joins[index].details.push(this.item.joins[index].details[pos]);  
      this.endPrice += this.item.joins[index].details[pos].price 
    }
    else{
      this.result.joins[index].details.push(joinR.menudetaillist[pos]);
      this.endPrice += joinR.menudetaillist[pos].price 
    }
  }

  isCheckedR(id_det: number, index: number){
    return this.result.joins[index].details.length > 0 && this.result.joins[index].details[0].id == id_det; 
  }

  getPath(){
    return AppInfo.ASSETS_URL + (!this.parent && this.item.media_id > 0 ? this.item.media.filename_small : (this.parent && this.parent.media_id > 0 ? this.parent.media.filename_small : ""))
  }

  initProduct(){
    this.endPrice = this.item.price;
    this.result = {id : this.item.id, qty : this.item.qty, joins : [], details : [], notes : "", typecustom: this.item.typecustom};
    if(this.item.joins.length > 0){
      this.item.joins.forEach((join: any) => {
        var jn = {
                    id: join.id, 
                    id_custom: join.id_custom, 
                    typecustom : join.typecustom, 
                    details : [], 
                    min: join.typecustom == this.TipoCustom.CHECKLIST && join.min ? join.min : -1, 
                    max: join.typecustom == this.TipoCustom.CHECKLIST && join.max ? join.max : -1, 
                    required : join.required}; 
        if(join.typecustom == this.TipoCustom.MENU){
          join.details.forEach(det => {
            var temp = {descrcd: det.descrcd, selected: null, required: det.required};
            jn.details.push(temp);
          });
        }
        this.result.joins.push(jn);
      });
      this.toggleGroup(this.result.joins[0], 0);
    }
    else{
      if(this.item.typecustom == this.TipoCustom.MENU){
        this.item.details.forEach((det: any) => {
          var temp = {descrcd: det.descrcd, selected: null, required: det.required};
          this.result.details.push(temp);
        });
      }
      this.toggleGroup(this.result, 0);
    }
  }
  
  addToCartModal(){
    var msg = "";
    this.result.notes = this.notes.text;
    if(this.result.joins.length > 0){
      this.result.joins.forEach((join: any) => {
        if(msg.length == 0){
          switch(join.typecustom){
            case this.TipoCustom.CHECKLIST:
              if(join.min > 0 && join.details.length < join.min){
                msg = 'Seleziona almeno ' + join.min +' opzion' + (join.min == 1 ? 'e' : 'i') +'!';
              }
              else if(join.max > 0 && join.details.length > join.max){
                msg = 'Hai selezionato troppe opzioni! (max ' + join.max +')';
              }
              else if(join.required && join.details.length == 0){
                msg = 'Attenzione! Completare tutte le personalizzazioni obbligatorie!';
              }
              break;
            case this.TipoCustom.MENU:
              join.details.forEach(det => {
                if((join.required || det.required) && det.selected == null){
                  msg = "Attenzione! Completare tutte le personalizzazioni obbligatorie!";
                }              
              });
              break;
            case this.TipoCustom.RADIOLIST:
              msg = join.required && join.details.length == 0 ? "Attenzione! Completare tutte le personalizzazioni obbligatorie!" : msg;
              break;
          }  
        }  
      });  
    }
    else if(this.result.typecustom == this.TipoCustom.MENU){
      this.result.details.forEach(det => {
        msg = det.selected == null || det.selected.length == 0 ? "Attenzione! Non è possibile aggiungere il prodotto! Scegliere tutte le opzioni!" : msg;
      });
    }
    if(msg.length) {
      this.toastService.presentToast(msg)
    }
    else{
      if(!this.isBook){
        this.cartService.addToCart(this.result.id, this.result.qty, this.result.notes, 1, this.result);
        this.dismiss(null);
      }
      else this.dismiss({productid: this.result.id, qty: this.result.qty, notes: this.result.notes, returndata: 1, result: this.result});
    }    
  }

  addQty(){
    this.result.qty++;
  }

  removeQty(){
    if(this.result.qty > 1){
      this.result.qty--;
    }
  }
}
