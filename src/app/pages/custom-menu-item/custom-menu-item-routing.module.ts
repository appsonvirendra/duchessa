import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CustomMenuItemPage } from './custom-menu-item.page';

const routes: Routes = [
  {
    path: '',
    component: CustomMenuItemPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CustomMenuItemPageRoutingModule {}
