import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CustomMenuItemPage } from './custom-menu-item.page';

describe('CustomMenuItemPage', () => {
  let component: CustomMenuItemPage;
  let fixture: ComponentFixture<CustomMenuItemPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomMenuItemPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CustomMenuItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
