import { Component, OnInit } from '@angular/core';
import { RESPONSE } from '../../shared/services/env.service';
import { ApiService } from '../../shared/services/api.service';
import { AlertService } from '../../shared/services/alert.service';
import { ToastService } from '../../shared/services/toast.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-fidelity',
  templateUrl: './fidelity.page.html',
  styleUrls: ['./fidelity.page.scss'],
})
export class FidelityPage implements OnInit {

  formData: any = {}
  n: number = 0;
  max: number = 1000;
  couponAvailable: boolean = false;

  constructor(private api: ApiService, private alertService: AlertService, private toastService: ToastService, private location: Location) { }

  ngOnInit() {
    this.fill();
  }

  ionViewWillEnter(){
    this.fill();
  }

  fill(){
    this.api.postj("user/settings/infofidelity", {}, "").then(res => {
      console.log(res);
      if (res[RESPONSE.JP_STATUS] == RESPONSE.JPS_OK && res[RESPONSE.JP_RESULT]) {
        this.formData = res[RESPONSE.JP_RESULT];
        this.getInfoApp()
      }
    })
  }

  downloadCoupon(){
    this.api.postj("user/settings/generatecoupon", {}, "").then(res => {
      console.log(res);
      if (res[RESPONSE.JP_STATUS] == RESPONSE.JPS_OK && res[RESPONSE.JP_RESULT]) {
        this.toastService.presentToast("Il tuo coupon è stato generato e inviato sulla tua mail!")
        this.fill()
      }
    })
  }

  convert(){
    let btns = [{text: 'Sì', handler: () => {this.confirmConversion()}}, {text: 'No', role: 'cancel'}]
    this.alertService.showAlert("Conversione punti", "Sicuro di voler convertire i tuoi punti in credito?", btns);
  }

  confirmConversion(){
    this.api.postj("credit-transactions/converteatpoints", {ep: this.formData.eatpoints}, "").then(res => {
      console.log(res);
      if (res[RESPONSE.JP_STATUS] == RESPONSE.JPS_OK && res[RESPONSE.JP_RESULT]) {
        this.toastService.presentToast("I tuoi punti sono stati convertiti in credito!")
        this.fill()
      }
    })
  }

  getInfoApp(){
    this.api.postj("apps/getinfoapp", "", "").then(res => {
      console.log(res);
      if (res[RESPONSE.JP_STATUS] == RESPONSE.JPS_OK && res[RESPONSE.JP_RESULT]) {
        this.max = res[RESPONSE.JP_RESULT].conversion_index;
        this.n = this.formData.eatpoints <= this.max ? this.max - this.formData.eatpoints : 0;
        this.couponAvailable = this.n == 0;
      }
    })
  }

  goBack() {
    this.location.back();
  }  

}
