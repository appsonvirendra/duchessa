import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { ConfigApp, TypeOrder, TypePayment } from '../../shared/services/config-app.service';
import { LoadingService } from '../../shared/services/loading.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-profile-orders',
  templateUrl: './profile-orders.page.html',
  styleUrls: ['./profile-orders.page.scss'],
})
export class ProfileOrdersPage implements OnInit {

  orders : Array<any> = [];
  queryPage: any = 0;
  queryPageSize: any = 5;
  payType: any = TypePayment;
  typeOrder: any = TypeOrder;

  constructor(private api: ApiService, public config: ConfigApp, private loadingService: LoadingService, private location: Location) { }

  ngOnInit() {
    this.fillOrders()
  }

  async fillOrders(){
    await this.loadingService.show();
    let params = {PAGE : this.queryPage, PAGESIZE : this.queryPageSize};
    this.api.postj("user/settings/orders", params, "").then(res => {
      console.log(res)
      if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {
        let tmparr = res[this.config.JP_RESULT];
        if (typeof tmparr !== 'undefined' && tmparr != null && tmparr.length > 0){
          if (this.orders.length == 0) this.orders = tmparr;
          else this.orders = this.orders.concat(tmparr); 
        }
      }
      else {
        console.log(res);
      }
      this.loadingService.hide();
    }, (err) => {
      console.log(err);
      this.loadingService.hide();
    })
  }

  nextclick(){
    this.queryPage ++;
    this.fillOrders();
  }
  
  toggleGroup(group: any) {
    if (event) {
      event.stopPropagation();
    }
    group.show = !group.show;
  }

  goBack() {
    this.location.back();
  }  

}
