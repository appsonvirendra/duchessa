import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfileOrdersPageRoutingModule } from './profile-orders-routing.module';

import { ProfileOrdersPage } from './profile-orders.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfileOrdersPageRoutingModule
  ],
  declarations: [ProfileOrdersPage]
})
export class ProfileOrdersPageModule {}
