import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConvertpointsPageRoutingModule } from './convertpoints-routing.module';

import { ConvertpointsPage } from './convertpoints.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConvertpointsPageRoutingModule
  ],
  declarations: [ConvertpointsPage]
})
export class ConvertpointsPageModule {}
