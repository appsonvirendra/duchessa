import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ConvertpointsPage } from './convertpoints.page';

describe('ConvertpointsPage', () => {
  let component: ConvertpointsPage;
  let fixture: ComponentFixture<ConvertpointsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConvertpointsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ConvertpointsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
