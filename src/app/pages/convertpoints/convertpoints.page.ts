import { Component, OnInit, Input } from '@angular/core';
import { ConfigApp } from '../../shared/services/config-app.service';
import { ApiService } from '../../shared/services/api.service';
import { ModalController } from '@ionic/angular';
import { AlertService } from '../../shared/services/alert.service';
import { ToastService } from '../../shared/services/toast.service';

@Component({
  selector: 'app-convertpoints',
  templateUrl: './convertpoints.page.html',
  styleUrls: ['./convertpoints.page.scss'],
})
export class ConvertpointsPage implements OnInit {

  convEp : any = {ep : 0, max : 0};
  @Input() useraccount: any;

  constructor(private api: ApiService, private modalCtrl: ModalController, public config: ConfigApp, private alertService: AlertService, private toastService: ToastService) {
    this.convEp.max = this.useraccount.eatpoints; 
  }

  ngOnInit() {
  }

  convert(){
    if(this.convEp.ep <= this.convEp.max && this.convEp.ep > 0 && this.convEp.ep == parseInt(this.convEp.ep, 10)){
      this.api.postj("credit-transactions/converteatpoints", this.convEp, "").then(res => {
        if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {        	 
          this.toastService.presentToast("Punti fedeltà convertiti con successo!")
          this.useraccount.eatpoints = res[this.config.JP_RESULT]['eatpoints'];
          this.useraccount.credit = res[this.config.JP_RESULT]['credit'];
          this.dismiss({'result': this.config.RESULT_OK});
        }
        else if (res[this.config.JP_STATUS] == this.config.JPS_ERROR && res[this.config.JP_MSG]) {
          res[this.config.JP_MSG].forEach(msg => {
            this.toastService.presentToast(msg)
          });
        }
        else {
          console.log(res);
        }
      }, (err) => {
        console.log(err);
      })
    }
    else{
      let msg = "";
      if(this.convEp.ep > this.convEp.max){
        msg = "Hai inserito un numero di punti fedeltà superiore al tuo saldo attuale!";
      }
      else if(this.convEp.ep <= 0){
        msg = "Puoi convertire minimo 1 punto fedeltà!";
      }
      else if(this.convEp.ep != parseInt(this.convEp.ep, 10)){
        msg = "Puoi convertire solo un numero intero di punti fedeltà!";
      }
      this.toastService.presentToast(msg)
    }
  }

  dismiss(data: any) {
    this.modalCtrl.dismiss(data);
  }

}
