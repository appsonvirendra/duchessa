import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConvertpointsPage } from './convertpoints.page';

const routes: Routes = [
  {
    path: '',
    component: ConvertpointsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConvertpointsPageRoutingModule {}
