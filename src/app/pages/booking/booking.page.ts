import { ChangeDetectorRef, Component, NgZone, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AppInfo, DB } from '../../shared/services/env.service';
import { Book } from '../../shared/classes/book';
import { ApiService } from '../../shared/services/api.service';
import { ConfigApp, TypeOrder } from '../../shared/services/config-app.service';
import { LoadingService } from '../../shared/services/loading.service';
import { CustomMenuItemPage } from '../custom-menu-item/custom-menu-item.page';
import { ToastService } from '../../shared/services/toast.service';
import { CartbookPage } from '../cartbook/cartbook.page';
import { AlertService } from 'src/app/shared/services/alert.service';
import { Router } from '@angular/router';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Location } from '@angular/common';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.page.html',
  styleUrls: ['./booking.page.scss'],
})
export class BookingPage implements OnInit {

  menu: any = {categories: []};
  categories: any = [];
  selectedCat : any;
  restaurantBase: any;
  restaurantFull: any = null;
  bookData: any = {};
  cartBookData: any = {};
  typeOrder: any = TypeOrder;
  //isBtEnabled: boolean = false;
  isActive : boolean;
  appInfo: any = AppInfo;
  enabledWithMenu : boolean = false;

  date: Date = new Date();
  daysInThisMonth: any;
  daysInLastMonth: any;
  daysInNextMonth: any;
  week_days: Array<string> = ['Dom', 'Lun', 'Mar', 'Mer', 'Gio', 'Ven', 'Sab']
  monthNames: Array<string> =  ["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"];
  currentMonth: any;
  currentYear: any;
  currentDate: any;
  currentM: any;

  fasce: Array<any> = [];
  fascepranzo: Array<any> = [];
  fascecena: Array<any> = [];

  timeselected : any;
  oraprenotazione : any; 
  numpeopleselected : any;  
  numpeoplearray : Array<any> = [];

  currentTab: number = 0;

  constructor(private api: ApiService, public config: ConfigApp, private loadingService: LoadingService, private modalController: ModalController, 
              private cd: ChangeDetectorRef, private toastService: ToastService, private alertService: AlertService, private router: Router, 
              private nativeStorage: NativeStorage, public zone: NgZone, private location: Location) { 
    let booktemp = localStorage.getItem(DB.BOOK);
    this.bookData = booktemp ? JSON.parse(booktemp) : new Book();

    if (JSON.parse(localStorage.getItem(DB.RESTAURANT))) {
      this.restaurantBase = JSON.parse(localStorage.getItem(DB.RESTAURANT));
      this.enabledWithMenu = this.restaurantBase.abilitamenupertavoli;
      console.log("Abilita menu per tavoli: " + this.enabledWithMenu);
    }
    this.getDaysOfMonth();
  }

  ngOnInit() {    
    this.fillMenu(); 
    this.fill();
  }

  dateIsOk(){
    let today = new Date();
    return this.currentDate && (this.currentM > today.getMonth()+1 || this.currentYear > today.getFullYear() || this.currentDate >= today.getDate());
  }

  selectTab(index: number){
    let res = "";
    switch(index){
      case 1: res = this.dateIsOk() ? "" : "Seleziona un giorno!"; break;
      case 2: res = this.dateIsOk() && this.timeselected && this.timeselected != "" ? "" : (this.currentDate ? "Seleziona un orario!" : "Seleziona un giorno!"); break;
    }
    if (res.length > 0) this.toastService.presentToast(res);
    else this.currentTab = index;
  }

  goOn(){
    this.currentTab++;
  }

  async goToCart(){
    let modal = await this.modalController.create({component: CartbookPage, componentProps: {}});
    (await modal).onDidDismiss().then((result) => {    
      var cart = localStorage.getItem(DB.CARTBOOK);
      if (cart) {
        this.cartBookData = JSON.parse(cart);
      }
      if (result && result.data.result == this.config.RESULT_OK) this.booknow();
    });
    return await modal.present();
  }

  goToCheckout(){
    this.booknow();
  }

  getTitle(){
    let res = "";
    switch(this.currentTab){
      case 0: res = "Scegli il giorno"; break;
      case 1: res = "Scegli l'orario"; break;
      case 2: res = "Per quante persone?"; break;
      case 3: res = "Menù"; break;
    }
    return res;
  }

  isInPast(day: number){
    let today = new Date();
    return this.date.getMonth() <= today.getMonth() && this.date.getFullYear() <= today.getFullYear() && day < today.getDate()
  }

  isToday(day: number){
    let today = new Date();
    return this.date.getMonth() == today.getMonth() && this.date.getFullYear() == today.getFullYear() && day == today.getDate()
  }

  isSelected(day: number){
    return this.date.getMonth() +1 == this.currentM && this.date.getFullYear() == this.currentYear && day == this.currentDate
  }

  withMenu(){
    return this.cartBookData.count > 0;
  }

  isBtEnabled(): boolean{
    let res = false;
    switch(this.currentTab){
      case 0: res = this.dateIsOk(); break;
      case 1: res = this.dateIsOk() && this.timeselected && this.timeselected != ""; break;
      case 2: res = this.dateIsOk() && this.timeselected != "" && this.numpeopleselected > 0; break;
      case 3: res = this.dateIsOk() && this.timeselected && this.timeselected != "" && this.numpeopleselected > 0; break;
    }
    return res;
  }

  booknow(){
    //verifico prima che ci siano tutti i dati e poi chiedo la conferma con il popup di riepilogo:    
    if (this.oraprenotazione && this.timeselected && this.numpeopleselected > -1){      
      let today = new Date();
      let bookingdate = new Date(this.oraprenotazione);      
      if (today.getTime() > bookingdate.getTime()) this.alertService.showAlert('Attenzione!', "Non è possibile prenotare per un giorno precedente ad oggi!");   
      else{
        this.nativeStorage.getItem(DB.USER).then(data =>{
          this.zone.run(() => {
            console.log(data)
            let params: any = {idbook: this.bookData.book_id, oraprenotazione: this.oraprenotazione, hour: this.timeselected, 
                               n_coperti: this.numpeopleselected, idrest: this.restaurantBase.id, idmaster: data.user_id}; 
            //aggiornamento dei dati impostati da utente, compreso le  info di login
            this.api.postj("booking/bookcheckout", params, "").then(res => {          
              if (res[this.config.JP_STATUS] == this.config.JPS_OK) {                
                this.bookData = res[this.config.JP_RESULT];
                localStorage.setItem(DB.BOOK, JSON.stringify(this.bookData));
                //... e sono stati convalidati              
                this.router.navigate(['/bookcheckout']);
              }
              //l'utente potrebbe aver gia effettuato una prenotazione, o i posti essere finiti nel frattempo...
              else this.alertService.showAlert('Attenzione!', res[this.config.JP_MSG]); 
            });            
          });
        }, () =>{
          this.router.navigate(['/login']);
        })
      }      
    }
    else if (this.oraprenotazione == undefined || this.oraprenotazione == ''){
      this.alertService.showAlert('Attenzione!', "Definire il giorno di prenotazione!");
    }
    else if (this.timeselected == undefined || this.timeselected == ''){
      this.alertService.showAlert('Attenzione!', "Definire l'orario di prenotazione!");
    }
    else if (this.numpeopleselected == undefined || this.numpeopleselected < 0){
      this.alertService.showAlert('Attenzione!', "Definire il numero di coperti!");
    }   
  }

  fillMenu() {
    this.loadingService.show();
    this.api.postj("restaurant/menu", this.restaurantBase.menu, "").then(res => {
      console.log(res);
      this.loadingService.hide();
      if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {
        this.menu = res[this.config.JP_RESULT];
        this.categories = Array.of(this.menu.categories);
        this.cd.detectChanges()
        this.selectedCat = 0
      }
      else {
        console.log(res);
      }
    },err => {
      console.log(err);
      this.loadingService.hide();
    });
  }

  fill() {
    this.api.postj("restaurant/booking", {rest_id: this.restaurantBase.id, book_id: this.bookData.book_id}, "").then(res => {
      console.log(res);
      if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {
          this.restaurantFull = res[this.config.JP_RESULT]["r"];
          this.bookData = res[this.config.JP_RESULT]["o"];  
          this.cartBookData = this.bookData.book.ordine;
          console.log("ID Ordine Book: " + this.cartBookData.order_id);
          localStorage.setItem(DB.BOOK, JSON.stringify(this.bookData));
          localStorage.setItem(DB.CARTBOOK, JSON.stringify(this.cartBookData));
          localStorage.setItem(DB.RESTAURANT_FULL, JSON.stringify(this.restaurantFull));
          //aggiornamento dati
          this.fillDataBook();
        }
      }, err => {
        console.log(err);
      }
    );
  }

  getDaysOfMonth() {
    this.daysInThisMonth = new Array();
    this.daysInLastMonth = new Array();
    this.daysInNextMonth = new Array();
    
    this.currentMonth = this.monthNames[this.date.getMonth()];
    this.currentM = this.date.getMonth() + 1;
    this.currentYear = this.date.getFullYear();    
    this.currentDate = this.date.getMonth() === new Date().getMonth() ? new Date().getDate() : 999;
  
    var firstDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth(), 1).getDay();
    var prevNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth(), 0).getDate();
    for(var i = prevNumOfDays-(firstDayThisMonth-1); i <= prevNumOfDays; i++) {
      this.daysInLastMonth.push(i);
    }
  
    var thisNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth()+1, 0).getDate();
    for (var i = 0; i < thisNumOfDays; i++) {
      this.daysInThisMonth.push(i+1);
    }
  
    var lastDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth()+1, 0).getDay();
    //var nextNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth()+2, 0).getDate();
    for (var i = 0; i < (6-lastDayThisMonth); i++) {
      this.daysInNextMonth.push(i+1);
    }
    var totalDays = this.daysInLastMonth.length+this.daysInThisMonth.length+this.daysInNextMonth.length;
    if(totalDays < 36) {
      for(var i = (7-lastDayThisMonth); i < ((7-lastDayThisMonth)+7); i++) {
        this.daysInNextMonth.push(i);
      }
    }
    this.getDataFromDay();
  }

  goToLastMonth() {
    let today = new Date();
    if(this.date.getFullYear() != today.getFullYear() || this.date.getMonth() != today.getMonth()){
      this.date = new Date(this.date.getFullYear(), this.date.getMonth(), 0);
      this.getDaysOfMonth();
    }
  }

  goToNextMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth()+2, 0);
    this.getDaysOfMonth();
  }

  selectDate(day: any){
    this.timeselected = this.oraprenotazione = ""; 
    this.numpeopleselected = -1;
    this.numpeoplearray = new Array();

    console.log("day: " + day);

    this.currentDate = day;
    this.getDataFromDay();
    //this.isBtEnabled = true;
    this.selectTab(1);
  }

  getDataFromDay(){
    let giorno = this.currentDate;
    let mese = this.currentMonth;
    let anno = this.currentYear;

    let currdate = new Date(this.date.getFullYear(), this.date.getMonth(), giorno);
    
    mese = this.currentM = (currdate.getMonth()+1);
    anno = currdate.getFullYear();

    //formato yyyy-mm-gg

    if(parseInt(giorno)<10)
    {
      giorno = '0'+giorno.toString();
    }
    if(parseInt(mese)<10)
    {
      mese = '0'+mese.toString();
    }

    let day = anno + "-" + mese + "-" + giorno;

    var params: any = {};
    params.IDREST = this.restaurantBase.id;
    params.AUX_FIELD_1 = day.toString(); 

    this.api.postj("booking/getdetailsday", params, "").then(res => {
      // this.loading.dismiss();
      console.log("getdetailsday post");
      if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT] && res[this.config.JP_RESULT].length) {
        console.log(res);
        
        this.fasce = res[this.config.JP_RESULT];

        this.fascepranzo = new Array();
        this.fascecena = new Array();
        for (let kk=0; kk < this.fasce.length; kk++){
          if (this.fasce[kk].pranzo){
            this.fascepranzo.push(this.fasce[kk]);
          }else{
            this.fascecena.push(this.fasce[kk]);
          }
        }

      }
      else {
        console.log(res);
        this.fascepranzo = new Array();
        this.fascecena = new Array();
        this.fasce =  new Array();
      }

    }, (err) => {
      console.log(err);
    });

  }

  fillDataBook(){
    if (this.bookData.book != undefined){
      this.oraprenotazione = this.bookData.book.oraprenotazione;
      var splitteddate = this.bookData.book.book_date.split("-"); 
      if (splitteddate.length == 3){
        this.currentDate = +splitteddate[0];
        this.currentMonth = this.monthNames[+splitteddate[1]-1];
        this.currentYear = +splitteddate[2];

        if (this.bookData.book.n_coperti>0){
          let day = this.bookData.book.book_date;
          var params: any = {};
          params.IDREST = this.restaurantBase.id;
          params.AUX_FIELD_1 = day.toString();       
          this.api.postj("booking/getdetailsday", params, "").then(res => {            
            console.log(res);              
            if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT] && res[this.config.JP_RESULT].length) {
              this.fasce = res[this.config.JP_RESULT];
              this.fascepranzo = new Array();
              this.fascecena = new Array();
              let found = false;
              this.fasce.forEach(f => {
                if (f.pranzo) this.fascepranzo.push(f);
                else this.fascecena.push(f);
                if (this.bookData.book.book_hours == f.orario && !found){
                  this.numpeoplearray = new Array();      
                  if (f.coperti > 0){
                    for(var i = 0; i < f.coperti; i++){
                      this.numpeoplearray.push({num: i+1, posti_rimanenti: f.posti_rimanenti});
                    }
                    this.numpeopleselected = this.bookData.book.n_coperti <= f.coperti ? this.bookData.book.n_coperti : -1;
                  }else{
                    this.numpeopleselected = -1;
                  }
                  
                  this.timeselected = this.bookData.book.book_hours;
                  //this.isBtEnabled = true;
                  found = true;
                }      
              });
            }     
          }, (err) => {
            console.log(err);
          });          
        }
        else this.getDataFromDay();
      }
      else this.getDataFromDay();
    }
    else this.getDataFromDay();
  }

  acceptorario(timeslots: any) {    
    this.timeselected = timeslots.orario;
    this.oraprenotazione = timeslots.mydate; 
    this.numpeopleselected = -1;    
    this.numpeoplearray = new Array();    
    if (timeslots.coperti > 0){
      for(let i = 0; i < timeslots.coperti; i++){
        this.numpeoplearray.push({num: i+1, posti_rimanenti: timeslots.posti_rimanenti});
      }
    }    
    this.selectTab(2);
  }

  acceptnpeople(num: any) {    
    this.numpeopleselected = num;
    if (this.enabledWithMenu){
      this.selectTab(3);
    }
    else{      
      //this.booknow();
    }   
  }
  
  addToCart(menuitem : any, variante: any) { 
    let item = variante == null ? menuitem : variante;
    console.log('add to cart', "ID Ordine:" + this.cartBookData.order_id);
    if (item != null) {
      if (0 == item.custom_type && item.joins.length == 0) {
        this.loadingService.show();
        this.api.postj("cart/addmtoc", {orderid: this.cartBookData.order_id, productid: item.id, returndata: 1}, "").then(res => {
          if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {
            this.cartBookData = res[this.config.JP_RESULT];
            localStorage.setItem(DB.CARTBOOK, JSON.stringify(this.cartBookData));
          } 
          else console.log(res);  
          this.loadingService.hide();
        }, err => {
          console.log(err);
          this.loadingService.hide();
        });
      } 
      else if (0 != item.custom_type || item.joins.length > 0) {
        this.showCustom({item: item, cartData: this.cartBookData, parent: menuitem, isBook: true})
      }
    }
  }

  async showCustom(data: any){    
    let modal = await this.modalController.create({component: CustomMenuItemPage, componentProps: data});
    (await modal).onDidDismiss().then((result) => {
      if ((result.data.result = this.config.RESULT_OK) && result)  this.addToCartWithSubitems(result);
    });  
    return await modal.present();
  }

  addToCartP(item: any) {
    if (item != null) {
      this.loadingService.show();
      this.api.postj( "cart/addptoc", {orderid: this.cartBookData.order_id, productid: item.id, returndata: 1, qty: 1}, "").then(res => {
        if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {
          this.cartBookData = res[this.config.JP_RESULT];
          localStorage.setItem(DB.CARTBOOK, JSON.stringify(this.cartBookData));
        }
        else console.log(res);  
        this.loadingService.hide();
      }, err => {
        console.log(err);
        this.loadingService.hide();
      });
    }
  }

  removeToCartP(item: any) {
    if (item != null) {
      this.loadingService.show();
      this.api.postj( "cart/removemtoc", {orderid: this.cartBookData.order_id, productid: item.id, returndata: 1, qty: 1}, "").then(res => {
        if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {
          this.cartBookData = res[this.config.JP_RESULT];
          localStorage.setItem(DB.CARTBOOK, JSON.stringify(this.cartBookData));
        }
        else console.log(res);  
        this.loadingService.hide();
      }, err => {
        console.log(err);
        this.loadingService.hide();
      });
    }
  }

  addToCartWithSubitems(result: any){
    result.orderid = this.cartBookData.order_id;    
    this.loadingService.show();
    this.api.postj("cart/addmtoc", result, "").then(res => {
      if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {
        this.cartBookData = res[this.config.JP_RESULT];
        localStorage.setItem(DB.CARTBOOK, JSON.stringify(this.cartBookData));
      }
      else console.log(res);  
      this.loadingService.hide();
    }, err => {
      console.log(err);
      this.loadingService.hide();
    });
  }

  shortDescr(item: any) {
    return item.descr.length > 0 && item.descr.length < 15 ? " (" + item.descr + ")" : "";
  }

  longDescr(item: any) {
    return item.descr.length > 15 ? item.descr + " " : "";
  }

  ingredients = function(item: any) {
    var result = "";
    for (var iG = 0; iG < item.ingredients.length; iG++) {
      result += item.ingredients[iG].name;
      if (iG < item.ingredients.length - 1) result += ", ";
    }
    return result;
  }

  goBack() {
    this.location.back();
  }     

}
