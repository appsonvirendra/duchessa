import { Component, OnInit } from '@angular/core';
import { ConfigApp, TypePayment } from '../../shared/services/config-app.service';
import { ApiService } from '../../shared/services/api.service';
import { ModalController } from '@ionic/angular';
import { CashPayPage } from '../cash-pay/cash-pay.page';
import { LoadingService } from '../../shared/services/loading.service';
import { AlertService } from '../../shared/services/alert.service';
import { HTTP } from '@ionic-native/http/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CardPayPage } from '../card-pay/card-pay.page';
import { PaypalPayPage } from '../paypal-pay/paypal-pay.page';
import { DB, EitUrl, Satispay } from '../../shared/services/env.service';
import { CreditPayPage } from '../credit-pay/credit-pay.page';
import { BookingService } from '../../shared/services/booking.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bookcheckpay',
  templateUrl: './bookcheckpay.page.html',
  styleUrls: ['./bookcheckpay.page.scss'],
})
export class BookcheckpayPage implements OnInit {

  bookData: any = {};
  cartBookData: any = {};
  payType: any = TypePayment;
  restaurantBase: any;
  payManaged: boolean = false;
  dataPay: any = {}
  hideCash: boolean = false;
  
  constructor(private api: ApiService, private modalController: ModalController, public config: ConfigApp, private loadingService: LoadingService,
              public http: HTTP, public iab: InAppBrowser, private alertService: AlertService, private router: Router) { 

    let booktemp = localStorage.getItem(DB.BOOK);
    if(booktemp) this.bookData = JSON.parse(booktemp);
    let carttemp = localStorage.getItem(DB.CARTBOOK);
    if(carttemp) {
      this.cartBookData = JSON.parse(carttemp);
      if (this.cartBookData) {
        this.dataPay.restid = this.cartBookData.rest_id;
        this.dataPay.orderid = this.cartBookData.order_id;
        this.dataPay.payment = {};
        this.dataPay.response = {};
      }   
    }
    if (JSON.parse(localStorage.getItem(DB.RESTAURANT))) this.restaurantBase = JSON.parse(localStorage.getItem(DB.RESTAURANT));
  }

  ngOnInit() {
  }

  ionViewWillEnter(){
    let carttemp = localStorage.getItem(DB.CARTBOOK);
    if(carttemp) {
      this.cartBookData = JSON.parse(carttemp);
      if (!this.cartBookData || this.cartBookData.order.status == 1) this.goToHome();
    }
    else this.goToHome();
  }

  goToHome(){
    console.log("gohome");
    localStorage.removeItem(DB.BOOK);
    localStorage.removeItem(DB.CARTBOOK);
    this.router.navigateByUrl('/');
    this.dismiss();
  }

  dismiss() {
    this.modalController.dismiss({result: this.config.RESULT_CANCEL});
  }

  onItemClick(type: number) {
    this.payManaged = false;

    switch (type) {
      case TypePayment.MONEY: this.onCash(); break;
      case TypePayment.PAYPAL: this.onPaypal(); break;
      case TypePayment.CARD: this.onCard(); break;
      case TypePayment.CREDIT: this.onCredit(); break;
      case TypePayment.SATISPAY: this.onSatispay(); break;
    }
  }

  async onCash() {
    let modal = await this.modalController.create({ component: CashPayPage, componentProps: {order: this.cartBookData.order} });
    (await modal).onDidDismiss().then((result) => {
      if (result && result.data && result.data.result == this.config.JPS_OK) {
        this.dataPay.type = TypePayment.MONEY;
        this.finalize();
      }
    });
    return await modal.present();
  }

  async onPaypal() {
    let modal = await this.modalController.create({ component: PaypalPayPage, componentProps: { cart: this.cartBookData } });
    (await modal).onDidDismiss().then((result) => {
      if (result && result.data && result.data.result == this.config.RESULT_OK) {
        this.dataPay.type = TypePayment.PAYPAL;
        this.dataPay.response.response = result.data.response;
        this.finalize();
      } else {
        this.alertService.showAlert("", "Attenzione! Pagamento non riuscito!");
      }

    });
    return await modal.present();
  }

  async onCard() {
    let modal = await this.modalController.create({ component: CardPayPage, componentProps: { id: this.cartBookData.order.id } });
    (await modal).onDidDismiss().then((result) => {
      if (result && result.data && result.data.result == this.config.RESULT_OK) {
        this.cartBookData.order.payment_id = TypePayment.CARD;
        localStorage.setItem(DB.CARTBOOK, JSON.stringify(this.cartBookData));

        var ref = this.iab.create(result.data.href, "_blank", "clearsessioncache=yes,clearcache=yes,hardwareback=yes,footer=no,location=no");

        ref.on("loadstart").subscribe(event => {
          console.log("Loading started: " + event.url);
          if (event.url.match(EitUrl.urlCardOk)) {
            this.payManaged = true;
            console.log("url managed: " + event);
            ref.close();
            this.manageCard(event);
          }
          if (event.url.match(EitUrl.urlCardKo)) {
            console.log("url managed: " + event);
            ref.close();
            this.payManaged = true;
            this.alertService.showAlert("", "Attenzione! Pagamento non autorizzato!");
          }
        });

        ref.on("loadstop").subscribe(event => {
          console.log("Loading finished: " + event.url);
          if (this.payManaged == false) {
            if (event.url.match(EitUrl.urlCardOk)) {
              this.payManaged = true;
              console.log("url managed: " + event);
              ref.close();
              this.manageCard(event);
            }
            if (event.url.match(EitUrl.urlCardKo)) {
              console.log("url managed: " + event);
              ref.close();
              this.payManaged = true;
              this.alertService.showAlert("", "Attenzione! Pagamento non autorizzato!");
            }
          }
        });

        ref.on("loaderror").subscribe(event => {
          console.log("Loading error: " + event.message);
          ref.close();
          if (!this.payManaged) {
            //this.recordPreaccepted();
          }
        });

        ref.on("exit").subscribe(() => {
          console.log("Browser is closed...");
          if (!this.payManaged) {
            //this.recordPreaccepted();
          }
        });
      }
    });
    return await modal.present();
  }

  async onCredit() {
    let modal = await this.modalController.create({ component: CreditPayPage, componentProps: { order: this.cartBookData.order } });
    (await modal).onDidDismiss().then((result) => {
      if (result && result.data && result.data.result == this.config.RESULT_OK) {

        var tot = this.cartBookData.order.euro_total.toFixed(2); 
        localStorage.setItem(DB.CARTBOOK, JSON.stringify(this.cartBookData));
        if (tot == 0) {
          this.dataPay.type = TypePayment.CREDIT;
         // this.finalize()
        }
      }
    });
    return await modal.present();
  }

  onSatispay() {
    this.http.setDataSerializer("json");
    var urlSat = Satispay.urlRel + "/online/v1/checkouts";
    var descr = "CMF_01_Order_" + this.cartBookData.order_id;
    var dataSat = {
      phone_number: this.cartBookData.order.phone,
      redirect_url: Satispay.urlReturn,
      description: descr,
      callback_url: Satispay.urlCallback,
      amount_unit: this.cartBookData.order.euro_total * 100,
      currency: "EUR",
      metadata: {
        orderid: this.cartBookData.order_id
      }
    };
    let headers = {"Content-Type": "application/json", Authorization: "Bearer " + this.cartBookData.SPKR};
    this.http.post(urlSat, dataSat, headers).then(res => {
      if (res && res.status == 200) {
        var resultData = JSON.parse(res.data);
        let target = "_blank";
        var ref = this.iab.create(resultData.checkout_url, target, "clearsessioncache=yes,clearcache=yes,hardwareback=yes,footer=yes,location=no");
        
        ref.on("loadstart").subscribe(event => {
          console.log("Loading started: " + event.url);
          if (event.url.match(Satispay.urlReturn)) {
            console.log("Loading finished: " + event);
            this.payManaged = true;
            ref.close();
            this.dataPay.type = TypePayment.SATISPAY;
            this.dataPay.satispay = event.url;
            this.dataPay.response = event;
           // this.finalize();
          }
        });

        ref.on("loaderror").subscribe(event => {
          console.log("Loading error: " + event.message);
        });

        ref.on("exit").subscribe(() => {
          console.log("Browser is closed...");
          if (this.payManaged == false) {
            //this.recordPreaccepted();
          }
        });
      }
    }, err => {
      console.log(err);
    });
  }

  manageCard(event: any) {
    this.dataPay.type = TypePayment.CARD;
    this.dataPay.gestpay = event.url;
    this.dataPay.response = event;
    this.finalize();
  }

  finalize(){
    this.loadingService.show();
    this.api.postj("booking/finalizeorder", this.dataPay, "").then(res => {
      console.log(res);
      this.loadingService.hide();
      if (res[this.config.JP_STATUS] == this.config.JPS_OK) {
        this.cartBookData.order.payment_id = this.dataPay.type;
        localStorage.setItem(DB.CARTBOOK, JSON.stringify(this.cartBookData));
        this.closeBook();
      }
      else {
        this.alertService.showAlert("", "Attenzione! Pagamento non riuscito!");
      }
    },
      err => {
        console.log(err);
        this.loadingService.hide();
      });
  }
  
  closeBook() {
    //localStorage.removeItem("cartbook");
    this.cartBookData.order.status = 1; // this.orderStatus.PAID;
    localStorage.setItem(DB.CARTBOOK, JSON.stringify(this.cartBookData));
    this.modalController.dismiss({result: this.config.RESULT_OK});
  }

}
