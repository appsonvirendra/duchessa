import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BookcheckpayPage } from './bookcheckpay.page';

describe('BookcheckpayPage', () => {
  let component: BookcheckpayPage;
  let fixture: ComponentFixture<BookcheckpayPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookcheckpayPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BookcheckpayPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
