import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BookcheckpayPageRoutingModule } from './bookcheckpay-routing.module';

import { BookcheckpayPage } from './bookcheckpay.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BookcheckpayPageRoutingModule
  ],
  declarations: [BookcheckpayPage]
})
export class BookcheckpayPageModule {}
