import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BookcheckpayPage } from './bookcheckpay.page';

const routes: Routes = [
  {
    path: '',
    component: BookcheckpayPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BookcheckpayPageRoutingModule {}
