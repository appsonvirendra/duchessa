import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LocationselectPage } from './locationselect.page';

const routes: Routes = [
  {
    path: '',
    component: LocationselectPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LocationselectPageRoutingModule {}
