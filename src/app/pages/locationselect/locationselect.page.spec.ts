import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LocationselectPage } from './locationselect.page';

describe('LocationselectPage', () => {
  let component: LocationselectPage;
  let fixture: ComponentFixture<LocationselectPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocationselectPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LocationselectPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
