import { Component, OnInit, NgZone, ViewChild } from '@angular/core';
import { MapsAPILoader, AgmMap } from '@agm/core';
import { ConfigApp } from '../../shared/services/config-app.service';
import { EitPlace } from 'src/app/shared/classes/eitplace';
import { ModalController } from '@ionic/angular';
import { AppInfo } from '../../shared/services/env.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
//import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { NativeGeocoder, NativeGeocoderResult } from '@ionic-native/native-geocoder/ngx';

declare var google;

@Component({
  selector: 'app-locationselect',
  templateUrl: './locationselect.page.html',
  styleUrls: ['./locationselect.page.scss'],
})
export class LocationselectPage implements OnInit {

  @ViewChild(AgmMap) agmMap: AgmMap;
  autocompleteService: any;
  query: string = '';
  places: any = [];
  evPlace: any;
  geocoder: any;
  location: any;
  latitude: number = 51.673858;
  longitude: number = 7.815982;
  zoom: number = 15
  place: EitPlace;
  saveDisabled: boolean = true;
  locationData: any;
  placeSelected: any;
  onSelected: boolean = false;
  loading: boolean = false;
  mapInfo: any = {lat: AppInfo.LATITUDE, lng: AppInfo.LONGITUDE, icon: {url: "assets/imgs/pin-mappa-150.png", scaledSize: new google.maps.Size(51, 60)}}

  constructor(private mapsAPILoader: MapsAPILoader, public zone: NgZone, public config: ConfigApp, private geolocation: Geolocation, private modalCtrl: ModalController,
              private nativeGeo: NativeGeocoder/*, private diagnostic: Diagnostic*/) { }

  ngOnInit() {
    this.loading = true;
    console.log("location select")

    this.places = [];
    this.query = "";
    this.place = new EitPlace();
    this.mapsAPILoader.load().then(() => {
      this.geocoder = new google.maps.Geocoder;
      this.autocompleteService = new google.maps.places.AutocompleteService();
    });
    this.initData();    
  }

  selectPlace(place: any) {
    this.places = [];
    this.query = place.description;
    this.placeSelected = place;   
    this.onSelected = true;
    console.log(place)
    this.geocoder.geocode({ 'placeId': place.place_id }, (results, status) => {
      if (status === 'OK' && results[0] && results[0].geometry && results[0].geometry.location) {
        this.mapInfo.lat = results[0].geometry.location.lat();
        this.mapInfo.lng = results[0].geometry.location.lng();
      }
    }) 
   // this.cd.detectChanges()
    this.saveDisabled = false;
    /*this.config.selectPlace(this.geocoder, this.locationData.place, place).then(res => {
      this.dismiss({'result':this.config.RESULT_OK, 'location':this.locationData.place});
    });*/
  }

  save(){
    this.config.selectPlace(this.geocoder, this.locationData.place, this.placeSelected).then(res => {
      this.dismiss({'result':this.config.RESULT_OK, 'location':this.locationData.place});
    });
  }

  searchPlace() {
    if(!this.onSelected){
      if (this.query.length > 0) {
        let config = {
          types: ['geocode'],
          input: this.query
        }

        this.autocompleteService.getPlacePredictions(config, (predictions, status) => {
          
          if (status == google.maps.places.PlacesServiceStatus.OK && predictions) {
            this.places = [];
            this.zone.run(() => {
              predictions.forEach((prediction) => {
                this.places.push(prediction);
              });
            });
          }

        });

      } else {
        this.places = [];
      }
    }
    else{
      this.onSelected = false;
    }
  }

  getPosition() {
   /* this.diagnostic.getLocationMode().then(state =>{
      if(state === this.diagnostic.locationMode.LOCATION_OFF) {
        this.diagnostic.switchToLocationSettings();
      }
      else{ */
        this.geolocation.getCurrentPosition().then(position => {            
          console.log(JSON.stringify(position));  
          let latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
          let request = {
            latLng: latlng
          }
          this.geocoder.geocode(request, (results, status) => {
            if (status === 'OK' && results[0] && results[0].geometry && results[0].geometry.location) {
              this.config.setLocationInfoFromNative(this.locationData.place, results[0], position.coords.latitude, position.coords.longitude);
              this.placeSelected = results[0];   
              this.onSelected = true;
              this.saveDisabled = false;
              this.query = this.locationData.place.description;
              this.mapInfo.lat = position.coords.latitude;
              this.mapInfo.lng = position.coords.longitude;
            }
          }) 
         /* this.nativeGeo.reverseGeocode( position.coords.latitude, position.coords.longitude).then((result: NativeGeocoderResult[]) =>{
            console.log(JSON.stringify(result[0]));  
            if(result!=null && result.length) {
              this.config.setLocationInfoFromNative(this.locationData.place, result[0], position.coords.latitude, position.coords.longitude);
              this.onSelected = true;
              this.saveDisabled = false;
              this.query = this.locationData.place.description;
              this.mapInfo.lat = position.coords.latitude;
              this.mapInfo.lng = position.coords.longitude;
            }
          }).catch((error: any) => console.log(error));*/
        }, err => {
          console.log(err);
        })
        .catch((error: any) => {
          console.log(error)
        });
     /* }
    });   */
  }

  initData() {
    this.locationData = {};
    this.locationData.place = new EitPlace();
    this.locationData.place.zipcode = "";
    this.locationData.place.num = "";
    this.locationData.place.nomelocale = "";
    this.locationData.place.latitude = AppInfo.LATITUDE;
    this.locationData.place.longitude = AppInfo.LONGITUDE;
    this.locationData.place.street_address = "";
    this.locationData.place.locality = "";
    this.locationData.place.sublocality = "";
    this.locationData.place.regione = "";
    this.locationData.place.nazione = "";
    this.locationData.place.admin_area_3 = "";

    if (localStorage.getItem("location")) {
       this.locationData.place = JSON.parse(localStorage.getItem("location"));
       this.query = this.locationData.place.description;
       this.mapInfo.lat = this.locationData.place.latitude
       this.mapInfo.lng = this.locationData.place.longitude
    }
    setTimeout(()=> {
      this.loading = false;
      this.agmMap.triggerResize(true);
    }, 100)
  }   


  dismiss(data: any) {
    this.modalCtrl.dismiss(data);
  }
  markerDragEnd($event: any) {
    console.log($event);
    /*this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    this.getAddress(this.latitude, this.longitude);*/
  }
  /*
  ngOnInit() {
    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation();
      this.geoCoder = new google.maps.Geocoder;

      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"]
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;
        });
      });
    });
  }

  // Get Current Location Coordinates
  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 8;
        this.getAddress(this.latitude, this.longitude);
      });
    }
  }


  markerDragEnd($event: any) {
    console.log($event);
    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    this.getAddress(this.latitude, this.longitude);
  }

  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      console.log(results);
      console.log(status);
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 12;
          this.address = results[0].formatted_address;
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }

    });
  }
  */
}
