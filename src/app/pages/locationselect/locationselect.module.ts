import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LocationselectPageRoutingModule } from './locationselect-routing.module';

import { LocationselectPage } from './locationselect.page';
import { AgmCoreModule } from '@agm/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LocationselectPageRoutingModule,
    AgmCoreModule,
    //Geolocation
  ],
  declarations: [LocationselectPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LocationselectPageModule {}
