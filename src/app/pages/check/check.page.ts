import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ConfigApp, StatusOrder, TypeOrder, TypePayment } from '../../shared/services/config-app.service';
import { ApiService } from '../../shared/services/api.service';
import { Router } from '@angular/router';
import { AppInfo } from '../../shared/services/env.service';
import { CartService } from '../../shared/services/cart.service';

@Component({
  selector: 'app-check',
  templateUrl: './check.page.html',
  styleUrls: ['./check.page.scss'],
})
export class CheckPage implements OnInit {

  cartData: any = "";
  orderStatus: any = StatusOrder;
  appInfo: any = AppInfo;
  payType: any = TypePayment;
  interval: any;
  numCheck: number = 0;
  checkClosed: boolean = false;
  time_order: any = "12:00";
  isDomicilio: boolean = true;
  isAsporto: boolean = true;
  items: Array<any> = []

  constructor(private cartService: CartService, private api: ApiService, public config: ConfigApp, private cdRef: ChangeDetectorRef, private router: Router) { 
    var cart = this.cartService.cart;
    if (cart) {
      this.cartData = cart;
      this.isDomicilio = this.cartData.order.type_order == TypeOrder.EIT_DOMICILIO || this.cartData.order.type_order == TypeOrder.REST_DOMICILIO
      this.isAsporto = this.cartData.order.type_order == TypeOrder.EIT_ASPORTO || this.cartData.order.type_order == TypeOrder.REST_ASPORTO
      this.toArray();
      this.closeOrder();
    }
  }

  ngOnInit() {
    console.log('check')
  }

  openInfo() { }

  onGotoHome() {
    console.log("gohome");
    this.router.navigateByUrl('/');
    //this.navCtrl.popToRoot();
  }

  initCheck() {
    this.cartData.order.status = StatusOrder.PAID;
    this.interval = setInterval(() => {
      this.checkOrder();
    }, 3000);
  }

  closeOrder() {    
    this.cartService.closeOrder();
    this.initCheck();
  }

  checkOrder() {
    this.numCheck++;
    var data = { orderid: this.cartData.order_id };
    this.api.postj("order/recordpreaccepted", data, "").then(res => {
      if (res[this.config.JP_STATUS] == this.config.JPS_OK) {
          this.cartData.order.delivery_time = res[this.config.JP_RESULT]["delivery_time"];
          this.cartData.order.real_delivery_time = res[this.config.JP_RESULT]["real_delivery_time"];
          this.cartData.order.data_ordine_dmy = res[this.config.JP_RESULT]["data_ordine_dmy"];
          this.cartData.order.status = res[this.config.JP_RESULT]["status"];
          this.cartData.order.tipooraconsegna = res[this.config.JP_RESULT]["tipooraconsegna"];
          this.cartData.order.fasciaoraria = res[this.config.JP_RESULT]["fasciaoraria"];
          this.cartData.order.bookingdate = res[this.config.JP_RESULT]["bookingdate"];
          this.cartData.order.tipomodoconsegna = res[this.config.JP_RESULT]["tipomodoconsegna"];
          this.cartData.order.order_date = res[this.config.JP_RESULT]["order_date"];
          this.cartData.order.consegna_eit = res[this.config.JP_RESULT]["consegna_eit"];
          this.cdRef.detectChanges();
          this.validateOrder();
        } else {
          console.log(this.numCheck);
        }
      },
      err => {
        console.log(err);
      }
    );
  }

  validateOrder() {
    if (this.cartData.order.status != StatusOrder.PAID) {
      clearInterval(this.interval);
      this.checkClosed = true;
      if (this.cartData.order.status == StatusOrder.ACCEPTED) {
        this.cartData.order.status = StatusOrder.ACCEPTED_NOTIFIED;
      } 
      else if (this.cartData.order.status == StatusOrder.REJECTED) {
        this.cartData.order.status = StatusOrder.REJECTED_NOTIFIED;
      } 
      else if (this.cartData.order.status == StatusOrder.NO_RESPONSE) {
        this.cartData.order.status = StatusOrder.NO_RESPONSE_NOTIFIED;
      }
      var data = {orderid: this.cartData.order_id, status: this.cartData.order.status, delivery_time: this.cartData.order.delivery_time};
      this.api.postj("order/status", data, "").then(res => {
        if (res[this.config.JP_STATUS] == this.config.JPS_OK) {
          console.log("Validation order OK");
          this.time_order = res[this.config.JP_RESULT]["order_time"];            
          this.cdRef.detectChanges();
        } 
        else {
          console.log(this.numCheck);
        }
      }, err => {
        console.log(err);
      });
    }
    if (this.numCheck >= 45 && !this.checkClosed) {
      clearInterval(this.interval);
      this.cartData.order.status = StatusOrder.NO_RESPONSE_NOTIFIED;
      this.cartData.order.delivery_time = -1;
    }
  }

  reactivateorder() {
    var data = { pid: this.cartData.order_id };
    this.api.postj("order/rreactivateorder", data, "").then(res => {
      if (res[this.config.JP_STATUS] == this.config.JPS_OK) {
        
      } 
      else {
        console.log(this.numCheck);
      }
    },err => {
      console.log(err);
    });
  }

  toArray(){
    if(this.cartData.order && this.cartData.order.items){
      this.items = Object.keys(this.cartData.order.items).map(key => ({type: key, value: this.cartData.order.items[key]}));
    }
  }
}
