import { Component, OnInit, Input } from '@angular/core';
import { ConfigApp, TypePayment } from '../../shared/services/config-app.service';
import { ApiService } from '../../shared/services/api.service';
import { ModalController } from '@ionic/angular';
import { EitUrl } from '../../shared/services/env.service';
import { AlertService } from '../../shared/services/alert.service';

@Component({
  selector: 'app-card-pay',
  templateUrl: './card-pay.page.html',
  styleUrls: ['./card-pay.page.scss'],
})
export class CardPayPage implements OnInit {

  @Input() id: number = 0;
  years: Array<any> = [];
  months: Array<string> = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"]
  valid: any = {buyername: true, number: true, expMonth: true, expYear: true, CVV: true}
  item: any = {}

  constructor(private api: ApiService, private modalCtrl: ModalController, public config: ConfigApp, private alertService: AlertService) { 
    let year = new Date().getFullYear();
    for(let i = 0; i < 10; i++){
      let str = ""+ (year + i)
      this.years.push({label: str, value: str.substr(2, 2)})
    }
   
  }

  ngOnInit() {
    this.item.urlOK = EitUrl.urlCardOk + "?" + this.id;
    this.item.urlKO = EitUrl.urlCardKo + "?" + this.id;
    this.item.urlServer = EitUrl.urlCardSync + "?shopid=" + this.id;
    this.item.ID = this.id;
    this.create();
  }

  create() {
    let params = {ID: this.id, type: TypePayment.CARD};
    this.api.postj("order/payment", params).then(res => {
      if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {
        this.item.paymentToken = res[this.config.JP_RESULT];
      }
    }, (err) => {
      console.log(err);
    });
  }

  onComplete() {
    if (this.validate()) {      
      this.api.postj("order/paysubmit", this.item).then(res => {
        if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {
          this.dismiss({'result':this.config.RESULT_OK, "href":res[this.config.JP_RESULT]['href']});
        }
        else {
          this.alertService.showAlert("", "Attenzione! Transazione NON eseguita, controllare i dati della carta!");
        }
      }, (err) => {
        console.log(err);
        this.alertService.showAlert("", "Attenzione! Transazione NON eseguita, controllare i dati della carta!");
      });
    }
    else{
      this.alertService.showAlert("", "Attenzione! Transazione NON eseguita, controllare i dati della carta!");
    }
  }

  validate(): boolean {
    this.valid.buyername = this.item.buyername && this.item.buyername.length > 0;
    this.valid.number = this.item.number && this.item.number.length > 0;
    this.valid.expMonth = this.item.expMonth && this.item.expMonth.length > 0;
    this.valid.expYear = this.item.expYear && this.item.expYear.length > 0;
    this.valid.CVV = this.item.CVV && this.item.CVV.length > 0;

    return this.valid.buyername && this.valid.number && this.valid.expMonth && this.valid.expYear && this.valid.CVV;
  }

  dismiss(data: any) {
    this.modalCtrl.dismiss(data);
  }
}
