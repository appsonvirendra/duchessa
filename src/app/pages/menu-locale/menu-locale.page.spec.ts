import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MenuLocalePage } from './menu-locale.page';

describe('MenuLocalePage', () => {
  let component: MenuLocalePage;
  let fixture: ComponentFixture<MenuLocalePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuLocalePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MenuLocalePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
