import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MenuLocalePageRoutingModule } from './menu-locale-routing.module';

import { MenuLocalePage } from './menu-locale.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MenuLocalePageRoutingModule
  ],
  declarations: [MenuLocalePage]
})
export class MenuLocalePageModule {}
