import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuLocalePage } from './menu-locale.page';

const routes: Routes = [
  {
    path: '',
    component: MenuLocalePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuLocalePageRoutingModule {}
