import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { ConfigApp } from '../../shared/services/config-app.service';
import { AppInfo } from '../../shared/services/env.service';
import { LoadingService } from '../../shared/services/loading.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-menu-locale',
  templateUrl: './menu-locale.page.html',
  styleUrls: ['./menu-locale.page.scss'],
})
export class MenuLocalePage implements OnInit {

  rId: number = 0;
  mId: number = 0;
  coords: any = "";
  restaurantFull: any = null;
  menu: any = {categories: []};
  categories: any = [];
  selectedCat : any;
  isActive : boolean;
  appInfo: any = AppInfo;

  constructor(private api: ApiService, public config: ConfigApp, private loadingService: LoadingService, private cd: ChangeDetectorRef, private router: Router, 
              private route: ActivatedRoute){
    console.log("menu")
    //devo prendere la posizione dell'utente e trovare il ristorante più vicino
    this.route.queryParams.subscribe(params => {
      this.coords = JSON.parse(params["coords"]);
      this.fill();
    });
  }

  ngOnInit() {
    console.log("menu")
  }

  ionViewWillEnter(){
    console.log("menu enter")
  }

  fill() {
    this.api.postj("restaurant/getnearestrestaurant", this.coords, "").then(res => {
      console.log(res);
      if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {
        this.rId = res[this.config.JP_RESULT]['rId'];
        this.mId = res[this.config.JP_RESULT]['mId'];
        this.fillMenu();
      } 
    }, err => {
      console.log(err);
    });
  }

  fillMenu() {
    this.loadingService.show();
    this.api.postj("restaurant/menu", {id: this.mId}, "").then(res => {
      console.log(res);
      if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {
        this.menu = res[this.config.JP_RESULT];
        this.categories = Array.of(this.menu.categories);
        this.cd.detectChanges()
        this.selectedCat = 0
      }
      else {
        console.log(res);
      }
      this.loadingService.hide();
    },err => {
      console.log(err);
      this.loadingService.hide();
    });
  }

  shortDescr(item: any) {
    return item.descr.length > 0 && item.descr.length < 15 ? " (" + item.descr + ")" : "";
  }

  longDescr(item: any) {
    return item.descr.length > 15 ? item.descr + " " : "";
  }

  ingredients = function(item: any) {
    var result = "";
    for (var iG = 0; iG < item.ingredients.length; iG++) {
      result += item.ingredients[iG].name;
      if (iG < item.ingredients.length - 1) {
        result += ", ";
      }
    }
    return result;
  }

  goToHome(){
    this.router.navigateByUrl('/');
  }
}
