import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ToastService } from '../../shared/services/toast.service';
import { ConfigApp } from '../../shared/services/config-app.service';
import { AlertService } from '../../shared/services/alert.service';
import { ApiService } from '../../shared/services/api.service'
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { AppInfo } from '../../shared/services/env.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  item: any = {};
  valid: any = {email: true, password: true, cpassword: true, name: true, lastname: true, phone: true}
  private regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  constructor(private modalCtrl: ModalController, private api: ApiService, private toastService: ToastService, public config: ConfigApp, 
              private alertService: AlertService, public iab: InAppBrowser) { }

  ngOnInit() {
  }

  register() {
    if(this.validate()){
      this.api.register(this.item).then(res => {
        if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {
          let btns = [{text: 'OK', role: 'cancel', handler: () => {console.log('OK clicked'); this.dismiss({'result': this.config.RESULT_OK})}}]
          this.alertService.showAlert("Benvenuto!", "Grazie per esserti registrato! Torna al login!", btns)
        }
        else{
          let err = res[this.config.JP_MSG]
          let errStr: string = "<ul>";
          errStr += (err.email != null ? "<li>" + err.email[0] + "</li>" : "") +
                    (err.name != null ? "<li>" + err.name[0] + "</li>" : "") + 
                    (err.lastname != null ? "<li>" + err.lastname[0] + "</li>" : "") +
                    (err.phone != null ? "<li>" + err.phone[0] + "</li>" : "") +
                    (err.password != null ? "<li>" + err.password[0] + "</li>" : "") +
                    (err.cpassword != null ? "<li>" + err.cpassword[0] + "</li>" : "") + "</ul>";
                       
          this.alertService.showAlert("Attenzione! Controlla i dati inseriti!", errStr)
          
        }
      }, err =>{
        let errStr: string = "<ul>";
        errStr += (err.email != null ? "<li>" + err.email[0] + "</li>" : "") +
                  (err.name != null ? "<li>" + err.name[0] + "</li>" : "") + 
                  (err.lastname != null ? "<li>" + err.lastname[0] + "</li>" : "") +
                  (err.phone != null ? "<li>" + err.phone[0] + "</li>" : "") +
                  (err.password != null ? "<li>" + err.password[0] + "</li>" : "") +
                  (err.cpassword != null ? "<li>" + err.cpassword[0] + "</li>" : "") + "</ul>";
                     
        this.alertService.showAlert("Attenzione! Controlla i dati inseriti!", errStr)
      })
    }
    else{
      this.toastService.presentToast("Controlla i dati inseriti!")
    }
  }

  validate(): boolean {
    this.item.cpassword = this.item.password;
    this.valid.name = this.item.name && this.item.name.length > 0;
    this.valid.lastname = this.item.lastname && this.item.lastname.length > 0;
    this.valid.phone = this.item.phone && this.item.phone.length > 0;
    this.valid.password = this.item.password && this.item.password.length >= 6;
    this.valid.cpassword = this.item.cpassword && this.item.password === this.item.cpassword;
    this.valid.email = this.regex.test(this.item.email);

    return this.valid.email && this.valid.password && this.valid.cpassword && this.valid.name /*&& this.valid.lastname*/ && this.valid.phone;
  }

  dismiss(data: any) {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss(data);
  }

    
  terms(){
    this.iab.create(AppInfo.LINK_TERMS, "_system");
  }

  privacy(){
    this.iab.create(AppInfo.LINK_PRIVACY, "_system");
  }
}
