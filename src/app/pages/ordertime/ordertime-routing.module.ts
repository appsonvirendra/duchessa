import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrdertimePage } from './ordertime.page';

const routes: Routes = [
  {
    path: '',
    component: OrdertimePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrdertimePageRoutingModule {}
