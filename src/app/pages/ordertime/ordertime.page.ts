import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ApiService } from '../../shared/services/api.service';
import { ConfigApp, TypeOrder, ET_TipoOraConsegna } from '../../shared/services/config-app.service';
import { DatePipe } from '@angular/common';
import { AppInfo } from '../../shared/services/env.service';

@Component({
  selector: 'app-ordertime',
  templateUrl: './ordertime.page.html',
  styleUrls: ['./ordertime.page.scss'],
})
export class OrdertimePage implements OnInit {

  @Input() cartData: any;
  @Input() restData: any;
  sceltatipoorario: any;
  elencoorari: any = [];
  orarioscelto: any;
  isPreorder : boolean;
  giornoconsegna: string;
  typeOrder: any = TypeOrder;
  isDomicilio: boolean = true;

  constructor(private api: ApiService, public config: ConfigApp, private modalCtrl: ModalController, private datePipe: DatePipe) { 

    let adesso = new Date();  
    this.giornoconsegna = this.datePipe.transform(adesso, 'yyyy-MM-dd');

    this.isPreorder = false;

    var rest = localStorage.getItem("restaurant");
    if (rest) {
      this.restData = JSON.parse(rest);
      this.isPreorder = (!this.restData.message_event && this.restData.nowclosed && !this.restData.day_closed && this.restData.preorder);  
    }

    var cart = localStorage.getItem("cart");
    if (cart != undefined && cart != null) {
      this.cartData = JSON.parse(cart);      
      this.sceltatipoorario = "ordertime";
      this.isDomicilio = this.cartData.order.type_order == TypeOrder.EIT_DOMICILIO || this.cartData.order.type_order == TypeOrder.REST_DOMICILIO
      this.orarioscelto = this.cartData.order.tipooraconsegna == ET_TipoOraConsegna.FASCIAORARIA ? this.cartData.order.fasciaoraria :  this.cartData.order.order_date;
    }

    console.log("Is preorder: " + this.isPreorder);
    this.fillTimes();
  }

  ngOnInit() {
  }

  dismiss(data: any) {
    this.modalCtrl.dismiss(data);
  }

  fillTimes() {
    var data: any = {restid: this.cartData.rest_id, isfortakeaway: this.cartData.order.type_order == TypeOrder.REST_ASPORTO, dayorder: this.giornoconsegna};
    this.api.postj("restaurant/elencoorari", data, "").then(res => {
      console.log(res);
      if (res[this.config.JP_STATUS] == this.config.JPS_OK) {
        this.elencoorari = res[this.config.JP_RESULT];

        //Forzo la scelta dell'orario
        this.sceltatipoorario = "ordertime";
        console.log(this.cartData.order.order_date);
        console.log(this.elencoorari[0]);

        if (this.cartData.order.order_date == "FAST" || this.cartData.order.order_date == null){
          this.orarioscelto = this.elencoorari[0].value;
        }
        else{
          this.orarioscelto = this.cartData.order.tipooraconsegna == ET_TipoOraConsegna.FASCIAORARIA ? this.cartData.order.fasciaoraria :  this.cartData.order.order_date;          
        }
      }
    },
    err => {
      console.log(err);
    });
  }

  onComplete() {
    console.log("oncomplete");
    if (this.sceltatipoorario == "fast"){
      this.orarioscelto = this.elencoorari[0].value;
    }
    this.dismiss({'result':this.config.JPS_OK, 'orarioscelto':this.orarioscelto, 'giornoconsegna':this.giornoconsegna});
  }

  updateMyDate($event) {
    console.log($event); // --> will contains $event.day, $event.month and $event.year 
    console.log(this.giornoconsegna);
    this.fillTimes();
  }

}
