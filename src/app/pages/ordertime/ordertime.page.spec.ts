import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OrdertimePage } from './ordertime.page';

describe('OrdertimePage', () => {
  let component: OrdertimePage;
  let fixture: ComponentFixture<OrdertimePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdertimePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OrdertimePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
