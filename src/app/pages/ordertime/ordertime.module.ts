import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrdertimePageRoutingModule } from './ordertime-routing.module';

import { OrdertimePage } from './ordertime.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrdertimePageRoutingModule
  ],
  declarations: [OrdertimePage]
})
export class OrdertimePageModule {}
