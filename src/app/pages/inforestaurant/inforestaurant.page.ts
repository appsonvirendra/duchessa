import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
//import { GoogleMap, Environment, GoogleMapOptions, GoogleMaps } from '@ionic-native/google-maps';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
//import { EmailComposer } from '@ionic-native/email-composer/ngx';
//import { CallNumber } from '@ionic-native/call-number/ngx';
//import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';
import { AppInfo } from '../../shared/services/env.service';
import { AgmMap } from '@agm/core';
import { ApiService } from '../../shared/services/api.service';
import { ConfigApp } from '../../shared/services/config-app.service';
import { Location } from '@angular/common';

//var google: any;

@Component({
  selector: 'app-inforestaurant',
  templateUrl: './inforestaurant.page.html',
  styleUrls: ['./inforestaurant.page.scss'],
})
export class InforestaurantPage implements OnInit {

  restaurant: any = {}
  appInfo: any = AppInfo;
  title: Array<any> = []
  days: any = ['Lun', 'Mar', 'Mer', 'Gio', 'Ven', 'Sab', 'Dom'];
  @ViewChild(AgmMap) agmMap: any;
  myMap: any;
  mapInfo: any = {lat: 0, lng: 0, icon: {url: "assets/imgs/pin-mappa-150.png", scaledSize: new google.maps.Size(51, 60)}}
//map: GoogleMap;
  isHeaderVisible = false;  
  //carousel: NgxCarousel;
  slideOpts: any = {slidesPerView: 1, initialSlide: 0, speed: 400, autoplay: true}

  constructor(private route: ActivatedRoute, public iab: InAppBrowser, private api: ApiService, public config: ConfigApp, private location: Location, 
          /*, private callNumber: CallNumber, private emailComposer: EmailComposer, public iab: InAppBrowser, private launchNavigator: LaunchNavigator*/) {
    this.route.queryParams.subscribe(params => {
      if(params && params.length) this.restaurant = JSON.parse(params["restaurant"]);
      if(this.restaurant.id){
        console.log(this.restaurant)
        //this.title = this.restaurant.name.split("Cammafà")
        if(this.restaurant.location){
          this.mapInfo.lat = this.restaurant.location.latitudine
          this.mapInfo.lng = this.restaurant.location.longitudine
        }
      }
      else{
        this.getInfoRest()
      }
    });
    //this.restaurant = JSON.parse(this.route.snapshot.params.queryParams);
   // this.loadMap();

    //this.carousel = {grid: {xs: 1, sm: 1, md: 1, lg: 1, all: 0}, slide: 1, speed: 400, interval: 4000, /*point: true,*/ load: 2, touch: true, loop: true, custom: 'banner'}
  }

  ngOnInit() {
  }

  loadMap() {
    // This code is necessary for browser
   /* Environment.setEnv({ API_KEY_FOR_BROWSER_RELEASE: 'AIzaSyBDt1H8nXcz7l6DH2qDFfZZj1rMbNVsZNo', API_KEY_FOR_BROWSER_DEBUG: 'AIzaSyBDt1H8nXcz7l6DH2qDFfZZj1rMbNVsZNo' });

    let mapOptions: GoogleMapOptions = {
      camera: { target: { lat: this.restaurant.location.latitudine, lng: this.restaurant.location.longitudine }, zoom: 18, tilt: 30 },
      gestures: { zoom: false, tilt: false, scroll: false, rotate: false }
    };

    this.map = GoogleMaps.create('mapContainer', mapOptions);
    this.map.addMarkerSync({
      title: this.restaurant.name,
      icon: { url: '/assets/imgs/locator.png', scaledSize: new google.maps.Size(33.19, 50) },
      animation: 'DROP',
      position: { lat: this.restaurant.location.latitudine, lng: this.restaurant.location.longitudine }
    });*/
  }

  onMapReady(map: any){
    console.log("On map ready");
    this.myMap = map;
    this.agmMap.triggerResize(true);
    map.setOptions({zoomControl: false, mapTypeControl: false, streetViewControl: false, fullscreenControl: false});
  }

  getInfoRest(){
    this.api.postj("restaurant/getinforest", "", "").then(res => {
      console.log(res);
      if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {
        this.restaurant = res[this.config.JP_RESULT][0];
        if(this.restaurant.location){
          this.mapInfo.lat = this.restaurant.location.latitudine
          this.mapInfo.lng = this.restaurant.location.longitudine
        }
        this.restaurant.mediagallery = [{media: {filename: '7-scheda-ristorante-1-md.jpg'}}, {media: {filename: '8-IMG-20190204-WA0083-md.jpg'}}, {media: {filename: '9-IMG-20190204-WA0023-md.jpg'}}]
      }
    })
  }

  goFacebook() {
    this.iab.create(this.restaurant.appsettings.link_facebook, '_system');
  }

  goInstagram() {
    this.iab.create(this.restaurant.appsettings.link_instagram, "_system");
  }

  goTwitter() {
    this.iab.create(this.restaurant.appsettings.link_twitter, "_system");
  }

  goGoogleplus() {
    this.iab.create(this.restaurant.appsettings.link_googleplus, "_system");
  }

  goTripadvisor() {
    this.iab.create(this.restaurant.appsettings.link_tripadvisor, "_system");
  }

  goSite() {
    this.iab.create(this.restaurant.appsettings.link_website, "_system");
  }

  email() {
    /*let email = {
      to: this.restaurant.appsettings.email_info,
      subject: 'Richiesta informazioni',
      body: '',
      isHtml: true
    };
    this.emailComposer.open(email);*/
  }

  phone() {
    /*this.callNumber.callNumber(this.restaurant.appsettings.phone_info, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));*/
  }

  goTracking(){
     let address = this.restaurant.location.address + " " + this.restaurant.location.num + ", " + 
                  (this.restaurant.location.zipstr ? this.restaurant.location.zipstr : this.restaurant.location.zip) + " " +
                   this.restaurant.location.townst;
     //this.launchNavigator.navigate(address);
  }

  showFollowus(){
    console.log("show")
    return this.restaurant && this.restaurant.appsettings && (this.restaurant.appsettings.activate_facebook || this.restaurant.appsettings.activate_twitter || 
           this.restaurant.appsettings.activate_instagram || this.restaurant.appsettings.activate_googleplus || this.restaurant.appsettings.activate_tripadvisor)
  }

  scrolling(event: any) {
    setTimeout(()=> {
      this.isHeaderVisible = event.detail.currentY > 210
    })
  }

  goBack() {
    this.location.back();
  }   

}
