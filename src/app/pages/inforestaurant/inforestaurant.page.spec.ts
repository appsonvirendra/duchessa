import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InforestaurantPage } from './inforestaurant.page';

describe('InforestaurantPage', () => {
  let component: InforestaurantPage;
  let fixture: ComponentFixture<InforestaurantPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InforestaurantPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InforestaurantPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
