import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InforestaurantPage } from './inforestaurant.page';

const routes: Routes = [
  {
    path: '',
    component: InforestaurantPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InforestaurantPageRoutingModule {}
