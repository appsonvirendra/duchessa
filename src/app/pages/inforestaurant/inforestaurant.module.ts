import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InforestaurantPageRoutingModule } from './inforestaurant-routing.module';

import { InforestaurantPage } from './inforestaurant.page';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InforestaurantPageRoutingModule,
    AgmCoreModule
  ],
  declarations: [InforestaurantPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InforestaurantPageModule {}
