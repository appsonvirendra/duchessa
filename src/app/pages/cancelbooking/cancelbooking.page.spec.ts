import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CancelbookingPage } from './cancelbooking.page';

describe('CancelbookingPage', () => {
  let component: CancelbookingPage;
  let fixture: ComponentFixture<CancelbookingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CancelbookingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CancelbookingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
