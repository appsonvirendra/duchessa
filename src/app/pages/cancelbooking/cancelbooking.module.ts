import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CancelbookingPageRoutingModule } from './cancelbooking-routing.module';

import { CancelbookingPage } from './cancelbooking.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CancelbookingPageRoutingModule
  ],
  declarations: [CancelbookingPage]
})
export class CancelbookingPageModule {}
