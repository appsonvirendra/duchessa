import { Component, Input, OnInit } from '@angular/core';
import { ConfigApp, StatusBook } from '../../shared/services/config-app.service';
import { ModalController } from '@ionic/angular';
import { ApiService } from '../../shared/services/api.service';
import { ToastService } from '../../shared/services/toast.service';

@Component({
  selector: 'app-cancelbooking',
  templateUrl: './cancelbooking.page.html',
  styleUrls: ['./cancelbooking.page.scss'],
})
export class CancelbookingPage implements OnInit {

  BookStatus : any = StatusBook;
  @Input() booking: any;

  constructor(private api: ApiService, public config: ConfigApp, private toastService: ToastService, private modalController: ModalController) { }

  ngOnInit() {
  }

  dismiss() {
    this.modalController.dismiss();
  }

  prenotazionenoneliminabile(): boolean{
    if (this.booking){
      let arr = this.booking.oraprenotazione.split(" ");
      let arrDate = arr[0].split("-");
      let arrHour = arr[1].split(":");
      let today = new Date();
      let maxDelTime = new Date(arrDate[0], parseInt(arrDate[1]) - 1, arrDate[2], parseInt(arrHour[0]) - 2, arrHour[1], arrHour[2]);
      return maxDelTime < today;
    }
    return false;
  }

  bookingcancellation(){
    let params = {ID: this.booking.id, AUX_FIELD_1: this.booking.reasoncancellation, STATUS: StatusBook.CANCELLED_BY_USER};
    this.api.postj("user/settings/cancelbooking", params, "").then(res => {
      if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) { 
        this.toastService.presentToast('Prenotazione cancellata!');  
        this.booking.status = StatusBook.CANCELLED_BY_USER;
        this.booking.datamodifica = res[this.config.JP_RESULT]['datamodifica'];
        this.booking.oramodifica = res[this.config.JP_RESULT]['oramodifica'];
        this.dismiss();
      }
      else if (res[this.config.JP_STATUS] == this.config.JPS_ERROR && res[this.config.JP_MSG]) {
        res[this.config.JP_MSG].forEach((msg: any) => {
          this.toastService.presentToast(msg); 
        });
      }
      else {
        console.log(res);
      }
    }, (err) => {
      console.log(err);
    })
  }
}
