import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CancelbookingPage } from './cancelbooking.page';

const routes: Routes = [
  {
    path: '',
    component: CancelbookingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CancelbookingPageRoutingModule {}
