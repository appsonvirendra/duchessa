//import { AppSettings } from '../../shared/services/app-settings';
import { Component } from '@angular/core';
//import { HomeService } from './../../services/home-service';
import { ModalController } from '@ionic/angular';
//import { IntroPage } from '../intro-page/intro-page.page';
import { LocationselectPage } from '../locationselect/locationselect.page';
import { ConfigApp, UtilityType, TypeService } from '../../shared/services/config-app.service';
import { RestaurantService } from '../../shared/services/restaurant.service';
import { Router } from '@angular/router';
import { NcivicoPage } from '../ncivico/ncivico.page';
import { UtilityPage } from '../utility/utility.page';
import { LoadingService } from '../../shared/services/loading.service';
import { EitPlace } from 'src/app/shared/classes/eitplace';
import { ApiService } from '../../shared/services/api.service';
import { AlertService } from '../../shared/services/alert.service';
import { AppInfo, DB } from '../../shared/services/env.service';
import { UtilityService } from '../../shared/services/utility.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
//import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { NativeGeocoder, NativeGeocoderResult } from '@ionic-native/native-geocoder/ngx';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { CartService } from 'src/app/shared/services/cart.service';
import { AuthService } from 'src/app/shared/services/auth.service';

declare var google;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {

  item = {}
  query: string = "";
  locationData: any;
  
  constructor(public modalController: ModalController, private api: ApiService, public config: ConfigApp, private router: Router, private restService: RestaurantService, 
              private loadingService: LoadingService, private alertService: AlertService, private utilityService: UtilityService,
              private geolocation: Geolocation, private nativeGeo: NativeGeocoder, private nativeStorage: NativeStorage/*, private diagnostic: Diagnostic*/,
              private auth: AuthService, private cartService: CartService) { 
      this.item = {'toolbarTitle': 'Ionic UI - Mikky Theme',
      'title': 'SAVE HOURS',
      'subtitle': 'OF DEVELOPING',
      'subtitle2': 'and make apps fast as light!',
      'link': 'http://csform.com/ionic4/ionic4-UI-mikky-theme/documentation/',
      'description': 'For better understanding how our template works please read documentation.',
      'background': 'assets/imgs/background/39.jpg'};
      let showWizard = localStorage.getItem("SHOW_START_WIZARD");

      /*if (AppSettings.SHOW_START_WIZARD && !showWizard) {
        this.openModal()
      }*/
      this.initData();
  }

  /*async openModal() {
    let modal = await this.modalController.create({component: IntroPage});
     return await modal.present();
  }*/

  initData() {
    this.locationData = {};
    this.locationData.place = new EitPlace();
    this.locationData.place.zipcode = "";
    this.locationData.place.num = "";
    this.locationData.place.nomelocale = "";
    this.locationData.place.latitude = 0;
    this.locationData.place.longitude = 0;
    this.locationData.place.street_address = "";
    this.locationData.place.locality = "";
    this.locationData.place.sublocality = "";
    this.locationData.place.regione = "";
    this.locationData.place.nazione = "";
    this.locationData.place.admin_area_3 = "";

    if (localStorage.getItem("location")) {
      this.locationData.place = JSON.parse(localStorage.getItem("location"));
      this.query = this.locationData.place.description;
    }
  }

  onTextChangeFunc(item): void {
   /* if (event) {
      event.stopPropagation();
    }
    this.onTextChange.emit(this.search);*/
  }

  async searchPlace()
  {
    let modal = await this.modalController.create({component: LocationselectPage});
    (await modal).onDidDismiss().then((result) => {
      if ((result.data.result = this.config.RESULT_OK)) {
        if (null != result.data.location) {
  
          this.query = result.data.location.description;
          this.locationData.place = result.data.location;
          this.locationData.place.numForced = "";
          //this.config.selectPlace(this.geocoder, this.locationData.place, result.location);
        }
      }
    });
  
    return await modal.present();
  }

  accountClick(){
    this.nativeStorage.getItem(DB.USER).then(data =>{
      console.log(data)
      this.router.navigate(['/profile']);
    }, 
    () =>{
      this.router.navigate(['/login']);
    })
  }

  menuClick() {
    /*this.diagnostic.getLocationMode().then(state =>{
      if(state === this.diagnostic.locationMode.LOCATION_OFF) {
        this.diagnostic.switchToLocationSettings();
      }
      else{  */
        this.geolocation.getCurrentPosition().then(position => {            
          console.log(JSON.stringify(position));  
          this.nativeGeo.reverseGeocode( position.coords.latitude, position.coords.longitude).then((result: NativeGeocoderResult[]) =>{
            console.log(JSON.stringify(result[0]));  
            this.router.navigate(['/menu-locale'], { queryParams: { coords: position.coords } });
          }).catch((error: any) => console.log(error));
        }, err => {
          console.log(err);
        })
        .catch((error: any) => {
          console.log(error)
        });
      /*}
    }); */  
  }

  takeawayClick() {
    this.api.postj("apps/getinfoapp", "", "").then(res => {
      console.log(res);
      if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {        
        if(res[this.config.JP_RESULT].count_asporto){
          localStorage.setItem("service", "0");
          let btns = [{text: 'No', role: 'cancel', handler: () => {console.log('Dismiss clicked');}}, 
                      {text: 'Sì', handler: () => {
                        localStorage.setItem("service", "1");
                        if(res[this.config.JP_RESULT].count_restaurants > 1){
                          this.restService.loadTakeAway(this.locationData.place);
                          this.router.navigate(['/restlist'], { /*queryParams: { record: JSON.stringify(order) } */});
                        }
                        else{
                          this.checkAddress();
                        }
                      }}]
          this.alertService.showAlert('Ritira al locale', "Confermi di voler ritirare il tuo ordine direttamente al locale?", btns);
        }
        else this.alertService.showAlert('Ritira al locale', "Servizio non attivo!");
      }
    })
  }

  deliveryClick() {

    if (!(this.locationData.place.num && this.locationData.place.num.length) && (this.locationData.place.numForced && this.locationData.place.numForced.length)) {
      this.locationData.place.num = this.locationData.place.numForced;
    }

    if(!this.config.isOnline()) {
      this.utilityService.showUtilityPage(UtilityType.OFFLINE);
    }
    else if (this.locationData.place.num && this.locationData.place.zipcode && this.locationData.place.num.length && this.locationData.place.zipcode.length) {
      this.api.postj("apps/getinfoapp", "", "").then(res => {
        console.log(res);
        if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {
          if (this.locationData.place.locality === "") this.locationData.place.locality = this.locationData.place.admin_area_3;

          localStorage.setItem("location", JSON.stringify(this.locationData.place));
          localStorage.setItem("service", "0");

          this.restService.loadDelivery(this.locationData.place);  
          if(res[this.config.JP_RESULT].count_restaurants > 1){
            this.router.navigate(['/restlist'], { /*queryParams: { record: JSON.stringify(order) } */});
          }
          else{
            this.checkAddress();
          }
        }
      })
    }
    else if (this.locationData.place.street_address && this.locationData.place.street_address.length) {
      this.selectCivico();
    }
    else {
      this.utilityService.showUtilityPage(UtilityType.ADDRESS);
    }
  }
  
  bookingClick(){
    this.api.postj("apps/getinfoapp", "", "").then(res => {
      console.log(res);
      if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {        
        if(res[this.config.JP_RESULT].count_tavoli){
          if(res[this.config.JP_RESULT].count_restaurants > 1){
            this.router.navigate(['/restlist'], { queryParams: { book: true }});
          }
          else{
            this.loadingService.show();
            this.api.postj("restaurant/recordtables", "", "").then(res => {
              console.log(res);        
              if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {    
                let rest = res[this.config.JP_RESULT]
                localStorage.setItem('restaurant', JSON.stringify(rest));
                this.router.navigate(['/booking']);
              }
              else {
                this.alertService.showAlert("Attenzione", res[this.config.JP_MSG]);
              }        
              this.loadingService.hide();
            }, (err) => {
              console.log(err);
              this.loadingService.hide();
            });
          } 
        }
        else this.alertService.showAlert('Ritira al locale', "Servizio non attivo!");
      }
    })
  }

  async selectCivico() {
    let modal = await this.modalController.create({component: NcivicoPage});
    (await modal).onDidDismiss().then((result) => {
      if ((result.data.result = this.config.RESULT_OK)) {
        if(null!=result.data.civico){
          let geocoder = new google.maps.Geocoder;
          this.locationData.place.num = result.data.civico;
          this.locationData.place.numForced = result.data.civico;
          this.locationData.place.description = this.config.getAddressFromPlace(this.locationData.place);
          this.config.selectAddress(geocoder, this.locationData.place, this.locationData.place.description);
          this.query = this.locationData.place.description;
          //this.goDelivery(); loop*/
        }
      }
      
    });
    return await modal.present();
  }

  async checkAddress() {
    let service =  JSON.parse(localStorage.getItem("service"));
    switch(service){
      case 0:
        //await this.loadingService.show();
        this.api.postj("restaurant/checkaddress", this.locationData, "").then(res => {
          console.log(res);
    
          this.loadingService.hide();
          if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {     
            let rest = res[this.config.JP_RESULT]
            localStorage.setItem('restaurant', JSON.stringify(rest));
            let ispreorder = (!rest.message_event && rest.nowclosed && !rest.day_closed && rest.preorder);
            this.cartService.createCart(rest.id, this.auth.idOrder, this.locationData.place, TypeService.HOME, ispreorder)
            this.router.navigate(['/menu']);
          }
          else if (res[this.config.JP_STATUS] == this.config.JPS_ERROR && res[this.config.JP_MSG]) {
            this.alertService.showAlert("Attenzione", res[this.config.JP_MSG]);
          }
          else {
            console.log(res);
            this.alertService.showAlert('Attenzione', "Il locale è momentaneamente chiuso!");
          }
    
        }, (err) => {
          console.log(err);
          this.loadingService.hide();
        });
        break;
      case 1:
        //devo prendere le info del ristorante
        this.loadingService.show();
        this.api.postj("restaurant/recordtakeaway", "", "").then(res => {
          console.log(res);
    
          if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {    
            let rest = res[this.config.JP_RESULT]
            localStorage.setItem('restaurant', JSON.stringify(rest));
            let ispreorder = (!rest.message_event && rest.nowclosed && !rest.day_closed && rest.preorder);
            this.cartService.createCart(rest.id, this.auth.idOrder, this.locationData, TypeService.TAKEAWAY, ispreorder)
            this.router.navigate(['/menu']);
          }
          else {
            console.log(res);
            this.alertService.showAlert("Attenzione", res[this.config.JP_MSG]);
            //this.showAlert('Attenzione', "Impossibile inviare la richiesta!");
          }
    
          this.loadingService.hide();
        }, (err) => {
          console.log(err);
          this.loadingService.hide();
        });
        break;
    }
  }

}
