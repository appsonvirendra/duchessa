import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { ConfigApp } from '../../shared/services/config-app.service';
import { ModalController } from '@ionic/angular';
import { ToastService } from '../../shared/services/toast.service';

@Component({
  selector: 'app-changepwd',
  templateUrl: './changepwd.page.html',
  styleUrls: ['./changepwd.page.scss'],
})
export class ChangepwdPage implements OnInit {

  item: any = {oldpwd : "", newpwd : "", confirmpwd : ""};
  valid: any = {oldpwd: true, newpwd: true, confirmpwd: true}

  constructor(private api: ApiService, public config: ConfigApp, private modalController: ModalController, private toastService: ToastService) { }

  ngOnInit() {
    console.log("changepwd")
  }

  savePwd(){
    if(this.validate()){
        this.api.postj("user/settings/chkpwd", this.item, "").then(res => {
          if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {
            this.toastService.presentToast("Password modificata con successo!");  
            this.dismiss({'result': this.config.RESULT_OK});
          }
          else if (res[this.config.JP_STATUS] == this.config.JPS_ERROR && res[this.config.JP_MSG]) {
            res[this.config.JP_MSG].forEach(msg => {
              this.toastService.presentToast(msg); 
            });
          }
          else {
            console.log(res);
          }
        }, (err) => {
          console.log(err);
        });
      }
      else{
        this.toastService.presentToast("Controlla i dati inseriti!");  
      }
  }

  validate(): boolean {
    this.valid.oldpwd = this.item.oldpwd && this.item.oldpwd.length >= 6;
    this.valid.newpwd = this.item.newpwd && this.item.newpwd.length >= 6;
    this.valid.confirmpwd = this.item.confirmpwd && this.item.newpwd === this.item.confirmpwd;

    return this.valid.oldpwd && this.valid.newpwd && this.valid.confirmpwd;
  }

  dismiss(data: any) {
    this.modalController.dismiss(data);
  }
}
