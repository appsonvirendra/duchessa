import { Component, OnInit, ChangeDetectorRef, NgZone } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { ConfigApp, TypeService } from '../../shared/services/config-app.service';
import { AppInfo } from '../../shared/services/env.service';
import { EitPlace } from '../../shared/classes/eitplace';
import { EitCart } from '../../shared/classes/eitcart';
import { LoadingService } from '../../shared/services/loading.service';
import { AlertController, ModalController } from '@ionic/angular';
import { CustomMenuItemPage } from '../custom-menu-item/custom-menu-item.page';
import { Router } from '@angular/router';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { DB } from 'src/app/shared/services/env.service';
import { CartService } from '../../shared/services/cart.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  rId: number = 0;
  locationData: any = "";
  cartData: any = "";
  service: any = "1";
  restaurantBase: any;
  restaurantFull: any = null;
  menu: any = {categories: []};
  categories: any = [];
  selectedCat : any;
  isActive : boolean;
  appInfo: any = AppInfo;

  constructor(public zone: NgZone, private api: ApiService, private cartService: CartService, public config: ConfigApp, private loadingService: LoadingService, private cd: ChangeDetectorRef, private alertCtrl: AlertController,
              private modalController: ModalController, private router: Router, private nativeStorage: NativeStorage){
    console.log("menu")
    this.cartData = new EitCart();
    this.locationData = new EitPlace();
    if (JSON.parse(localStorage.getItem("location"))) {
      this.locationData = JSON.parse(localStorage.getItem("location"));
    }
    
    this.cartService.cartObs.subscribe(x => {

      this.cartData = x;
     
    });
    

    var service = localStorage.getItem("service");
    if (service!=undefined  && service!="undefined" && service!=null && service!="null") {
      this.service = service;
    }
    console.log("service: " + service);    

    if (JSON.parse(localStorage.getItem("restaurant"))) {
      this.restaurantBase = JSON.parse(localStorage.getItem("restaurant"));
    }
  }

  ngOnInit() {
    console.log("menu");
    setTimeout(()=> {
      this.loadingService.hide();
    }, 3000);
  }

  ionViewWillEnter(){
    console.log("menu enter")
    if(this.restaurantBase){
    
      this.fill();
      this.fillMenu();
    }    
  }

  fill() {
    var data: any = {};
    data.rest_id = this.restaurantBase.id;
    //data.order_id = this.cartData.order_id;
    data.service = this.service=="0" ? TypeService.HOME : TypeService.TAKEAWAY;
    data.location = this.locationData;
    this.api.postj("restaurant/delivery", data, "").then(res => {
      console.log(res);
      if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {
        this.restaurantFull = res[this.config.JP_RESULT]["r"];
        localStorage.setItem("restaurantFull", JSON.stringify(this.restaurantFull));
      } 
      else {
        console.log(res);
      }
    }, err => {
      console.log(err);
    });
  }

  fillMenu() {
    this.loadingService.show();
    this.api.postj("restaurant/menu", this.restaurantBase.menu, "").then(res => {
      console.log(res);
      this.loadingService.hide();
      if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {
        this.menu = res[this.config.JP_RESULT];
        this.categories = Array.of(this.menu.categories);
        this.cd.detectChanges()
        this.selectedCat = 0
      }
      else {
        console.log(res);
      }
    },err => {
      console.log(err);
      this.loadingService.hide();
    });
  }

  shortDescr(item: any) {
    return item.descr.length > 0 && item.descr.length < 15 ? " (" + item.descr + ")" : "";
  }

  longDescr(item: any) {
    return item.descr.length > 15 ? item.descr + " " : "";
  }

  ingredients = function(item: any) {
    var result = "";
    for (var iG = 0; iG < item.ingredients.length; iG++) {
      result += item.ingredients[iG].name;
      if (iG < item.ingredients.length - 1) {
        result += ", ";
      }
    }
    return result;
  }

  addToCart(menuitem : any, variante: any) {
    console.log ('add to cart');
    var item = variante == null ? menuitem : variante;
    if (this.restaurantFull.nowclosed) {
      if (this.restaurantFull.preorder) {
        if (item != null) {
          if (0 == item.custom_type && item.joins.length == 0) {
            this.cartService.addToCart(item.id, 1, "", 1, "");
          } 
          else if (0 != item.custom_type || item.joins.length > 0) {
            this.showCustom({item: item, cartData: this.cartData, parent: menuitem})
          }
        }
      } 
      else {
        this.infoRestClosed();
      }
    } 
    else if (this.restaurantFull.dayclosed) {
      this.infoRestClosed();
    } 
    else if (item != null) {
      if (0 == item.custom_type && item.joins.length == 0) {
        this.cartService.addToCart(item.id, 1, "", 1, "");
      } 
      else if (0 != item.custom_type || item.joins.length > 0) {
        this.showCustom({item: item, cartData: this.cartData, parent: menuitem})
      }
    }
  }

  async showCustom(data: any){    
    let modal = await this.modalController.create({component: CustomMenuItemPage, componentProps: data});
   /* (await modal).onDidDismiss().then((result) => {
      //if ((result.data.result = this.config.RESULT_OK) && result)  this.addToCartWithSubitems(result);
    });  */
    return await modal.present();
  }

  addToCartP(item: any) {
    var dataCall:any = {};
    if (this.restaurantFull.nowclosed) {
      if (this.restaurantFull.preorder) {
        if (item != null) {
          this.cartService.addToCartSingle(item.id, 1, "", 1);
        }
      } 
      else {
        this.infoRestClosed();
      }
    } 
    else if (this.restaurantFull.dayclosed) {
      this.infoRestClosed();
    } 
    else if (item != null) { 
      this.cartService.addToCartSingle(item.id, 1, "", 1);
    }
  }

  removeToCartP(item: any) {
    if (item != null) {
      this.cartService.removeToCartP(item.id, 1, 1);
    }
  }

  addToCartWithSubitems(result: any){
    console.log("add with sub");
    console.log(JSON.stringify(result));
    this.cartData = result.data;
    console.log(JSON.stringify(this.cartData));
    localStorage.setItem("cart", JSON.stringify(this.cartData));
  }

  async infoRestClosed() {
    let alert = await this.alertCtrl.create({
      header: AppInfo.APP_NAME,
      message: "Il locale al momento è chiuso!",
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
            console.log('OK clicked');
          }
        }
      ]
    });
    await alert.present();
  }

  async goToLogin(){
    this.router.navigate(['/login']);
  }

  goToCart(){
    this.nativeStorage.getItem(DB.USER).then(data =>{
      this.zone.run(() => {
      console.log(data)
      if((this.restaurantFull.nowclosed && !this.restaurantFull.preorder) || this.restaurantFull.dayclosed) {
        this.infoRestClosed();
      } 
      else {
        this.router.navigate(['/cart']);
      }
    });
    }, 
    () =>{
      this.goToLogin();
    })
    /*if (localStorage.getItem("userData")) {
      if((this.restaurantFull.nowclosed && !this.restaurantFull.preorder) || this.restaurantFull.dayclosed) {
        this.infoRestClosed();
      } 
      else {
        this.router.navigate(['/cart']);
      }
    } 
    else {
      this.goToLogin();
    }*/
  }

  goToHome(){
    this.router.navigateByUrl('/');
  }
}
