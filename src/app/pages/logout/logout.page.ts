import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../shared/services/auth.service';
import { MenuService } from '../../shared/services/menu-service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.page.html',
  styleUrls: ['./logout.page.scss'],
})
export class LogoutPage implements OnInit {

  data:any = {
    'title': 'Vuoi uscire dal tuo account?',
    'send': 'Esci',
    'logo': 'assets/imgs/logo.png',
    'errorIcon': 'remove-circle'
  }

  constructor(private authenticationService: AuthService, private menuService: MenuService, private router: Router, private location: Location) { }

  ngOnInit() {
  }

  logout(){
    this.authenticationService.logout();
    this.menuService.initMenu();
    //this.menuService.getAllThemes();
    this.router.navigateByUrl('/');
  }

  goBack() {
    this.location.back();
  }   

}
