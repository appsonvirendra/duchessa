import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CartbookPageRoutingModule } from './cartbook-routing.module';

import { CartbookPage } from './cartbook.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CartbookPageRoutingModule
  ],
  declarations: [CartbookPage]
})
export class CartbookPageModule {}
