import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CartbookPage } from './cartbook.page';

describe('CartbookPage', () => {
  let component: CartbookPage;
  let fixture: ComponentFixture<CartbookPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartbookPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CartbookPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
