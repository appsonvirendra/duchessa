import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { AppInfo, DB } from '../../shared/services/env.service';
import { ConfigApp, TypeOrder } from '../../shared/services/config-app.service';
import { LoadingService } from '../../shared/services/loading.service';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-cartbook',
  templateUrl: './cartbook.page.html',
  styleUrls: ['./cartbook.page.scss'],
})
export class CartbookPage implements OnInit {

  cartBookData: any = "";
  typeOrder: any = TypeOrder;
  appInfo: any = AppInfo;
  items: Array<any> = []
  isDomicilio: boolean = true;

  constructor(private api: ApiService, public config: ConfigApp, private modalController: ModalController, private loadingService: LoadingService) {  
    let booktemp = localStorage.getItem(DB.CARTBOOK);
    if(booktemp) {
      this.cartBookData = JSON.parse(booktemp);
      this.toArray()
    }
  }

  ngOnInit() {
    console.log("cart")
  }

  dismiss() {
    this.modalController.dismiss({result: this.config.RESULT_CANCEL});
  }

  addToCartP(item: any) {
    if (item != null) {
      this.loadingService.show();
      this.api.postj( "cart/addptoc", {orderid: this.cartBookData.order_id, productid: item.id, returndata: 1, qty: 1}, "").then(res => {
        if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {
          this.cartBookData = res[this.config.JP_RESULT];
          localStorage.setItem(DB.CARTBOOK, JSON.stringify(this.cartBookData))
          this.toArray()
        }
        else console.log(res);  
        this.loadingService.hide();
      }, err => {
        console.log(err);
        this.loadingService.hide();
      });
    }
  }

  removeToCartP(item: any) {
    if (item != null) {
      this.loadingService.show();
      this.api.postj("cart/removemtoc", {orderid: this.cartBookData.order_id, productid: item.id, returndata: 1, qty: 1}, "").then(res => {
        if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {
          this.cartBookData = res[this.config.JP_RESULT];
          localStorage.setItem(DB.CARTBOOK, JSON.stringify(this.cartBookData))
          this.toArray()
        }
        else console.log(res);  
        this.loadingService.hide();
      }, err => {
        console.log(err);
        this.loadingService.hide();
      });
    }
  }

  goToCheckout() {
    this.modalController.dismiss({result: this.config.RESULT_OK});
  }

  toArray() {
    if (this.cartBookData.order && this.cartBookData.order.items) {
      this.items = Object.keys(this.cartBookData.order.items).map(key => ({ type: key, value: this.cartBookData.order.items[key] }));
    }
  }
}

