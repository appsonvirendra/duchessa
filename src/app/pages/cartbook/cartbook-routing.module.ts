import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CartbookPage } from './cartbook.page';

const routes: Routes = [
  {
    path: '',
    component: CartbookPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CartbookPageRoutingModule {}
