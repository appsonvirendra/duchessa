import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RestlistPage } from './restlist.page';

const routes: Routes = [
  {
    path: '',
    component: RestlistPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RestlistPageRoutingModule {}
