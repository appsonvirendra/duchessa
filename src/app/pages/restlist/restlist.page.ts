import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { AuthService } from '../../shared/services/auth.service';
import { CartService } from '../../shared/services/cart.service';
import { ConfigApp, TypeService } from '../../shared/services/config-app.service';
import { LoadingService } from '../../shared/services/loading.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AppInfo } from '../../shared/services/env.service';
import { RestaurantService } from '../../shared/services/restaurant.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-restlist',
  templateUrl: './restlist.page.html',
  styleUrls: ['./restlist.page.scss'],
})
export class RestlistPage implements OnInit {

  @Input() records: any = [];

  locationData: any = {};  
  locTmp: any;
  allRecords: any = [];  
  curTime:any;
  query: string = "";
  queryTmp: string = "";
  searchText:any;
  appInfo: any = AppInfo;
  service: number = 0;
  listonly: boolean = false;

  constructor(private restService: RestaurantService, private auth: AuthService, private cartService: CartService, private api: ApiService, public config: ConfigApp, private loadingService: LoadingService, private route: ActivatedRoute, private router: Router, private location: Location) { 

    this.records = [];

    this.restService.restaurants.subscribe(x => {

      this.records = x;

    });
  }

  ngOnInit() {
    console.log("restlist");
   
    this.listonly = this.route.snapshot.data.listonly;
    if(this.listonly)
    {
      this.restService.loadInfo(null);
    }

    this.locationData = JSON.parse(localStorage.getItem("location"));

    setTimeout(()=> {
      this.loadingService.hide();
    }, 2000)

    this.curTime = new Date().getHours(); 
  }

  goToRestaurant(item: any){
    if(this.listonly) this.router.navigate(['/inforestaurant'], {queryParams: {restaurant: JSON.stringify(item)}});
    else{
      localStorage.setItem('restaurant', JSON.stringify(item));

      if (item) {
        var ispreorder = (!item.message_event && item.nowclosed && !item.day_closed && item.preorder);
        this.cartService.createCart(item.id, this.auth.idOrder, this.locationData, this.service==0 ? TypeService.HOME : TypeService.TAKEAWAY, ispreorder)
      }
      this.router.navigate(['/menu']);
    }
  }

  goBack() {
    this.location.back();
  }     

}
