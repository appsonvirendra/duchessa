import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RestlistPageRoutingModule } from './restlist-routing.module';

import { RestlistPage } from './restlist.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RestlistPageRoutingModule
  ],
  declarations: [RestlistPage]
})
export class RestlistPageModule {}
