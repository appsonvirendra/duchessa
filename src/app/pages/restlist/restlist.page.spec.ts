import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RestlistPage } from './restlist.page';

describe('RestlistPage', () => {
  let component: RestlistPage;
  let fixture: ComponentFixture<RestlistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestlistPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RestlistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
