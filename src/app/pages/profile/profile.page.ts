import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { ConfigApp } from '../../shared/services/config-app.service';
import { AppInfo } from '../../shared/services/env.service';
import { LoadingService } from '../../shared/services/loading.service';
import { ModalController } from '@ionic/angular';
import { ToastService } from '../../shared/services/toast.service';
import { ChangepwdPage } from '../changepwd/changepwd.page';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  item: any = {}
  valid: any = {email: true, name: true, lastname: true, phone: true, address: true, num: true, townstr: true, zip: true, citofono: true}
  private regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  constructor(private api: ApiService, public config: ConfigApp, private loadingService: LoadingService, private modalController: ModalController, private toastService: ToastService, private navController: NavController) { }

  ngOnInit() {
    this.fill()
  }

  async fill() {
    await this.loadingService.show();
    this.api.postj("user/settings/infoaccount", "", "").then(res => {
      console.log(res);
      if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {
        this.item = res[this.config.JP_RESULT];
      }
      else {
        console.log(res);
      }
      this.loadingService.hide();
    }, (err) => {
      console.log(err);
      this.loadingService.hide();
    })
  }

  async updateAccount(){
    if(this.validate()){
      await this.loadingService.show();
      this.api.postj("user/settings/updateaccount", this.item, "").then(res => {
        if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {
          this.toastService.presentToast("Profilo aggiornato!");  
        }
        else {
          console.log(res);
        }

        this.loadingService.hide();
      }, (err) => {
        console.log(err);
        this.loadingService.hide();
      })
    }
  }
 
  async changePwd(){
    let modal = await this.modalController.create({component: ChangepwdPage});
    return await modal.present();
  }
 
  validate(): boolean {
    this.valid.name = this.item.name && this.item.name.length > 0;
    this.valid.lastname = this.item.lastname && this.item.lastname.length > 0;
    this.valid.phone = this.item.phone && this.item.phone.length > 0;
    this.valid.address = this.item.address && this.item.address.length > 0;
    this.valid.num = this.item.num && this.item.num.length > 0;
    this.valid.townstr = this.item.townstr && this.item.townstr.length > 0;
    this.valid.zip = this.item.zip && this.item.zip.length > 0;
    this.valid.citofono = this.item.citofono && this.item.citofono.length > 0;
    this.valid.email = this.regex.test(this.item.email);

    return this.valid.email && this.valid.address && this.valid.num && this.valid.name && this.valid.lastname && 
           this.valid.num && this.valid.townstr && this.valid.zip && this.valid.citofono;
  }

  goBack() {
    this.navController.back();
  }  

}
