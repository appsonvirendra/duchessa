import { Component, OnInit, ChangeDetectorRef, NgZone } from '@angular/core';
import { ConfigApp, TypePayment/*, PaypalSettings*/, StatusOrder, TypeOrder, ET_OpzioniCoupon } from '../../shared/services/config-app.service';
import { ApiService } from '../../shared/services/api.service';
import { Location } from '@angular/common';
import { ModalController } from '@ionic/angular';
import { CashPayPage } from '../cash-pay/cash-pay.page';
import { LoadingService } from '../../shared/services/loading.service';
import { CartService } from '../../shared/services/cart.service';
import { Router } from '@angular/router';
//import { PayPal, PayPalPayment, PayPalConfiguration } from "@ionic-native/paypal/ngx";
import { AlertService } from '../../shared/services/alert.service';
import { HTTP } from '@ionic-native/http/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CardPayPage } from '../card-pay/card-pay.page';
import { PaypalPayPage } from '../paypal-pay/paypal-pay.page';
import { AppInfo, EitUrl, Satispay } from '../../shared/services/env.service';
import { CreditPayPage } from '../credit-pay/credit-pay.page';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-checkpay',
  templateUrl: './checkpay.page.html',
  styleUrls: ['./checkpay.page.scss'],
})
export class CheckpayPage implements OnInit {

  cartData: any = "";
  hideCash: boolean = false;
  payType: any = TypePayment;
  restaurantBase: any;
  payManaged: boolean = false;
  dataPay: any = {}
  cartChanges:Subscription;
  isDomicilio: boolean = true;

  constructor(private api: ApiService, private location: Location, private modalController: ModalController, public config: ConfigApp, private loadingService: LoadingService,
    private router: Router, private cartService: CartService, public http: HTTP, public iab: InAppBrowser, private cdRef: ChangeDetectorRef,
    private alertService: AlertService) {
    this.cartChanges = this.cartService.cartObs.subscribe(x => {

      this.cartData = x;
      if (x && this.cartData.order) {

        this.isDomicilio = this.cartData.order.type_order == TypeOrder.EIT_DOMICILIO || this.cartData.order.type_order == TypeOrder.REST_DOMICILIO
        this.dataPay.restid = this.cartData.rest_id;
        this.dataPay.orderid = this.cartData.order_id;
        this.dataPay.payment = {};
        this.dataPay.response = {};
        let isTakeaway = this.cartData.order.type_order == TypeOrder.EIT_ASPORTO || this.cartData.order.type_order == TypeOrder.REST_ASPORTO;
        this.hideCash = this.cartData.order.opzionecoupon == ET_OpzioniCoupon.ONLYCARDS || (isTakeaway && !this.cartData.showcashontakeaway);
      }

    });
  }

  ionViewWillEnter(){
    this.cartService.cartObs.subscribe(x => {
      this.cartData = x;
      if (!this.cartData.order || this.cartData.order.status == 1) this.goToHome();
    })
  }

  ngOnInit() {
   
    var restBase = JSON.parse(localStorage.getItem("restaurant"));
    if (restBase != undefined && restBase != null) {
      this.restaurantBase = restBase;
    }
  }

  goToHome() {
    this.router.navigateByUrl('/');
  }

  onItemClick(type: number) {
    this.payManaged = false;

    switch (type) {
      case TypePayment.MONEY: this.onCash(); break;
      case TypePayment.PAYPAL: this.onPaypal(); break;
      case TypePayment.CARD: this.onCard(); break;
      case TypePayment.CREDIT: this.onCredit(); break;
      case TypePayment.SATISPAY: this.onSatispay(); break;
    }
  }

  async onCash() {
    let modal = await this.modalController.create({ component: CashPayPage, componentProps: {order: this.cartData.order} });
    (await modal).onDidDismiss().then((result) => {
      if (result && result.data && result.data.result == this.config.JPS_OK) {
        this.dataPay.type = TypePayment.MONEY;
        this.finalize();
      }
    });
    return await modal.present();
  }

  async onPaypal() {
    let modal = await this.modalController.create({ component: PaypalPayPage, componentProps: { cart: this.cartData } });
    (await modal).onDidDismiss().then((result) => {
      if (result && result.data && result.data.result == this.config.RESULT_OK) {
        this.dataPay.type = TypePayment.PAYPAL;
        this.dataPay.response.response = result.data.response;
        this.finalize();
      } else {
        this.alertService.showAlert("", "Attenzione! Pagamento non riuscito!");
      }

    });
    return await modal.present();
  }

  async onCard() {
    let modal = await this.modalController.create({ component: CardPayPage, componentProps: { id: this.cartData.order.id } });
    (await modal).onDidDismiss().then((result) => {
      if (result && result.data && result.data.result == this.config.RESULT_OK) {
        this.cartData.order.payment_id = TypePayment.CARD;
        localStorage.setItem("cart", JSON.stringify(this.cartData));

        var ref = this.iab.create(result.data.href, "_blank", "clearsessioncache=yes,clearcache=yes,hardwareback=yes,footer=no,location=no");

        ref.on("loadstart").subscribe(event => {
          console.log("Loading started: " + event.url);
          if (event.url.match(EitUrl.urlCardOk)) {
            this.payManaged = true;
            console.log("url managed: " + event);
            ref.close();
            this.manageCard(event);
          }
          if (event.url.match(EitUrl.urlCardKo)) {
            console.log("url managed: " + event);
            ref.close();
            this.payManaged = true;
            this.alertService.showAlert("", "Attenzione! Pagamento non autorizzato!");
          }
        });

        ref.on("loadstop").subscribe(event => {
          console.log("Loading finished: " + event.url);
          if (this.payManaged == false) {
            if (event.url.match(EitUrl.urlCardOk)) {
              this.payManaged = true;
              console.log("url managed: " + event);
              ref.close();
              this.manageCard(event);
            }
            if (event.url.match(EitUrl.urlCardKo)) {
              console.log("url managed: " + event);
              ref.close();
              this.payManaged = true;
              this.alertService.showAlert("", "Attenzione! Pagamento non autorizzato!");
            }
          }
        });

        ref.on("loaderror").subscribe(event => {
          console.log("Loading error: " + event.message);
          ref.close();
          if (!this.payManaged) {
            this.recordPreaccepted();
          }
        });

        ref.on("exit").subscribe(() => {
          console.log("Browser is closed...");
          if (!this.payManaged) {
            this.recordPreaccepted();
          }
        });
      }
    });
    return await modal.present();
  }

  async onCredit() {
    let modal = await this.modalController.create({ component: CreditPayPage, componentProps: { order: this.cartData.order } });
    (await modal).onDidDismiss().then((result) => {
      if (result && result.data && result.data.result == this.config.RESULT_OK) {

        var tot = this.cartData.order.euro_total.toFixed(2); //this.cartData.order.euro_total;// - this.cartData.order.credit - this.cartData.order.creditB - this.cartData.order.creditCorp;        
        localStorage.setItem("cart", JSON.stringify(this.cartData));
        if (tot == 0) {
          this.dataPay.type = TypePayment.CREDIT;
          this.finalize()
        }
      }
    });
    return await modal.present();
  }

  onSatispay() {
    this.http.setDataSerializer("json");
    var urlSat = Satispay.urlRel + "/online/v1/checkouts";
    var descr = "VIOTTI_01_Order_" + this.cartData.order_id;
    var dataSat = {
      phone_number: this.cartData.order.phone,
      redirect_url: Satispay.urlReturn,
      description: descr,
      callback_url: Satispay.urlCallback+"&idApp="+ AppInfo.APP_ID+"&idOrder="+this.cartData.order_id,
      amount_unit: this.cartData.order.euro_total * 100,
      currency: "EUR",
      metadata: {
        orderid: this.cartData.order_id
      }
    };

    var headers = {
      "Content-Type": "application/json",
      Authorization: "Bearer " + this.cartData.SPKR
    };

    this.http.post(urlSat, dataSat, headers).then(res => {
      if (res && res.status == 200) {
        var resultData = JSON.parse(res.data);
        let target = "_blank";
        var ref = this.iab.create(resultData.checkout_url, target, "clearsessioncache=yes,clearcache=yes,hardwareback=yes,footer=yes,location=no");
        ref.on("loadstart").subscribe(event => {
          console.log("Loading started: " + event.url);
          if (event.url.match(Satispay.urlReturn)) {
            console.log("Loading finished: " + event);
            this.payManaged = true;
            ref.close();

            this.dataPay.type = TypePayment.SATISPAY;
            this.dataPay.satispay = event.url;
            this.dataPay.response = event;

            this.finalize();
          }
        });

        ref.on("loaderror").subscribe(event => {
          console.log("Loading error: " + event.message);
        });

        ref.on("exit").subscribe(() => {
          console.log("Browser is closed...");
          if (this.payManaged == false) {
            this.recordPreaccepted();
          }
        });
      }
    }, err => {
      console.log(err);
    });
  }

  manageCard(event: any) {
    // this.loadingService.show();
    this.dataPay.type = TypePayment.CARD;
    this.dataPay.gestpay = event.url;
    this.dataPay.response = event;
    this.finalize();
  }

  finalize() {
    this.loadingService.show();
    this.api.postj("order/finalize", this.dataPay, "").then(res => {
      console.log(res);
      this.loadingService.hide();
      if (res[this.config.JP_STATUS] == this.config.JPS_OK) {
        this.cartData.order.payment_id = this.dataPay.type;
        this.cartService.next(this.cartData);
        this.closeOrder();
      }
      else {
        this.alertService.showAlert("", "Attenzione! Pagamento non riuscito!");
      }
    },
      err => {
        console.log(err);
        this.loadingService.hide();
      });
  }

  recordPreaccepted() {
    this.api.postj("order/recordpreaccepted", { orderid: this.cartData.order_id }, "").then(resCk => {
      if (resCk[this.config.JP_STATUS] == this.config.JPS_OK && resCk[this.config.JP_RESULT]) {
        this.cartData.order.status = resCk[this.config.JP_RESULT]["status"];
        if (this.cartData.order.status == StatusOrder.PAID || this.cartData.order.status == StatusOrder.ACCEPTED ||
          this.cartData.order.status == StatusOrder.PREORDERED || this.cartData.order.status == StatusOrder.ACCEPTED_NOTIFIED || this.cartData.order.status == StatusOrder.PREACCEPTED) {
          this.payManaged = true;
          this.cartData.order.payment_id = this.dataPay.type;
          this.cartService.next(this.cartData);
          this.cdRef.detectChanges();
          this.closeOrder();
        }
      }
    }, err => {
      this.loadingService.hide();
      console.log(err);
    });
  }

  closeOrder() {
    this.cartService.loadCart(this.cartData.order_id, true);
    //this.cartChanges.unsubscribe();
    this.router.navigate(['/check']);
  }

  goBack() {
    this.location.back();
  }
}
