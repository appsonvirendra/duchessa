import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CheckpayPage } from './checkpay.page';

describe('CheckpayPage', () => {
  let component: CheckpayPage;
  let fixture: ComponentFixture<CheckpayPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckpayPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CheckpayPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
