import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CheckpayPage } from './checkpay.page';

const routes: Routes = [
  {
    path: '',
    component: CheckpayPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CheckpayPageRoutingModule {}
