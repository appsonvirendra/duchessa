import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CheckpayPageRoutingModule } from './checkpay-routing.module';

import { CheckpayPage } from './checkpay.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CheckpayPageRoutingModule
  ],
  declarations: [CheckpayPage]
})
export class CheckpayPageModule {}
