import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AuthService } from '../services/auth.service';

import { EitUrl } from '../services/env.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        let user = this.authenticationService.currentUserValue;
       
        //console.log(currentUser)
        if (user && user.access_token ) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${user.access_token}`
                },
                setParams:{ 'access-token': user.access_token}
            });
        }

        return next.handle(request);
    }
}