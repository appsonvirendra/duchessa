import { NgModule } from '@angular/core';
import { ArrayPipe } from './array/array';
@NgModule({
	declarations: [ArrayPipe],
	imports: [],
	exports: [ArrayPipe]
})
export class PipesModule {}
