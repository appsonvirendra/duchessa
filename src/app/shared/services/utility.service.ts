import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { UtilityPage } from '../../pages/utility/utility.page';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {

  constructor(private modalController: ModalController) { }

  async showUtilityPage(type: number, info?: any){
    let modal = await this.modalController.create({component: UtilityPage, componentProps: {type: type, info: info}});    
    return await modal.present();
  }

}
