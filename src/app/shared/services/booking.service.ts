import { Injectable } from '@angular/core';
import { RESPONSE, DB } from '../services/env.service';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BookingService {

  private bookSubject: BehaviorSubject<any>;
  public bookObs: Observable<any>;
  private cartSubject: BehaviorSubject<any>;
  public cartObs: Observable<any>;

  constructor() { 
    this.cartSubject = new BehaviorSubject<any>(null);
    this.cartObs = this.cartSubject.asObservable();
    this.bookSubject = new BehaviorSubject<any>(null);
    this.bookObs = this.cartSubject.asObservable();
  }

  public get cart(): any {
    return this.cartSubject.value;
  }

  public get book(): any {
    return this.bookSubject.value;
  }

  public nextCart(value: any) {
    localStorage.setItem(DB.CARTBOOK, JSON.stringify(value));
    this.cartSubject.next(value);
  }

  public nextBook(value: any) {
    localStorage.setItem(DB.BOOK, JSON.stringify(value));
    this.bookSubject.next(value);
  }

  reset(){
    this.resetBook();
    this.resetCart();
  }

  resetCart() {
    this.cartSubject.next(null);
  }

  resetBook() {
    this.bookSubject.next(null);
  }

}
