import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { ApiService } from './api.service';
import { LoadingService } from './loading.service';
import { ConfigApp } from './config-app.service';
import { Restaurant } from '../classes/restaurant';

@Injectable({
  providedIn: 'root'
})
export class RestaurantService {

  private restaurantsrSubject: BehaviorSubject<Restaurant[]>;
  public restaurants: Observable<Restaurant[]>;

  private menuSubject: BehaviorSubject<any>;
  public menu: Observable<any>;


  constructor(private api: ApiService, private config: ConfigApp, private loadingService: LoadingService) {

    this.restaurantsrSubject = new BehaviorSubject<Restaurant[]>([]);
    this.restaurants = this.restaurantsrSubject.asObservable();

    this.menuSubject = new BehaviorSubject<any>(null);
    this.menu = this.menuSubject.asObservable();
  }

  public get currentDelivery(): Restaurant[] {
    return this.restaurantsrSubject.value;
  }

  public get currentMenu(): any {
    return this.menuSubject.value;
  }

  resetDelivery() {
    this.restaurantsrSubject.next([]);
  } 

  loadDelivery(location: any) {
    this.loadingService.show();
    return this.api.postj("restaurant/rdelivery", location).then((data: any) => {
      if (data[this.config.JP_STATUS] == this.config.JPS_OK && data[this.config.JP_RESULT] && data[this.config.JP_RESULT].length > 0) {
        this.restaurantsrSubject.next(data[this.config.JP_RESULT]);
      }
      this.loadingService.hide();
    }, (err: any) => {
      console.log(err);
      this.loadingService.hide();
    })
  }

  loadTakeAway(location: any) {
    this.loadingService.show();
    location.zipcode = location.zipcode == '' ? -1 : location.zipcode;
    return this.api.postj("restaurant/rtakeaway", location).then((data: any) => {
      if (data[this.config.JP_STATUS] == this.config.JPS_OK && data[this.config.JP_RESULT] && data[this.config.JP_RESULT].length > 0) {
        this.restaurantsrSubject.next(data[this.config.JP_RESULT]);
      }
      this.loadingService.hide();
    }, (err: any) => {
      console.log(err);
      this.loadingService.hide();
    })
  }

  loadInfo(location: any) {
    this.loadingService.show();
    return this.api.postj("restaurant/getinforest", location).then((data: any) => {
      if (data[this.config.JP_STATUS] == this.config.JPS_OK && data[this.config.JP_RESULT] && data[this.config.JP_RESULT].length > 0) {
        this.restaurantsrSubject.next(data[this.config.JP_RESULT]);
      }
      this.loadingService.hide();
    }, (err: any) => {
      console.log(err);
      this.loadingService.hide();
    })
  }

  loadMenu(menu_id: number) {
    this.loadingService.show();
    return this.api.postj("restaurant/menu", {id: menu_id}).then((data: any) => {
      if (data[this.config.JP_STATUS] == this.config.JPS_OK && data[this.config.JP_RESULT]) {      
        this.menuSubject.next(data[this.config.JP_RESULT]);
      }
      this.loadingService.hide();
    }, (err: any) => {
      console.log(err);
      this.loadingService.hide();
    })
  }
}
