import { Injectable } from '@angular/core';
import { Observable, throwError, Subscription } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { map } from 'rxjs/operators';
//import { ToasterConfig } from 'angular2-toaster';


import { ApiService } from './api.service';
import { ConnectivityService } from './connectivity.service';


@Injectable({
    providedIn: 'root'
})

export class ConfigApp {

    public RESULT_OK = 1;
    public RESULT_CANCEL = 0;

    public JP_STATUS = "status";
    public JP_RESULT = "result";
    public JP_RURL = "url";
    public JP_MSG = "msg";
    public JP_HTML = "html";

    public JPS_OK = "ok";
    public JPS_ERROR = "error";
    public JPS_REDIRECT = "redirect";
    public JPS_VIEW = "view";

    public DBKEY_LOCATION = "location";
    public DBKEY_USER = "currentuser";
    public DBSETTINGS = "settings";

    public JPIO_READ = 0;
    public JPIO_WRITE = 1;

    public DRIVER_CALL_MIN = 30;

    public JParamRecord = {
        UNDEF : - 1,
        STD : 0,
        FULL : 1,
        FAST : 2,
        MEDIUM : 3,
        EDIT : 4,	
        LIST_STD : 5,	
        TABS_INFO : 6
     }

     public VAT = 22;
     

     /*public toasterConfig = new ToasterConfig({
        positionClass: 'toast-top-center',
        showCloseButton: true
     });*/
  
     public bsConfig = {
        containerClass: 'theme-eit'
     } 
     
     public bsConfigRange = {
        containerClass: 'theme-eit',
        dateInputFormat: 'dd/MM/yyyy'
     }

     componentForm: any = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        country: 'long_name',
        postal_code: 'short_name',
        administrative_area_level_1: 'short_name',
        administrative_area_level_2: 'short_name',
        administrative_area_level_3: 'short_name'
    };

    _modelsCb: any = new Array();
    _models: any = new Array();


    constructor(private api: ApiService, public connectivityService: ConnectivityService) { }


    Combo = function (module, type) {
        var result = this._modelsCb["0:" + module + ":" + type];
        result = result == null ? new Array() : result;
        return result;
    }
    /*
     * type = controller:conn
     */
    LoadModels(typeArray, forced, async, functionSuccess)  {
        var result = new Array();
        var loadArray = new Array();
        if (async == null) {
            async = false;
        }
        if (forced != null && forced == true) {
            /*for (var type in typeArray) {
                var tipominuscolo = typeArray[type].toLowerCase();*/
            var index = 0;
            for (index = 0; index < typeArray.length; ++index) {
                var type = typeArray[index];
                var tipominuscolo = type.toLowerCase();
                var controller = tipominuscolo.charAt(0).toUpperCase() + tipominuscolo.slice(1);
                loadArray.push(controller);
            }
        }
        else {
            var index = 0;
            for (index = 0; index < typeArray.length; ++index) {
                var type = typeArray[index];
                // typeArray.forEach(function(type) {
                //var tipominuscolo = typeArray[type].toLowerCase();
                var tipominuscolo = type.toLowerCase();
                var controller = tipominuscolo.charAt(0).toUpperCase() + tipominuscolo.slice(1);
                if (this._models[controller] == null) {
                    loadArray.push(controller);
                }
                else {
                    result[controller] = this._models[controller];
                }
            }
        }
        // if (loadArray.length > 0) {
        var httpData: any = {};
        httpData.combo = loadArray;
        return this.api.postj('utils/loadcombo', httpData)
            .then(data => {
                    if (data[this.JP_STATUS] == this.JPS_OK &&
                        data[this.JP_RESULT]) {

                        // var result  = new Array();
                        for (var cb = 0; cb < data[this.JP_RESULT].length; cb++) {

                            for (let key in data[this.JP_RESULT][cb]) {
                                let value = data[this.JP_RESULT][cb][key];
                                // Use `key` and `value`
                                if (value) {
                                    result[key] = value;
                                    switch (cb) {
                                        case TCombo.COMBO:
                                            {
                                                this._modelsCb[key] = result[key];
                                            }
                                            break;

                                        case TCombo.DATASOURCE:
                                            {
                                                this._models[key] = result[key];
                                            }
                                            break;
                                    }
                                }
                            }
                        }

                        return data;
                    }
                },
                error => {

                    //this.loading = false;
                });


        // }

        //return result;
    }

    isOnline() {
      return this.connectivityService.isOnline();
    }
  
    selectPlace(geodecoder, place, placeSelected) : Promise<any> {
        return new Promise((resolve) => {  
            geodecoder.geocode({ 'placeId': placeSelected.place_id }, (results, status) => {
                if (status === 'OK' && results[0]) {
                    var locationTmp = results[0];
                    this.setLocationInfo(place, locationTmp);
                    resolve(place);
                }
            })
        })    
    }

    setLocationInfoFromNative(place, locationSelected, lat, long) {
        if(locationSelected) {
          place.zipcode = "";
          if(locationSelected.postalCode && locationSelected.postalCode.length>0) {
            place.zipcode = locationSelected.postalCode;
          }
          place.num = "";
          if(locationSelected.subThoroughfare && locationSelected.subThoroughfare.length>0)
          {
            place.num = locationSelected.subThoroughfare;
          }
          place.street_address = "";
          if(locationSelected.thoroughfare && locationSelected.thoroughfare.length>0)
          {
            place.street_address = locationSelected.thoroughfare;
          }
          place.locality = "";
          if(locationSelected.locality && locationSelected.locality.length>0)
          {
            place.locality = locationSelected.locality;
          }
          place.sublocality = "";
          if(locationSelected.sublocality && locationSelected.sublocality.length>0)
          {
            place.sublocality = locationSelected.subLocality;
          }
          place.regione = "";
          if(locationSelected.administrativeArea && locationSelected.administrativeArea.length>0)
          {
            place.regione = locationSelected.administrativeArea;
          }
          place.nazione = "";
          if(locationSelected.countryName && locationSelected.countryName.length>0)
          {
            place.nazione = locationSelected.countryName;
          }
             
          place.nomelocale = "";
          place.latitude = lat;
          place.longitude = long;
          place.nazione = "";
          place.admin_area_3 = ""; 
    
          place.description = "";
          if(locationSelected.thoroughfare && locationSelected.thoroughfare.length>0)
          {
            place.description = locationSelected.thoroughfare;
    
            if(locationSelected.subThoroughfare.length>0)
            {
              place.description = place.description+', '+place.num;
            }
            if(locationSelected.postalCode.length>0)
            {
              place.description = place.description+', '+place.zipcode;
            }
          }
          else if(locationSelected.formatted_address){
            place.description = locationSelected.formatted_address;

          }
    
          if(locationSelected.locality && locationSelected.locality.length>0)
          {
            if(place.description.length>0)
            {
              place.description = place.description + ', ';
            }
            place.description = place.description + locationSelected.locality;
          }
    
        }
      }
    
    setLocationInfo(place, locationSelected) {
        place.zipcode = "";
        place.num = "";
        place.nomelocale = "";
        place.latitude = 0;
        place.longitude = 0;
        place.street_address = "";
        place.locality = "";
        place.sublocality = "";
        place.regione = "";
        place.nazione = "";
        place.admin_area_3 = ""; 
        place.description = "";

        var tipiPlaces = ['bakery', 'bar', 'cafe',
        'establishment', 'food', 'liquor_store',
        'meal_delivery', 'meal_takeaway',
        'pharmacy', 'restaurant', 'point_of_interest', 'store'];

        // Optional
        var countryCode = 'IT';

        var result_type = locationSelected.types[0]; // "Street_address"
        if (result_type) {

            place.description = locationSelected.formatted_address;

            if (result_type == "administrative_area_level_3"
                || result_type == "locality") {
                // ho inserito il nome della città o del comune

                for (var i = 0; i < locationSelected.address_components.length; i++) {
                var addressType = locationSelected.address_components[i].types[0];
                if (this.componentForm[addressType]) {
                    var val = locationSelected.address_components[i][this.componentForm[addressType]];
                    if (addressType === "administrative_area_level_3"
                        || addressType === "locality") {
                        place.locality = val;
                    } else if (addressType === "administrative_area_level_2") {
                        place.sublocality = val;
                    } else if (addressType === "administrative_area_level_1") {
                        place.regione = val;
                    } else if (addressType === "country") {
                        place.nazione = val;
                    }
                }
                }// end ciclo address_components


            } else if ((result_type == "route")
                || (result_type == "street_address")
                || (result_type == "premise")
            ) {
                // Ho inserito il nome esatto della via

                for (var i = 0; i < locationSelected.address_components.length; i++) {
                    var addressType = locationSelected.address_components[i].types[0];
                    if (this.componentForm[addressType]) {
                        var val = locationSelected.address_components[i][this.componentForm[addressType]];
                        if (addressType === "street_number") {
                            place.num = val;

                        } else if (addressType === "postal_code") {
                            place.zipcode = val;
                        if ((locationSelected.geometry)
                            && (locationSelected.geometry.location)) {
                            place.latitude = locationSelected.geometry.location.lat();
                            place.longitude = locationSelected.geometry.location.lng();
                        }
                        } else if (addressType === "street_address"
                            || addressType === "route") {
                            place.street_address = val;
                        } else if (addressType === "locality") {
                            place.locality = val;
                        } else if (addressType === "sublocality"
                        || addressType === "administrative_area_level_2") {
                            place.sublocality = val;
                        } else if (addressType === "administrative_area_level_3") {
                            place.admin_area_3 = val;
                        }
                    }
                }

            } else {
                // potrei aver inserito il nome del
                // locale (da verificare se è¨
                // nell'elenco dei tipi riconosciuti)!
                //if ($scope.contains($scope.tipiPlaces, result_type)) {

                // ho riconosciuto il tipo di locale
                // come compatibile e ne estrapolo
                // tutte le informazioni

                if (result_type != 'point_of_interest') {
                    place.nomelocale = locationSelected.name;
                }

                var tmp = locationSelected.formatted_address;
                if (tmp != undefined) {
                    var infoindirizzo = tmp.split(',');
                if (infoindirizzo.length > 0) {
                    place.street_address = infoindirizzo[0];
                }
                }

                if (locationSelected.address_components != undefined) {
                for (var i = 0; i < locationSelected.address_components.length; i++) {
                    var addressType = locationSelected.address_components[i].types[0];
                    if (this.componentForm[addressType]) {
                        var val = locationSelected.address_components[i][this.componentForm[addressType]];
                        var refresh = false;

                        if (addressType === "street_number") {
                            place.num = val;
                        } else if ((addressType === "street_address")
                            || (addressType === "route")) {
                            place.street_address = val;
                        } else if (addressType === "locality") {
                            place.locality = val;
                        } else if (addressType === "administrative_area_level_3") {
                            place.admin_area_3 = val;
                        } else if (addressType === "sublocality"
                            || addressType === "administrative_area_level_2") {
                            place.sublocality = val;
                        } else if (addressType === "administrative_area_level_1") {
                            place.regione = val;
                        } else if (addressType === "country") {
                            place.nazione = val;
                        } else if (addressType === "postal_code") {
                            place.zipcode = val;
                            refresh = true;
                            if ((locationSelected.geometry)
                            && (locationSelected.geometry.location)) {
                            place.latitude = locationSelected.geometry.location
                                .lat();
                            place.longitude = locationSelected.geometry.location
                                .lng();
                            }
                        }
                    }

                }


                }/* else {
                // non ho inserito nulla di
                // decifrato...
                //$scope
                // .popupMsg("Inserire una città  o una via!");
                }*/
            }
        }

    }

    getAddressFromPlace(place: any) {

        let result = "";  

        if(place) {

            place.description = "";

            if(place.street_address && place.street_address.length > 0) {
                place.description = place.street_address;  
                if(place.num && place.num.length > 0) place.description += ', ' + place.num; 
                if(place.zipcode && place.zipcode.length > 0) place.description += ', ' + place.zipcode; 
            }
    
            if(place.locality && place.locality.length > 0) {
                if(place.description.length > 0) place.description += ', '; 
                place.description += place.locality;
            }
    
            result = place.description;
        }
    
        return result;
    }

    selectAddress(geodecoder: google.maps.Geocoder, place: any, addressSelected: any) {
        geodecoder.geocode({ 'address': addressSelected }, (results: Array<any>, status: any) => {
            if (status === 'OK' && results[0]) {        
                var locationTmp = results[0];
                this.setLocationInfo(place, locationTmp);
        
            }
        })
    }
    
    checkLocationCallDriver(geocoder: any, place: any) {

        return new Promise((resolve, reject) => {
    
          if (place) {
    
            place.address = place.address.replace(/'/g, ' '); //elimino tutti gli apici, sostituendoli con spazi...
            geocoder.geocode({ 'address': place.address }, (results, status) => {
              if (status === 'OK' && results[0]) {
    
                var locationTmp = results[0];
    
                if (locationTmp.address_components) {
                  for (var i = 0; i < locationTmp.address_components.length; i++) {
                    var addressType = locationTmp.address_components[i].types[0];
                    if (this.componentForm[addressType]) {
                      var val = locationTmp.address_components[i][this.componentForm[addressType]];
    
                      if (addressType === "postal_code") {
                        if ((place.zipcode.length && place.zipcode == val) || place.zipcode.length == 0) {
                          place.zipcode = val;
                          if ((locationTmp.geometry) && (locationTmp.geometry.location)) {
                            place.latitude = locationTmp.geometry.location.lat();
                            place.longitude = locationTmp.geometry.location.lng();
                          }
                        }
                      }
                    }
                  }
                }
              }
    
              if (place.num.length && place.zipcode.length) {
    
                if (place.locality === '') {
                  place.locality = place.admin_area_3;
                }
                resolve("");
              }
              else {
                reject("Inserire un indirizzo di consegna!");
              }
    
    
            })
    
          } else {
    
            reject("Inserire un indirizzo di consegna!");
          }
    
        });
      }
    
}

export enum TypeService {
    HOME = 0,
    TAKEAWAY = 1,
    TABLES = 2
}

export enum ET_OpzioniCoupon {
    NOTYPEOPTIONS = 0,
    ONLYCARDS = 1
}


  



export const environment = {
    production: false,
    firebase: {
      apiKey: "AIzaSyCou3YOVgLwQ85XMCTw7JRO1vzJNag1WNY",
      authDomain: "driveit-154811.firebaseapp.com",
      databaseURL: "https://driveit-154811.firebaseio.com",
      projectId: "driveit-154811",
      storageBucket: "driveit-154811.appspot.com",
      messagingSenderId: "556866775414",
      appId: "1:556866775414:web:53218b1b159abcd2"
    },
    googlekey:'AIzaSyDDxO0bCfZmlU3llHOuuXSs0uxavuRBx6Q'
   };

export enum Role {
    Guest = 'guest',
    User = 'user',
    Admin = 'admin',
    HelpDesk = 'support',
    Developer = 'developer',

    /*const GUEST = 'guest';
      const USER = 'user';
      const PARTNER = 'company-user';
      const PARTNER_COMPANY = 'company';
      const PARTNER_MENU = 'company-menu';
      const CO_USER = 'co-user';
      const CO_ADMIN = 'co-admin';
      const HELPDESK = 'helpdesk';
      const ADMIN = 'admin';
      const DEVELOPER = 'developer';
      const DRIVER = 'driver';
      const PROMOTER = 'promoter';
      const SUPPORT = 'support';
      const SUPPORT_DRIVER = 'support-driver';
      const SUPPORT_INVOICES = 'support-invoices';
      const SUPPORT_ORDERS = 'support-orders';
      const SUPPORT_PROMO = 'support-promo';
      const SUPPORT_REPORT = 'support-report';
      const SUPPORT_RESTAURANT = 'support-restaurant';
      const SUPPORT_SUPERRESTAURANT = 'support-superrestaurant';
      const SUPPORT_USERS = 'support-users';
      const SUPPORT_SUPPORT = 'support-support';
      const SUPPORT_ONLINE = 'support-online';*/
}
export enum  JParamFilter
{
	ID = 'ID',
    ID_MASTER = 'id_master',
}
    
export enum TCombo {
    COMBO = 0,
    DATASOURCE = 1
}

export enum TModule {
    EatInTime = 1
}

export enum ET_InvoiceItemType {
    UNDEF = -1,
    ETT_TAX = 0,
    MANAGEMENT = 1,
    INVOICE = 2,
    PAYMENT = 3,
    PAYMENT_VAT_INCLUDED = 4,
    REFUND = 5,
    SUL_CONTO = 6,
    IIT_VISUALIZZAZIONE = 7,
    ETT_TAX_BOOKS = 8,
    GIROCONTO = 9,
    ETT_TAX_MENUBOOKS = 10
 }

 export enum ET_ProductOp {
    ADD = 0,
    REMOVE = 1
 }

export enum TEit {
    GENDER = 1,
    CITY = 2,
    INGREDIENT = 3,
    DELIVERY_TIME = 4,
    CUCINA = 5,
    RESTAURANT_STATUS = 6,
    ORDER_STATUS = 7,
    USER_STATUS = 8,
    PAY_METHODS = 9,
    ZIPCODE = 10,
    TOWN = 11,
    MENUITEM_TYPE = 12,
    CUSTOM_TYPE = 13,
    DAY_WEEK = 14,
    INGREDIENTS_CUSTOM = 15,
    COUPON_TYPE = 16,
    COUPON_STATUS = 17,
    COUPON_TYPE_CALC = 18,
    INVOICE_STATUS = 19,
    ACCOUNT_STATUS = 20,
    CUSTOMSDB = 21,
    CATEGORYMENU = 22,
    RESTAURANTS = 23,
    FEATURED_PROMO = 24,
    REFUNDS_PROMO = 25,
    CUSTOMDETAILSDB = 26,
    MENUITEMREST = 27,
    TYPE_OPERATIONS = 28,
    DELIVERY_TIME_MINUTES = 29,
    COUPON_TYPE_OPTIONS = 30,
    RESTAURANT_OPTIONS = 31,
    INVOICE_TYPEOPERATIONS = 32,
    INVOICE_OPERATIONS = 33,
    RIDE_STATUS = 34,
    CALL_STATUS = 35,
    ORDERS = 36,
    ORDERS_BY_REST = 37,
    USERS = 38,
    DRIVER_STATUS = 39,
    TIME_HOUR = 40,
    TIME_MINUTES = 41,
    ACCOUNT_DRIVER_STATUS = 42,
    STATUS_EVENT = 43,
    SHIFT_TYPE = 44,
    DELIVERY_TIME_SLOTS = 45,
    PAYMENTS_TYPE = 46,
    ORDER_TYPE = 47,
    PARTNERS = 48,
    REST_SERVICE_TYPE = 49,
    PROMO_VALIDITY = 50,
    USER_TYPE = 51,
    FRANCHISEE_OP = 52,
    FRANCHISEE_LIST = 53,
    TIPO_MEZZO = 54,
    ZIPCODE_BY_CITY = 58,
    RESTAURANTS_BY_CITY = 59,
    YEARS_DESC = 60,
    RIDES_AVAILABLE = 61,
    ACTIVECITY = 63,
    SLAVE_STATUS = 64,
    PARTNERS_BY_CITY = 69,
    RESTAURANTS_BY_FRANCHISEE = 70,
    //71...75
    TABLESSERVICE_STATUS = 76,
    RESTAURANTS_BY_SUPPORT = 79,
    CUSTOM_TYPE_V2 = 80,
    //81...85
    STATUS_BONIFICO= 86,
    TIPOVERSO_BONIFICO = 87,
    TIPOINTESTATARIO_BONIFICO = 88,
    INVOICES_LIST = 89,
    //90
    FAST_MESSAGES = 91,
    //92...93
    TIPOSOGGETTO = 94,
    //95..97
    TICKET_STATUS = 98,
    TICKET_TYPE = 99,
    DOCUMENT_TYPE = 100,
    TIPO_CONTRATTO = 101,
    MENUTEMPLATE_TYPE = 102,
    DRIVERINVOICES_LIST = 103,
    SELL_CATEGORY = 104,
    SELL_PRODUCT = 105,
    SELLER = 106,
    PACK = 107,
    UM = 108,
    USER_ROLES = 109,
    BUYER = 110,
	BUY_LIST_DETAIL = 111,
    BUY_LIST_DETAIL_CODE = 112,
    BUY_PRODUCT = 113,
    BUY_CATEGORY = 114,
    BUY_SELLER_DETAIL = 115,
    ORDER_DETAIL_STATUS = 116,
    RECORD_STATUS = 117,	
    ORDER_DELIVERY_SLOT = 118,
    RESTS_BY_IDPARTNER = 119
}

export enum TEsoft {
    GENDER = 1,
    CITY = 2,
    INGREDIENT = 3,
    DELIVERY_TIME = 4,
    CUCINA = 5,
    RESTAURANT_STATUS = 6,
    ORDER_STATUS = 7,
    USER_STATUS = 8,
    PAY_METHODS = 9,
    ZIPCODE = 10,
    TOWN = 11,
    MENUITEM_TYPE = 12,
    CUSTOM_TYPE = 13,
    DAY_WEEK = 14,
    INGREDIENTS_CUSTOM = 15,
    COUPON_TYPE = 16,
    COUPON_STATUS = 17,
    COUPON_TYPE_CALC = 18,
    INVOICE_STATUS = 19,
    ACCOUNT_STATUS = 20,
    CUSTOMSDB = 21,
    CATEGORYMENU = 22,
    RESTAURANTS = 23,
    FEATURED_PROMO = 24,
    REFUNDS_PROMO = 25,
    CUSTOMDETAILSDB = 26,
    MENUITEMREST = 27,
    TYPE_OPERATIONS = 28,
    DELIVERY_TIME_MINUTES = 29,
    COUPON_TYPE_OPTIONS = 30,
    RESTAURANT_OPTIONS = 31,
    INVOICE_TYPEOPERATIONS = 32,
    INVOICE_OPERATIONS = 33,
    RIDE_STATUS = 34,
    CALL_STATUS = 35,
    ORDERS = 36,
    ORDERS_BY_REST = 37,
    USERS = 38,
    DRIVER_STATUS = 39,
    TIME_HOUR = 40,
    TIME_MINUTES = 41,
    ACCOUNT_DRIVER_STATUS = 42,
    STATUS_EVENT = 43,
    SHIFT_TYPE = 44,
    DELIVERY_TIME_SLOTS = 45,
    PAYMENTS_TYPE = 46,
    ORDER_TYPE = 47,
    PARTNERS = 48,
    REST_SERVICE_TYPE = 49,
    PROMO_VALIDITY = 50,
    USER_TYPE = 51,
    FRANCHISEE_OP = 52,
    FRANCHISEE_LIST = 53,
    TIPO_MEZZO = 54,
    ZIPCODE_BY_CITY = 58,
    RESTAURANTS_BY_CITY = 59,
    YEARS_DESC = 60,
    RIDES_AVAILABLE = 61,
    ACTIVECITY = 63,
    SLAVE_STATUS = 64,
    PARTNERS_BY_CITY = 69,
    RESTAURANTS_BY_FRANCHISEE = 70,
    //71...75
    TABLESSERVICE_STATUS = 76,
    RESTAURANTS_BY_SUPPORT = 79,
    CUSTOM_TYPE_V2 = 80,
    //81...85
    PROMO_TYPE_CALC = 84,
    STATUS_BONIFICO = 86,
    TIPOVERSO_BONIFICO = 87,
    TIPOINTESTATARIO_BONIFICO = 88,
    INVOICES_LIST = 89,
    //90
    FAST_MESSAGES = 91,
    //92...93
    TIPOSOGGETTO = 94,
    //95..97
    TICKET_STATUS = 98,
    TICKET_TYPE = 99,
    DOCUMENT_TYPE = 100,
    TIPO_CONTRATTO = 101,
    MENUTEMPLATE_TYPE = 102,
    DRIVERINVOICES_LIST = 103,
    RESTAURANTS_BY_PARTNER = 107,
      EVENTS_TYPE = 108,
    MENUCATEGORY_BYREST = 109,
    MENUITEM_BYREST = 110,
    CUSTOMSDB_BYREST = 111
  }
  
export enum RideStatus {
	Created = 0,
	Sent = 1,
	Accepted = 2,
	Collecting = 3,
	Collected = 4,
	Delivered = 5,
    Interrupted = 6,
	Closed = 7
}

export enum RecordStatus
{
	UNDEF = -1,
	ACTIVE = 0,
	DELETED = 1,
	BLOCKED = 2,
}

export enum  UM
{
	PZ = 0,
	KG = 1,
	L = 2,
}


export enum EtInvoiceTypeOperations {
    NoOptions = -1,
	COMMISSIONI = 1,
	AMMINISTRAZIONE = 2,
	PAGAMENTO_IVA_INCLUSA = 3,
	PAGAMENTO_IVA_ESCLUSA = 4,
	RIMBORSO = 5,
	PAGAMENTO_SPESE_CONSEGNA = 6,
	COMMISSIONI_TAVOLI = 7,
	GIROCONTO_CONSEGNE_ORDINI_GIA_PAGATI_CONSEGNA_INCLUSA = 8,	
	MARCABOLLO = 9,
	PAGAMENTIGENERICI = 10,
	COMMISSIONI_TAVOLI_ORDINI = 11
}

export enum UserType {
	User = 0,
	Admin = 1,
	Sales_agent = 2,
}

export enum ProfileTabs {
	INFO = 0,
	LOCATION = 1,
	BUSINESS = 2,
	BILLINGINFO = 3,
	REPORT = 4,
	BUSINESS_USER = 5,
	REPORT_AGENT = 6,
}

export enum EtTipoStampaDriver
{
	TConsegneCompleto = 0,
	TConsegneDigitali = 1,
	TPrestOccasionale = 2,
	TConsegneCompletoDinamico = 3,
	TPrestOccasionaleDinamico = 4
}

export enum OrderbyMedia {
	ID_ASC = 0,//ID crescente
	ID_DESC = 1,//ID Decrescente
	NAME_ASC = 2,//Nome crescente
	NAME_DESC = 3,//Nome Decrescente
}

export enum OrderStatus {
    Undef = -1,
    Created = 0,//Bozza
    Accepted = 1,//Accettato
	Send = 2,//Inviato
	Error = 3,//Errato
	Rejected = 3,//Rifiutato
	Refund = 5,//Rimborare
    Standby = 6,//In Attesa	
    Collecting = 7,//Preso in carico
	Collected = 8,//In Consegna
	Delivered = 9,//Consegnato
	Interrupted = 10,//Interrotto	
}

export enum OrderDetailStatus {
    Undef = -1,
    Created = 0,//Bozza
    Send = 1, //Inviato
    Accepted = 2,//Accettato
    Collecting = 3,//Preso in carico
    Collected = 4,//In consegna
	Consegnato = 5,//Consegnato
	Interrotto = 6,//Interrotto
}



export enum  ListDetailType {
	REF = 0,
	CAT = 1
}

export enum SellerTabs {
	INFO = 0,
	LOCATION = 1,
    BILLINGINFO = 2,
    BILLINGLOCATION = 3,
    LISTSELLER = 4,
    AREECONSEGNA= 5,
    FASCEORARIE = 6
}

export enum EtFiltroSeller
{
    FS_Tutti = 0,
    //FS_SoloConAreeConsegna = 1
}

export enum TypeOrder {
    EIT_DOMICILIO = 0,
    REST_DOMICILIO = 1,
    EIT_ASPORTO = 2,
    REST_ASPORTO = 3,
}

export enum UtilityType {
    //COMBO = 0,
    OFFLINE = 1,
    ADDRESS = 2,
    MINORDER = 3,
}

export enum EtTipoCustom {
    CHECKLIST = 1,
    MENU = 2,
    RADIOLIST = 3
}

export enum ET_TipoOraConsegna {
	ORARIO = 0,
	FASCIAORARIA = 1
}


export enum TypePayment
{
	MONEY = 1,
	PAYPAL = 2,
	CARD = 3,
	TICKET = 4,
	EATPOINTS = 5,
	CREDIT = 6,
  SATISPAY=7,
  INLIRE=8

	/* ID Table */
}

/*
export enum PaypalSettings {
    keyRel = "Ad27XU85tEhawHSm1wEM1KV3ybTYXz34t4kpr8YS5fLf1baoDiGGMxp3q3VVBWAgio8pHhBO3vuvDyXY",
    keySand = "AV5dnyh3y5DeM-5xvZUPycFxewXT4LYHpajvPgQVk0N2qF0U_J8IRGAq6rDdPCMzVSoYAZz4BwveLMKZ",
}  */

export enum StatusOrder {
    UNDEF = -1,
    CREATED = 0,
    PAID = 1,
    CLOSED = 2,
    ERROR = 3,
    ACCEPTED = 4,
    REJECTED = 5,
    NO_RESPONSE = 6,
    ACCEPTED_NOTIFIED = 7,
    REJECTED_NOTIFIED = 8,
    NO_RESPONSE_NOTIFIED = 9,
    REFUND = 10,
    PREORDERED = 11,
    PREACCEPTED = 12,
    STANDBY = 13,	
}
  
export enum StatusBook {
    CREATED = 0,
    CONFIRMED = 1,
    CANCELLED_BY_USER = 2,
    CANCELLED_BY_RESTAURANT = 3,
    MISSED_PRESENTATION = 4,
    ARRIVED = 5,
    BILL = 6,
    SETTLED = 7,
    FINISHED = 8
}