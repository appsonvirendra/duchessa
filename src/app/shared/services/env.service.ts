import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})




export class EnvService {

  constructor() { }
}

export enum EitUrl {
  apiUrl = "http://api5621.esoft.it/v3/apimobile/web/v1",
  urlCardOk = "https://api56.esoft.it/eatintime/cardpayok",
  urlCardKo = "https://api56.esoft.it/eatintime/cardpayko",
  urlCardSync =  "https://api5621.esoft.it/v3/apimobile/web/v1/order/cardsync",
  urlCardSyncEdenred =  "https://api5621.esoft.it/v3/apimobile/web/v1/order/ecardsync",
}

export enum Satispay {
  urlRel = "https://authservices.satispay.com",
  urlSand = "https://staging.authservices.satispay.com",
  urlReturn = "http://localhost/satispayreturn",
  urlCallback = "https://api5621.esoft.it/v3/apimobile/web/v1/order/satispay?charge_id={uuid}",
  urlCallbackEdenred = "https://api5621.esoft.it/v3/apimobile/web/v1/order/esatispay?charge_id={uuid}",
  keyRel = "osh_cu673pcv7pvpmt351j564feo7qco2044577grpfadm4p1gigctaa2iu0di47s8inghjphsefvfg0lcdom4e22v1siaiqbgm2k052uoo18e6f88hn12ia4vdodqhtek8uhtqbvsdrtp3c6oqvvmc3e2j72hgvku74h1m0kdoe5olalio7qg5ivgeng77gd6p2b3bn1tt7"
}

export let AppInfo : any = {
  APP_ID: 23,
  APP_NAME: "Viotti",
  LATITUDE: 44.8977953,
  LONGITUDE: 8.2028974,
  ASSETS_URL: "http://assets.esoft.it/viotti/",
  LINK_TERMS: "https://www.eatintime.it/terms",
  LINK_PRIVACY: "https://www.eatintime.it/privacy",
};

export enum DB {
  LOCATION = "location",
  USER = "currentuser",
  SETTINGS = "settings",
  TOKEN = "token",
  ORDER_ID = "orderid",
  ACTIVITY_BASE = "activity_base",
  CART = "cart",
  CARTBOOK = "cartbook",
  BOOK = "book",
  RESTAURANT = "restaurant",
  RESTAURANT_FULL = "restaurantFull",
  USERNAME = "user",
  PW = "pw",
}

export enum RESPONSE {
  JP_STATUS = "status",
  JP_RESULT = "result",
  JP_RURL = "url",
  JP_MSG = "msg",
  JP_HTML = "html",
  JPS_OK = "ok",
  JPS_ERROR = "error",
  JPS_REDIRECT = "redirect",
  JPS_VIEW = "view",
}