import { IService } from './IService';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { AuthService } from './auth.service';
import { AppInfo } from './env.service';
import { ApiService } from './api.service';
import { ConfigApp } from './config-app.service';

@Injectable({ providedIn: 'root' })
export class MenuService implements IService {

  private menuSubject: BehaviorSubject<Array<any>>;
  public menu: Observable<Array<any>>;

  app: any = {}

  profileRoute: any = { url: 'profile', title: 'Profilo', theme: 'listViews', icon: 'person-outline', listView: true, component: '', singlePage: false }
  pOrdersRoute: any = { url: 'profile-orders', title: 'I tuoi ordini', theme: 'listViews', icon: 'reader-outline', listView: true, component: '', singlePage: false }
  pBookingsRoute: any = { url: 'profile-bookings', title: 'Prenotazioni', theme: 'listViews', icon: 'reader-outline', listView: true, component: '', singlePage: false }
  loginRoute: any = { url: 'login', title: 'Accedi', theme: 'listViews', icon: 'person-outline', listView: true, component: '', singlePage: false }
  fidelityRoute: any = { url: 'fidelity', title: 'Carta fedeltà', theme: 'listViews', icon: 'person-outline', listView: true, component: '', singlePage: false }
  logoutRoute: any = { url: 'logout', title: 'Logout', theme: 'listViews', icon: 'person-outline', listView: true, component: '', singlePage: false }
  inforestRoute: any = { url: 'inforestlist', title: 'I nostri ristoranti', theme: 'listViews', icon: 'person-outline', listView: true, component: '', singlePage: false }
  infoonerestRoute: any = { url: 'inforestaurant', title: 'Il nostro ristorante', theme: 'listViews', icon: 'person-outline', listView: true, component: '', singlePage: false }

  constructor(private api: ApiService, public config:ConfigApp, private authService: AuthService) {
    this.menuSubject = new BehaviorSubject<Array<any>>([]);
    this.menu = this.menuSubject.asObservable();
  }

  getId = (): string => 'menu';

  getTitle = (): string => AppInfo.APP_NAME;

  //* Data Set for main menu
  getAllThemes = (): Array<any> => {
    console.log('all themes');
    let menu: Array<any> = [];
    let isEit = this.authService.isEITEnabled();
    if (isEit) {
      menu = [this.profileRoute, this.pOrdersRoute]
      if(this.app.count_tavoli > 0) menu.push(this.pBookingsRoute)
      if(this.app.fidelitycard) menu.push(this.fidelityRoute)
      if(this.app.raccoltapunti) menu.push(this.fidelityRoute)
      if(this.app.count_restaurants > 1) menu.push(this.inforestRoute)
      else menu.push(this.infoonerestRoute)
      menu.push(this.logoutRoute);
    } else {
      menu = [this.loginRoute] 
      if(this.app.count_restaurants > 0) menu.push(this.inforestRoute)
      else menu.push(this.infoonerestRoute)
    }
    this.menuSubject.next(menu);
    return menu;
  } 

  async initMenu(){
    await this.getAppInfo();
    this.getAllThemes();
  }

  async getAppInfo () {    
    return this.api.postj("apps/getinfoapp", "", "").then(res => {
      console.log(res);
      if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {
        this.app = res[this.config.JP_RESULT]
      } 
    })
  }

  getDataForTheme = (menuItem: any) => {
    return {
      'background': '',
      'image': 'assets/imgs/logo.png',
      'title': ''
    };
  }

  getEventsForTheme = (menuItem: any): any => {
    return {};
  }

  prepareParams = (item: any) => {
    return {
      title: item.title,
      data: {},
      events: this.getEventsForTheme(item)
    };
  }

  load(item: any): Observable<any> {
    return new Observable;
  }
}
