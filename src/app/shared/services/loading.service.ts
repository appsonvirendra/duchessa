import { LoadingController } from '@ionic/angular';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class LoadingService {

  loading: any;
  isLoading = false;

  constructor(private loadingCtrl: LoadingController) { 
    this.init()
  }

  private async init(){
    this.loading = null;
  }
  
  async show() {   
    this.isLoading = true;
    /*if(this.loading)
    {
      this.loading.dismiss();
      this.loading = null;
    }
    */
   return /*this.loading = */await this.loadingCtrl.create({
      message: "Caricamento...",
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
    //return await this.loading.present();
  }

  async show_M(msg) {   

    if(!msg || !msg.length)
    {
      msg = "Please wait..."
    }
    if(this.loading)
    {
      this.loading.dismiss();
      this.loading = null;
    }
    
    this.loading = await this.loadingCtrl.create({
      message: msg,
    });
    await this.loading.present();
  }

 /* async hide() {
    if(this.loading)
    {
     // this.loading = null;
      return await this.loading.dismiss();
    }
  }*/
  async hide() {
    this.isLoading = false;
    return await this.loadingCtrl.dismiss().then(() => console.log('dismissed'));
  }
}
