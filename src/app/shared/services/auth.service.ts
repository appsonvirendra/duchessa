import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { User } from '../classes/user';
import { DB } from '../services/env.service';
import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  private id_orderSubject: BehaviorSubject<any>;
  public id_order: Observable<any>;
  httpData: {};

  constructor(private platform: Platform, private nativeStorage: NativeStorage) {
    this.currentUserSubject = new BehaviorSubject<User>(null);
    this.currentUser = this.currentUserSubject.asObservable();
    
    this.id_orderSubject = new BehaviorSubject<any>(null);
    this.id_order = this.id_orderSubject.asObservable();
  }
  
  /*load()
  {
    this.nativeStorage.getItem(DB.USER).then(data => {
      this.currentUserSubject.next(JSON.parse(data));
    });
  }*/

  public get currentUserValue(): User {
    var result = this.currentUserSubject.value;
    return result;
  }

  public next(value) {
    this.currentUserSubject.next(value);
    this.nativeStorage.setItem(DB.USER, JSON.stringify(value))
      .then(
        () => {
          console.debug('Ok user  login store')
        },
        error => console.error('Error login store', error)
      );
  }

  public get idOrder(): any {

    return this.id_orderSubject.value;
  }

  public nextOrder(value) {
    localStorage.setItem(DB.ORDER_ID, JSON.stringify(value));
    this.id_orderSubject.next(value);
  }


  logout() {
    // remove user from local storage to log user out
    this.nativeStorage.remove(DB.USER);
    this.currentUserSubject.next(null);
    localStorage.removeItem(DB.ORDER_ID)
    this.id_orderSubject.next(null);
  }

  isEITEnabled() {
    var result = false;
    let currentUser = this.currentUserValue;
    if (currentUser) {
      result = true;
    }
    return result;
  }
}
