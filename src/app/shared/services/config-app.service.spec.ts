import { TestBed } from '@angular/core/testing';

import { ConfigApp } from './config-app.service';

describe('ConfigAppService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConfigApp = TestBed.get(ConfigApp);
    expect(service).toBeTruthy();
  });
});
