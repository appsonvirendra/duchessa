import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { EitUrl, DB, AppInfo } from './env.service';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { User } from '../classes/user';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  token: any = null;
  

  constructor(private http: HTTP, private nativeStorage: NativeStorage, private auth: AuthService) {

    this.token = null;

     this.auth.currentUser.subscribe(state => {
       this.token = null;
       if (state) {
         this.token = state.access_token
       }
     });

   }

  post(endpoint: string, body: any, reqOpts?: any) {

    var url = EitUrl.apiUrl + '/' + endpoint;
    var headers = { 'Content-Type': 'application/json', 'Authorization': '' };
    var b = '';

    if (this.token) {
      var bearer: string = 'Bearer ' + this.token;
      headers.Authorization = bearer;
      b = 'access-token=' + this.token;
      url = url + '?'+b;
    }
    if(body === ""){
      body = {};
    }
    body.idApp = AppInfo.APP_ID;

    this.http.setDataSerializer("urlencoded");

    return new Promise((resolve, reject) => {
      this.http.post(url, body, headers)
        .then(res => {
          if (res.data) {
            resolve(JSON.parse(res.data));
          }
          else {
            resolve(res);
          }

        }, (err) => {

          reject(err);
          console.log(err);

          ;
        });
    });
  }

  postj(endpoint: string, body: any, reqOpts?: any) {
    var url = EitUrl.apiUrl + '/' + endpoint;
    var headers = { 'Content-Type': 'application/json', 'Authorization': '' };
    var b = '';

    if (this.token) {
      var bearer: string = 'Bearer ' + this.token;
      headers.Authorization = bearer;
      b = 'access-token=' + this.token;
      url = url + '?'+b;
    }
    if(body === "" || !body){
      body = {};
    }
    body.idApp = AppInfo.APP_ID;

    this.http.setDataSerializer("json");

    return new Promise((resolve, reject) => {
      this.http.post(url, body, headers)
        .then(res => {
          if (res.data) {
            resolve(JSON.parse(res.data));
          }
          else {
            resolve(res);
          }

        }, (err) => {

          reject(err);
          console.log(err);

          ;
        });
    });
  }

  login(username: string, password: string) {
    var httpData = { 'email': username, 'password': password };

    return new Promise((resolve, reject) => {
      this.postj('user/security/login', httpData).then(data => {

        resolve(data);
      }, (err) => {

        reject(err);
        console.log(err);

        ;
      });
    });
  }

  register(credentials: any) {
    console.log(credentials);

    return new Promise((resolve, reject) => {
      this.postj('user/registration/register', credentials).then(data => {

        resolve(data);
      }, (err) => {

        reject('Controlla i dati inseriti!');
        console.log(err);

        ;
      });
    });
  }

}
