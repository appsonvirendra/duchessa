import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { AppInfo } from '../../shared/services/env.service';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  private btns: Array<any> = [{text: 'OK', role: 'cancel', handler: () => {console.log('OK clicked');}}];

  constructor(private alertCtrl: AlertController) { }

  async showAlert(subHeader: string, message: string, btns?: Array<any>) {
    let alert = await this.alertCtrl.create({
      header: AppInfo.APP_NAME,
      subHeader: subHeader,
      message: message,
      buttons: btns ? btns : this.btns
    });
    //await alert.present();
    console.log("present")
    alert.present();
    console.log("presented")
  }

}
