import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Restaurant } from '../classes/restaurant';
import { ApiService } from './api.service';
import { ConfigApp } from './config-app.service';
import { LoadingService } from './loading.service';
import { RESPONSE, DB } from '../services/env.service';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  private cartSubject: BehaviorSubject<Restaurant[]>;
  public cartObs: Observable<Restaurant[]>;


  constructor(private api: ApiService, private auth: AuthService, private config: ConfigApp, private loadingService: LoadingService) {

    this.cartSubject = new BehaviorSubject<any>(null);
    this.cartObs = this.cartSubject.asObservable();
  }

  public get cart(): any {
    return this.cartSubject.value;
  }

  public next(value: any) {
    localStorage.setItem(DB.CART, JSON.stringify(value));
    this.cartSubject.next(value);
  }

  reset() {
    this.cartSubject.next(null);
  }

  createCart(id_rest, id_order, location, service, is_preorder) {
    var data: any = {};
    data.rest_id = id_rest;
    data.order_id = id_order;
    data.service = service;
    data.location = location;
    data.ispreorder = is_preorder;

    this.api.postj("restaurant/cart", data, "").then(
      res => {
        if (
          res[RESPONSE.JP_STATUS] == RESPONSE.JPS_OK &&
          res[RESPONSE.JP_RESULT]
        ) {
          this.next(res[RESPONSE.JP_RESULT]);
          var current = this.cart;
          this.auth.nextOrder(current.order_id)
        } else if (
          res[RESPONSE.JP_STATUS] == RESPONSE.JPS_ERROR &&
          res[RESPONSE.JP_MSG]
        ) {
        } else {
          console.log(res);
        }
      },
      err => {
        console.log(err);
      }
    );
  }


  loadCart(order_id, silent?) {
    if(!silent)
    {
      this.loadingService.show();
    }
    var data: any = { rest_id: 0, order_id: order_id };
    return this.api.postj("cart/updatecart", data).then(data => {
      this.loadingService.hide();
      if (data[this.config.JP_STATUS] == this.config.JPS_OK && data[this.config.JP_RESULT]) {
        this.cartSubject.next(data[this.config.JP_RESULT]);
      }
      
    }, (err) => {
      console.log(err);
      this.loadingService.hide();
    })
  }

  postCart(cart) {
    var data: any = cart;

    this.api.postj("cart/postcart", data, "").then(res => {
      if (
        res[RESPONSE.JP_STATUS] == RESPONSE.JPS_OK &&
        res[RESPONSE.JP_RESULT]
      ) {
        this.next(cart);

      } else if (
        res[RESPONSE.JP_STATUS] == RESPONSE.JPS_ERROR &&
        res[RESPONSE.JP_MSG]
      ) {

      } else {
        console.log(res);
      }
    },
      err => {
        console.log(err);
      }
    );
  }

  postLocation(location, cart) {
    var data: any = {};
    data.location = location;
    data.order_id = cart.order_id;

    cart.order.location.address = location.street_address;
    cart.order.location.num = location.num;
    cart.order.location.zip = location.zipcode;
    cart.order.location.townstr = location.locality;
    cart.order.location.latitudine = location.latitude;
    cart.order.location.longitudine = location.longitude;
    this.next(cart);


    this.api.postj("cart/updatelocation", data, "").then(res => {
      if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {
        console.log(res);
      } else if (
        res[this.config.JP_STATUS] == this.config.JPS_ERROR &&
        res[this.config.JP_MSG]
      ) {
      } else {
        console.log(res);
      }
    },
      err => {
        console.log(err);
      }
    );
  }

  postOrario(cart, orarioscelto, giornoconsegna) {

    let data: any = { orarioscelto: orarioscelto, order_id: cart.order_id, orderday: giornoconsegna }
    this.api.postj("cart/updateorario", data, "").then(res => {
      if (res[this.config.JP_STATUS] == this.config.JPS_OK && res[this.config.JP_RESULT]) {
        let citofono = cart.order.location.citofono;
        let interno = cart.order.location.interno;
        let note = cart.order.note;
        var cartData = res[this.config.JP_RESULT];
        cartData.order.location.citofono = citofono;
        cartData.order.location.interno = interno;
        cartData.order.note = note;
        this.next(cartData);
      }
      else {
        console.log(res);
      }
    }, err => {
      console.log(err);
    });

  }

  addToCart(id_item, qty, notes, returndata, startdata) {
    console.log("add to cart")
    var current = this.cart;
    var dataCall: any = {};
    dataCall.orderid = current.order_id;
    dataCall.productid = id_item;
    dataCall.returndata = returndata;
    dataCall.qty = qty;
    dataCall.notes = "";
    if (notes) {
      dataCall.notes = notes;
    }
    if (startdata) {
      dataCall.result = startdata;
    }

    //this.loadingService.show();
    this.api.postj("cart/addmtoc", dataCall, "").then(
      res => {
        if (
          res[RESPONSE.JP_STATUS] == RESPONSE.JPS_OK &&
          res[RESPONSE.JP_RESULT]
        ) {
          this.next(res[RESPONSE.JP_RESULT]);

        } else if (
          res[RESPONSE.JP_STATUS] == RESPONSE.JPS_ERROR &&
          res[RESPONSE.JP_MSG]
        ) {
          //this.showAlert('Attenzione', res[this.config.JP_MSG]);
        } else {
          console.log(res);
          //this.showAlert('Attenzione', "Impossibile inviare la richiesta!");
        }
        //this.loadingService.hide();
      },
      err => {
        console.log(err);
        //this.loadingService.hide();
      }
    );
  }

  addToCartSingle(id_item, qty, notes, returndata) {
    var current = this.cart;
    var dataCall: any = {};
    dataCall.orderid = current.order_id;
    dataCall.productid = id_item;
    dataCall.returndata = returndata;
    dataCall.qty = qty;
    dataCall.notes = notes;
    if (notes) {
      dataCall.notes = notes;
    }

    //this.loadingService.show();
    this.api.postj("cart/addptoc", dataCall, "").then(
      res => {
        if (
          res[RESPONSE.JP_STATUS] == RESPONSE.JPS_OK &&
          res[RESPONSE.JP_RESULT]
        ) {
          this.next(res[RESPONSE.JP_RESULT]);

        } else if (
          res[RESPONSE.JP_STATUS] == RESPONSE.JPS_ERROR &&
          res[RESPONSE.JP_MSG]
        ) {
          //this.showAlert('Attenzione', res[this.config.JP_MSG]);
        } else {
          console.log(res);
          //this.showAlert('Attenzione', "Impossibile inviare la richiesta!");
        }
        //this.loadingService.hide();
      },
      err => {
        console.log(err);
        //this.loadingService.hide();
      }
    );
  }

  removeToCartP(id_item, qty, returndata) {
    var current = this.cart;
    var dataCall: any = {};
    dataCall.orderid = current.order_id;
    dataCall.productid = id_item;
    dataCall.returndata = returndata;
    dataCall.qty = qty;
    //this.loadingService.show();
    this.api.postj("cart/removemtoc", dataCall, "").then(
      res => {
        if (
          res[RESPONSE.JP_STATUS] == RESPONSE.JPS_OK &&
          res[RESPONSE.JP_RESULT]
        ) {
          this.next(res[RESPONSE.JP_RESULT]);

        } else if (
          res[RESPONSE.JP_STATUS] == RESPONSE.JPS_ERROR &&
          res[RESPONSE.JP_MSG]
        ) {
          //this.showAlert('Attenzione', res[this.config.JP_MSG]);
        } else {
          console.log(res);
          //this.showAlert('Attenzione', "Impossibile inviare la richiesta!");
        }
        // this.loadingService.hide();
      },
      err => {
        console.log(err);
        //this.loadingService.hide();
      }
    );

  }

  closeOrder() {
    this.auth.nextOrder(0);
    this.cartSubject.next(null);
    this.reset();
  }
}
