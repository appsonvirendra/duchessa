//import { Injectable } from '@angular/core';

/*
  Generated class for the GoogleMapsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
//@Injectable()
export class EitPlace {

  zipcode:any = "";
  num:any = "";
  nomelocale:any = "";
  latitude:any = 0;
  longitude:any = 0;
  street_address:any = "";
  locality:any = "";
  sublocality:any = "";
  regione:any = "";
  nazione:any = "";
  admin_area_3:any = "";
  description:any = "";

  importo:any='';
  citofono:any ='';
  interno:any ='';
  telefono:any ='';
  noteride:any ='';
  address:any = '';


  constructor() {
    this.initData();
  }

  initData() {
    this.zipcode = "";
    this.num = "";
    this.nomelocale = "";
    this.latitude = 0;
    this.longitude = 0;
    this.street_address = "";
    this.locality = "";
    this.sublocality = "";
    this.regione = "";
    this.nazione = "";
    this.admin_area_3 = "";
    this.importo='';
    this.citofono ='';
    this.interno ='';
    this.telefono ='';
    this.noteride='';
    this.address='';
    this.description='';
  }

}