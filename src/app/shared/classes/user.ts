export class User {
    id: number;
    user_id: number;
    username: string;
    email:string;
    password: string;
    firstName: string;
    lastName: string;
    roles: string[];
    access_token: string;
    token?: string;
    additionalservices: number;
    serviziotavoli: number;
    status: number;
    idRest: number;
}
