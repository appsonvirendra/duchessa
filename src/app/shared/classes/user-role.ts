export enum UserRole {
    User = 'user',
    Admin = 'admin',
    Partner = 'company',
    PartnerUser = 'company-user',
    PartnerMenu = 'company-menu',
    HelpDesk = 'support',
    Developer = 'developer',
    PartnerCM0 = 'user',
    Driver = 'driver',
}
