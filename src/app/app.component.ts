import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { MenuService } from './shared/services/menu-service';
import { NavController } from '@ionic/angular';
import { AuthService } from './shared/services/auth.service';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
//import { ApiService } from './shared/services/api.service';
import { FCM } from "cordova-plugin-fcm-with-dependecy-updated/ionic/ngx";
import { DB } from './shared/services/env.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
  providers: [MenuService]
})
export class AppComponent implements OnInit {
  public appPages = [];
  headerMenuItem = {
    'image': '',
    'title': '',
    'background': '',
  }
  isEnabledRTL: boolean = false;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authenticationService: AuthService,
    private nativeStorage: NativeStorage,
    /* private api: ApiService,*/
    private menuService: MenuService,
    private navController: NavController
  ) {
    this.isEnabledRTL = localStorage.getItem('isEnabledRTL') == "true";
    this.initializeApp();
    //this.appPages = this.menuService.getAllThemes()
    this.headerMenuItem = this.menuService.getDataForTheme(null)
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.overlaysWebView(false);
      this.statusBar.backgroundColorByHexString('#000000');

      this.menuService.menu.subscribe(x =>{
        this.appPages = x;
      })
      this.menuService.initMenu();
      //this.menuService.getAllThemes();
      //this.splashScreen.hide();
      this.setRTL();

      /*EIT */
      var id_order = JSON.parse(localStorage.getItem(DB.ORDER_ID));
      if (id_order) {
        this.authenticationService.nextOrder(id_order);
      }

    this.nativeStorage.getItem(DB.USER).then(data => {
      if (data) {
        this.authenticationService.next(JSON.parse(data));
      }
    });

    /*END EIT */

      this.authenticationService.currentUser.subscribe(state => {
        if (state) {
         // let cUser = this.authenticationService.currentUserValue;
      this.menuService.initMenu();
      //this.menuService.getAllThemes();
         // this.router.navigate(['/home']);
        } else {
          //this.router.navigate(['/login']);
        }
      });
    });
  }

  ngOnInit() {
    /*const path = window.location.pathname.split('folder/')[1];
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }*/
  }

  setRTL() {
    document.getElementsByTagName('html')[0]
            .setAttribute('dir', this.isEnabledRTL  ? 'rtl': 'ltr');
  }

  openPage(page) {
    this.navController.navigateRoot([page.url], {});
  }
}
