import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgmCoreModule } from '@agm/core';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyC1ewEPNVNYVrFifs4Of1V93uivtBXouhA', 
      libraries: ['places'],
      region: "IT",
      language: "IT"
    }),
  ],
  exports: []
})
export class SharedModule { 
  
}
