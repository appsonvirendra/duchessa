import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './shared/guard/auth.guard';

const routes: Routes = [
  
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'locationselect',
    loadChildren: () => import('./pages/locationselect/locationselect.module').then( m => m.LocationselectPageModule)
  },
  {
    path: 'restlist',
    loadChildren: () => import('./pages/restlist/restlist.module').then( m => m.RestlistPageModule)
  },
  {
    path: 'inforestlist',
    data: {listonly: true},
    loadChildren: () => import('./pages/restlist/restlist.module').then( m => m.RestlistPageModule)
  },
  {
    path: 'ncivico',
    loadChildren: () => import('./pages/ncivico/ncivico.module').then( m => m.NcivicoPageModule)
  },
  {
    path: 'utility',
    loadChildren: () => import('./pages/utility/utility.module').then( m => m.UtilityPageModule)
  },
  {
    path: 'menu',
    loadChildren: () => import('./pages/menu/menu.module').then( m => m.MenuPageModule)
  },
  {
    path: 'custom-menu-item',
    loadChildren: () => import('./pages/custom-menu-item/custom-menu-item.module').then( m => m.CustomMenuItemPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'cart',
    canActivate: [AuthGuard],
    loadChildren: () => import('./pages/cart/cart.module').then( m => m.CartPageModule)
  },
  {
    path: 'recovery',
    loadChildren: () => import('./pages/recovery/recovery.module').then( m => m.RecoveryPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'ordertime',
    loadChildren: () => import('./pages/ordertime/ordertime.module').then( m => m.OrdertimePageModule)
  },
  {
    path: 'coupon',
    loadChildren: () => import('./pages/coupon/coupon.module').then( m => m.CouponPageModule)
  },
  {
    path: 'checkout',
    loadChildren: () => import('./pages/checkout/checkout.module').then( m => m.CheckoutPageModule)
  },
  {
    path: 'checkpay',
    loadChildren: () => import('./pages/checkpay/checkpay.module').then( m => m.CheckpayPageModule)
  },
  {
    path: 'check',
    loadChildren: () => import('./pages/check/check.module').then( m => m.CheckPageModule)
  },
  {
    path: 'cash-pay',
    loadChildren: () => import('./pages/cash-pay/cash-pay.module').then( m => m.CashPayPageModule)
  },
  {
    path: 'card-pay',
    loadChildren: () => import('./pages/card-pay/card-pay.module').then( m => m.CardPayPageModule)
  },
  {
    path: 'credit-pay',
    loadChildren: () => import('./pages/credit-pay/credit-pay.module').then( m => m.CreditPayPageModule)
  },
  {
    path: 'recharge',
    loadChildren: () => import('./pages/recharge/recharge.module').then( m => m.RechargePageModule)
  },
  {
    path: 'convertpoints',
    loadChildren: () => import('./pages/convertpoints/convertpoints.module').then( m => m.ConvertpointsPageModule)
  },
  {
    path: 'profile',
    canActivate: [AuthGuard],
    loadChildren: () => import('./pages/profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'profile-orders',
    loadChildren: () => import('./pages/profile-orders/profile-orders.module').then( m => m.ProfileOrdersPageModule)
  },
  {
    path: 'inforestaurant',
    loadChildren: () => import('./pages/inforestaurant/inforestaurant.module').then( m => m.InforestaurantPageModule)
  },
  {
    path: 'logout',
    loadChildren: () => import('./pages/logout/logout.module').then( m => m.LogoutPageModule)
  },
  {
    path: 'changepwd',
    loadChildren: () => import('./pages/changepwd/changepwd.module').then( m => m.ChangepwdPageModule)
  },
  {
    path: 'menu-locale',
    loadChildren: () => import('./pages/menu-locale/menu-locale.module').then( m => m.MenuLocalePageModule)
  },
  {
    path: 'fidelity',
    loadChildren: () => import('./pages/fidelity/fidelity.module').then( m => m.FidelityPageModule)
  },
  {
    path: 'booking',
    loadChildren: () => import('./pages/booking/booking.module').then( m => m.BookingPageModule)
  },
  {
    path: 'cartbook',
    loadChildren: () => import('./pages/cartbook/cartbook.module').then( m => m.CartbookPageModule)
  },
  {
    path: 'bookcheckout',
    loadChildren: () => import('./pages/bookcheckout/bookcheckout.module').then( m => m.BookcheckoutPageModule)
  },
  {
    path: 'bookcheckpay',
    loadChildren: () => import('./pages/bookcheckpay/bookcheckpay.module').then( m => m.BookcheckpayPageModule)
  },
  {
    path: 'bookfinalize',
    loadChildren: () => import('./pages/bookfinalize/bookfinalize.module').then( m => m.BookfinalizePageModule)
  },
  {
    path: 'profile-bookings',
    loadChildren: () => import('./pages/profile-bookings/profile-bookings.module').then( m => m.ProfileBookingsPageModule)
  },
  {
    path: 'cancelbooking',
    loadChildren: () => import('./pages/cancelbooking/cancelbooking.module').then( m => m.CancelbookingPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
